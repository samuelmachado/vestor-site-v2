<?php

namespace frontend\controllers;

use common\models\Anuncio;
use Yii;
use common\models\Imovel;
use common\models\ImovelSearch;
use common\models\Usuario;
use common\models\Visita;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ImovelController implements the CRUD actions for Imovel model.
 */
class MinhasVisitasController extends Controller
{
    public function init() {
        date_default_timezone_set('America/Sao_Paulo');
        parent::init();

        
    }

    public function beforeAction($action) {
        if ($action->id == 'atendimento' || $action->id == 'condiz-realidade') {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $this->enableCsrfValidation = false;
        }
                // return false;
        $id = Usuario::decryptID();
        if(!$id) {
            $this->redirect(['imoveis/index']);
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all Imovel models.
     * @return mixed
     */
    public function actionIndex()
    {
        // print_r(\Yii::$app->User->identity);
        // $id = \Yii::$app->User->identity->id;

//        $session = Yii::$app->session;
        $id = Usuario::decryptID();
        $visitas = Visita::find()->andWhere(['idUsuarioAgendado' => $id])->all();
        
        return $this->render('index', [
            'visitas' => $visitas
            // 'model' => $this->findModel($id),
        ]);
    } 
    
    public function actionAtendimento($id){
        $_POST = json_decode(file_get_contents('php://input'), true);

        $anuncio = Anuncio::findOne($id);
        $anuncio->atendimento = $_POST['data'];
        $anuncio->save(false);
        return ['status' => true];

    }

    public function actionCondizRealidade($id){
        $_POST = json_decode(file_get_contents('php://input'), true);

        $anuncio = Anuncio::findOne($id);
        $anuncio->condizRealidade = $_POST['data'];
        $anuncio->save(false);
        return ['status' => true];
    

    }
    protected function findModel($id)
    {
        if (($model = Imovel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
