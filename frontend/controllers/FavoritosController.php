<?php

namespace frontend\controllers;

use common\models\Documento;
use Yii;
use common\models\Imovel;
use common\models\ImovelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Visita;
use common\models\Usuario;
use common\models\Sessao;
use common\models\TipoDocumento;
use Exception;
use yii\web\UploadedFile;
/**
 * ImovelController implements the CRUD actions for Imovel model.
 */
class FavoritosController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {            
        if ($action->id == 'upload' || $action->id == 'delete-doc') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
    public function actionIndex()
    {
        return $this->render('index', []);
    }


  
}