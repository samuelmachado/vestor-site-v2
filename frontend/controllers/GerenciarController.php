<?php

namespace frontend\controllers;

use common\models\Documento;
use Yii;
use common\models\Imovel;
use common\models\ImovelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Visita;
use common\models\Usuario;
use common\models\Sessao;
use common\models\TipoDocumento;
use Exception;
use yii\web\UploadedFile;
/**
 * ImovelController implements the CRUD actions for Imovel model.
 */
class GerenciarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {            
        if ($action->id == 'upload' || $action->id == 'delete-doc') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
    public function actionIndex()
    {
        // return 'ok';
        $id = Usuario::decryptID();
        if (!$id) {
            $this->redirect('imoveis');
        }
       
        $imoveis = Imovel::find()->where(['idProprietarioUsuario' =>  $id])->all();
        // return $imoveis;
        $out = [];
        $i = 0;
        foreach ($imoveis as $imovel) {
            if ($imovel->anuncio) {
                $out[$i]['imovel'] = $imovel;
                $out[$i]['sessoes'] = Sessao::find()->where(['id' => $imovel->idSessao])->all();
                $i++;
            }
        }

        $imoveisAlugando = Sessao::find()->where(['idUsuarioAlugando' => Usuario::decryptID()])->all();
        return $this->render('index', [
            'meusImoveis' => $out,
            'imoveisAlugando' => $imoveisAlugando,
        ]);
    }


    private function getDates($model)
    {
        $data = \DateTime::createFromFormat('d/m/Y', $model->gerenciarData1);
        if ($data)
            $model->gerenciarData1 = $data->format('Y-m-d');

        return $model;
    }

    public function actionEtapa1($id)
    {

        $model = Sessao::findOne($id);
        if (!$model) {
            $this->redirect(['index']);
        }

        if (Yii::$app->request->post()) {
            $model = $this->getDates($model);
            if(Yii::$app->request->post('selecionado') != ''){
                if(Yii::$app->request->post('selecionado') == 'sim'){
                    $model->gerenciarData1 = $model->dataEtapa1;
                } 
                
                $model->ultimaEtapa = 5;
                $model->save();
                return $this->redirect(['etapa2', 'id' => $model->id]);
            } else {
                \Yii::$app->session->setFlash('flashMessage', 'Preencha corretamente os campos');
            }
        }
        return $this->render('gerenciar-etapa1', [
            'model' => $model,
        ]);
    }

    public function actionEtapa2($id)
    {
        $model = Sessao::findOne($id);
        if (!$model) {
            $this->redirect(['index']);
        }

        if ($model->load(Yii::$app->request->post())) {
            // print Yii::$app->request->post('gerenciarValor2');
            if($model->gerenciarValor2 != ''){
                $model = $this->getDates($model);
                $model->ultimaEtapa = 6;
                $model->save();
                return $this->redirect(['etapa3', 'id' => $model->id]);
            } else {
                \Yii::$app->session->setFlash('flashMessage', 'Preencha corretamente os campos');
            }
        }
        // if($model->ultimaEtapa != 5)
        //     return $this->redirecionarEtapaCorreta($model);
        return $this->render('gerenciar-etapa2', [
            'model' => $model,
        ]);
    }

    public function actionEtapa3($id)
    {
        $model = Sessao::findOne($id);
        if (!$model) {
            $this->redirect(['index']);
        }

        if ($model->load(Yii::$app->request->post())) {
            $exibirFlash = false;
            if(Yii::$app->request->post('selecionado') == 'sim' && !$model->gerenciarCarencia3)
                $exibirFlash = true;
            if(Yii::$app->request->post('selecionado') && !$exibirFlash){
                $model = $this->getDates($model);
                $model->ultimaEtapa = 7;
                $model->save();
                return $this->redirect(['etapa4', 'id' => $model->id]);
            } else {
                \Yii::$app->session->setFlash('flashMessage', 'Preencha corretamente os campos');
            }
        }
        // if($model->ultimaEtapa != 6)
        //     return $this->redirecionarEtapaCorreta($model);
        return $this->render('gerenciar-etapa3', [
            'model' => $model,
        ]);
    }

    public function actionEtapa4($id)
    {
        $model = Sessao::findOne($id);
        if (!$model) {
            $this->redirect(['index']);
        }

        if ($model->load(Yii::$app->request->post())) {
            $exibirFlash = false;
            if(Yii::$app->request->post('selecionado') == 'sim' && !$model->gerenciarTempoDesconto4)
                $exibirFlash = true;
            if(Yii::$app->request->post('selecionado') == 'sim' && $model->gerenciarTempoDesconto4 && !$model->gerenciarValorDesconto4)
                $exibirFlash = true;
            if(Yii::$app->request->post('selecionado') && !$exibirFlash){
                $model = $this->getDates($model);
                $model->ultimaEtapa = 8;
                $model->save();
                return $this->redirect(['etapa5', 'id' => $model->id]);
            } else {
                \Yii::$app->session->setFlash('flashMessage', 'Preencha corretamente os campos');
            }
        }
        // if($model->ultimaEtapa != 7)
        //     return $this->redirecionarEtapaCorreta($model);
        return $this->render('gerenciar-etapa4', [
            'model' => $model,
        ]);
    }
    
    

    public function actionEtapa5($id)
    {
        $model = Sessao::findOne($id);
        if (!$model) {
            $this->redirect(['index']);
        }

        if (Yii::$app->request->post()) {
            $model = $this->getDates($model);
            $model->ultimaEtapa = 9;
            $model->save();
            return $this->redirect(['imoveis/aguarde', 'tipo' => 2]);
        }
        // if($model->ultimaEtapa != 8)
        //     return $this->redirecionarEtapaCorreta($model);
        return $this->render('gerenciar-etapa5', [
            'model' => $model,
        ]);
    }


    //SESSÃO INQUILINO
    public function actionEtapa6($id)
    {
        $model = Sessao::findOne($id);
        if (!$model) {
            $this->redirect(['index']);
        }  

        if ($model->load(Yii::$app->request->post())) {
            // $model->desconto7 =  Yii::$app->request->post('desconto7');
            // $model->valorAluguel7 =  Yii::$app->request->post('valorAluguel7');
            // $model->carencia7 =  Yii::$app->request->post('carencia7');
        

            $model = $this->getDates($model);
            $model->ultimaEtapa = 10;
            $model->save();
            return $this->redirect(['etapa7', 'id' => $model->id]);

        }

        $camposAcordo = [
            'inicioContrato' => false,
            'valorAluguel' => false,
            'descontoAluguel' => false,
            'tempoCarencia' => false
        ];
        // Data de início do contrato
        if($model->dataEtapa1 == $model->gerenciarData1){
            $camposAcordo['inicioContrato'] = true;
        }

        // Valor aluguel
        if($model->valor2 == $model->gerenciarValor2){
            $camposAcordo['valorAluguel'] = true;
        }



        // if($model->ultimaEtapa != 9)
        //     return $this->redirecionarEtapaCorreta($model);
     
        return $this->render('gerenciar-etapa6', [
            'model' => $model,
            'camposAcordo' => $camposAcordo
        ]);
    }

    public function actionEtapa7($id)
    {
        $model = Sessao::findOne($id);
        if (!$model) {
            $this->redirect(['index']);
        }

        if (Yii::$app->request->post()) {
            $model = $this->getDates($model);
            $model->ultimaEtapa = 11;
            $model->save();
            return $this->redirect(['aguarde']);
        }
        // if($model->ultimaEtapa != 10)
        //     return $this->redirecionarEtapaCorreta($model);
        return $this->render('gerenciar-etapa7', [
            'model' => $model,
        ]);
    }

    public function actionUpload($id, $tipo){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $sessao = Sessao::findOne($id);
    
        return $resposta  = $this->actionUploadFile($sessao, 'file', $tipo);
      
    }
    public function redirecionarEtapaCorreta($model){
        switch($model->ultimaEtapa){
            // case 5: return $this->redirect(['etapa2', 'id' => $model->id]); break;
            // case 6: return $this->redirect(['etapa3', 'id' => $model->id]); break;
            // case 7: return $this->redirect(['etapa4', 'id' => $model->id]); break;
            // case 8: return $this->redirect(['etapa5', 'id' => $model->id]); break;
            // case 8: return $this->redirect(['etapa5', 'id' => $model->id]); break;
            // case 9: return $this->redirect(['aguarde']); break;
            // case 12: return $this->redirect(['aguarde']); break;
        }
    }
   
    private function actionUploadFile($model, $file, $idTipoDocumento)
    {

        $arquivos = UploadedFile::getInstancesByName($file);
        if ($arquivos) {
           
            $dirBase = Yii::getAlias('@webroot') . '/';
            $dir = 'arquivos/sessao/' . $idTipoDocumento . '/';

            if (!file_exists($dirBase . $dir))
                mkdir($dir, 0777, true);

            $i = 1;
            $resposta = [];
            foreach ($arquivos as $arquivo) {
                $nomeArquivo = $model->id.'_'.$idTipoDocumento . '_' . time() . '_' . $i.rand(1,10).rand(1,10).rand(1,10) . '.' . $arquivo->extension;
                $arquivo->saveAs($dirBase . $dir . $nomeArquivo);

                $modelDocumento = new Documento();
                $modelDocumento->nome = $arquivo->name;
                $modelDocumento->idSessao = $model->id;
                $modelDocumento->arquivo = $dir . $nomeArquivo;
                $modelDocumento->idTipoDocumento = $idTipoDocumento;
                $modelDocumento->dataCadastro = date('Y-m-d H:i:s');
                $modelDocumento->save();

                $modelDocumento->arquivo = '../'.$modelDocumento->arquivo; 
                $resposta[] = $modelDocumento;
                // print_r($modelDocumento->getErrors());
                $i++;
            }
            return $resposta;
        }
    }

    public function actionAguarde()
    {
        return $this->render('gerenciar-aguarde', [
            // 'model' => $model
        ]);
    }

    public function actionDeleteDoc($id)
    {
        try {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model = Documento::findOne($id);
            $arquivo = $model->arquivo;
            if ($model->delete()) {
                return [
                    'status' => true,
                    'message' => 'Documento excluído da base. ' . ((!unlink(Yii::$app->basePath . "/web/" . $arquivo)) ? 'Arquivo não excluído.' : ''),
                ];
            } else {
                return [
                    'status' => false,
                    'message' => 'Erro ao excluir documento',
                ];
            }
        } catch(Exception $e){
            return [
                'status' => true,
                'message' => 'Arquivo já foi excluído',
            ];
        }
        
    }

    public function actionFinalizarEtapa($id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = Sessao::findOne($id);
        if ($model) {
            $model->ultimaEtapa = 12;
            $model->save();
            return [
                'status' => true,
                'message' => '',
            ];
        }
        return [
            'status' => false,
            'message' => '',
        ];
      
        
    }
}