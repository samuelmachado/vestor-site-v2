<?php

namespace frontend\controllers;

use Yii;
use common\models\Imovel;
use common\models\ImovelSearch;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Visita;
use common\models\Usuario;
use common\models\Sessao;

/**
 * ImovelController implements the CRUD actions for Imovel model.
 */
class ImoveisController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Imovel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = Imovel::find()->joinWith('anuncio')->joinWith('cep');
        
        if ($get = Yii::$app->request->get()){

            if (isset($get['bairro']) && !empty($get['bairro']))
                $model->andFilterWhere(['in', 'cep.idBairro', $get['endereco']]);
            
            if(isset($get['valor_minimo']) && is_numeric($get['valor_minimo']))
                $model->andFilterWhere(['>=','valor',$get['valor_minimo']]);

            if(isset($get['valor_maximo']) && is_numeric($get['valor_maximo']))
                $model->andFilterWhere(['<=','valor',$get['valor_maximo']]);

            if(isset($get['quarto']) && is_numeric($get['quarto']))
                $model->andFilterWhere(['>=','quarto',$get['quarto']]);

            if(isset($get['quarto']) && is_numeric($get['quarto']))
                $model->andFilterWhere(['>=','quarto',$get['quarto']]);

            if(isset($get['quarto']) && is_numeric($get['quarto']))
                $model->andFilterWhere(['>=','quarto',$get['quarto']]);

        }
        $countQuery = clone $model;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);
        
        return $this->render('index', [
            'model' => $model->offset($pages->offset)->limit($pages->limit)->all(),
            'pages' => $pages,
            'get' => Yii::$app->request->get()
        ]);
    }

    /**
     * Displays a single Imovel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
    
        return $this->render('view', [
            // 'model' => $this->findModel($id),
        ]);
    }

    public function actionGerenciar(){
        // return 'ok';
        $imoveis = Imovel::find()->where(['idProprietarioUsuario' =>  Usuario::decryptID()])->all();
        // return $imoveis;
        $out=[];
        foreach($imoveis as $imovel) {
            if($imovel->anuncio){
                $out[] = $imovel;
            }
        }
        
    }
 
    public function actionEtapa1($id) {
        $visita = Visita::findOne($id);
        if(!$visita || Usuario::decryptID() != $visita->idUsuarioAgendado)
            $this->redirect(['minhas-visitas']);

        $model = Sessao::findOne(['idVisita' => $id]);
        
        if(!$model){
            $model = new Sessao();
            $model->idVisita = $id;
            $model->ultimaEtapa = 1;
        }

        if ($model->load(Yii::$app->request->post())) {
            $imovel = Imovel::findOne($visita->anuncio->idImovel);

            $model = $this->getDates($model);
            // print $model->dataEtapa1;
            if($model->dataEtapa1 != ''){
                $model->ultimaEtapa = 2;
                $model->idUsuarioAlugando = $visita->idUsuarioAgendado;
                $model->save();
                $imovel->idSessao = $model->id;
                $imovel->save();
                return $this->redirect(['etapa2', 'id' => $model->id]);
            } else {
                \Yii::$app->session->setFlash('flashMessage', 'Preencha corretamente os campos');
            }
        }
            // if($model->ultimaEtapa != 1)
                // return $this->redirecionarEtapaCorreta($model);
            $model = $this->getDatesBr($model);
            return $this->render('etapa1', [
                'model' => $model
            ]);   
        
    }

    public function actionEtapa2($id) {
        $model = Sessao::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->valor2){
                $model->ultimaEtapa = 3;
                $model->save();
                return $this->redirect(['etapa3', 'id' => $model->id]);
            } else {
                \Yii::$app->session->setFlash('flashMessage', 'Preencha corretamente os campos');
            }
        }
    
        return $this->render('etapa2', [
            'model' => $model
        ]);   
        
    }

    public function actionEtapa3($id) {
        $model = Sessao::findOne($id);

        // este model não carrega! é só o post
        if (Yii::$app->request->post()) 
        {
            $model->ultimaEtapa = 4;
            $model->save();
            return $this->redirect(['aguarde', 'tipo' => 1]);

        } else {
            // if($model->ultimaEtapa != 3)
                // return $this->redirecionarEtapaCorreta($model);
            return $this->render('etapa3', [
                'model' => $model
            ]);   
        }
    }

    public function actionAguarde() {
        return $this->render('aguarde', [
            // 'model' => $model
        ]);   
    }

    private function getDates($model)
    {
        $data = \DateTime::createFromFormat('d/m/Y', $model->dataEtapa1);
        if ($data)
            $model->dataEtapa1 = $data->format('Y-m-d');
       
        return $model;
    }

    public function redirecionarEtapaCorreta($model){
        switch($model->ultimaEtapa){
            case 2: return $this->redirect(['etapa2', 'id' => $model->id]); break;
            case 3: return $this->redirect(['etapa3', 'id' => $model->id]); break;
            default: return $this->redirect(['aguarde', 'etapa' => 1]);
        }
    }
    public function actionRedirecionar($id){
        $sessao = Sessao::find()->where(['idVisita' => $id])->one();
        if(!$sessao){
            return $this->redirect(['etapa1', 'id' => $id]);
        }
        return $this->redirecionarEtapaCorreta($sessao);
    }
    private function getDatesBr($model)
    {
        $data = \DateTime::createFromFormat('Y-m-d', $model->dataEtapa1);

        if ($data && $model->dataEtapa1 != '0000-00-00')
            $model->dataEtapa1 = $data->format('d/m/Y');
        else
            $model->dataEtapa1 = '';

        return $model;
    }

    protected function findModel($id)
    {
        if (($model = Imovel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
