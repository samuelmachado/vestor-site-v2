<?php

namespace frontend\controllers;

use Yii;
use common\models\Usuario;
use common\models\UsuarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\AccessRule;
use yii\filters\AccessControl;
use yii\helpers\BaseHtml;

/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['update', 'delete', 'index', 'view'],
                'rules' => [
                    [
                        'actions' => ['update', 'delete', 'index'],
                        'allow' => true,
                        // Allow moderators and admins to update
                        'roles' => [
                            Usuario::PERFIL_SUPER_ADMIN,
                        ],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        // Allow moderators and admins to update
                        'roles' => [
                            Usuario::PERFIL_SUPER_ADMIN,
                        ],
                    ],
                ],
            ]
        ];
    }
    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 20];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usuario();

        if ($model->load(Yii::$app->request->post())) {
            // Mesma coisa com a senha

            if ($model->password) {
                $model->setPassword($model->password);
                $model->generateAuthKey();
            }


            if (Usuario::find()->where(['email' => $model->email])->one()) {
                \Yii::$app->getSession()->setFlash('error', 'E-mail já cadastrado no sistema');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }


            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Usuário criado com sucesso');
                //return $this->redirect(Yii::$app->request->referrer);
                return $this->redirect(['usuario/view', 'id' => $model->id]);
            } else {
                \Yii::$app->getSession()->setFlash('error', BaseHtml::errorSummary($model, ['header' => 'Erro ao salvar o usuário.']));
            }
        }



        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->password) {
                $model->setPassword($model->password);
                $model->save();
            }else{
                \Yii::$app->getSession()->setFlash('error', BaseHtml::errorSummary($model, ['header' => 'Erro ao alterar o usuário.']));
            }
            
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
