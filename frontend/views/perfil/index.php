
    <style type="text/css">
        .orange {
            color: #ff8800;
            background: transparent;
        }
       .js-choose:hover {
    background-color:#ff8800 !important;
    color: white !important;
    }
    .icones {
        max-width: 76%;
    }


    .banner-area .banner-content .rent-box .tab-content .for-rent-content {
            background: transparent;
            padding: 10px 0px 0px 0px;

            box-shadow: none;
        }
    
        .banner-content span, h1 {
            color: white;
            font-family: "Nunito";
            letter-spacing: 3.12px;
            font-weight: bold;
            font-size: 104px;
        }

        .banner-content .text-vestor {
            font-size: 43px;
            letter-spacing: 1.29px;
            color:white;
        }
        .title {
            z-index: 20 !important;
        }
        button {
            background-color: #ff8800 !important;
        }
        .h4-input {
            font-family: "Nunito";
            color: white;
        }
        p {
            font-family: "Nunito";
            color:#575757;
        }
        /* .banner-area .banner-content .rent-box .tab-content .for-rent-content .item .input-box input {
            width: 170px;
        } */
        .text-white {
           color: white; 
           font-family: "Nunito";
        }
        .text-reverse {
            color: #A90C44; 
            font-family: "Nunito";
        }
        .title-banner {
            font-family: "Nunito";
            color: white;
            font-size: 34px !important;
        }
      

        .item input {
            margin-top: 33px !important;
    	    height: 39px !important;
        }
    </style>
  


    <section class="property-grid-area property-grid-3-area   mt-50 pb-120">
        <div class="container-fluid" >
            <div class="row justify-content-center">
                <?= $this->render('../layouts/include-menu') ?>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="item">
                                <div class="input-box">
                                    <div class="col-12">
                                        <input type="text" id="nome" placeholder="Nome Completo" class="form-control border-input-full">
                                    </div>
                                </div>
                                <div class="input-box input-group">
                                    <div class="col-3 col-sm-4 mt-2">
                                    <input type="text" id="ddd" placeholder="DDD" class="form-control border-input-full ">
                                    </div>
                                    <div class="col-9 col-sm-8 mt-2">
                                        <input type="text"  id="celular" placeholder="Celular" class="form-control border-input-full float-right">
                                    </div>
                                </div>
                                <div class="input-box">
                                    <div class="col-12 mt-2">
                                        <input type="text" id="email" placeholder="E-mail" class="form-control border-input-full">
                                    </div>
                                </div>

                                <div class="input-box mt-5">
                                    <div class="col-12 float-right">
                                        <button type="button" class="btn-large btn-global float-right" onclick="continuar()" >SALVAR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <div id="tmpDiv" style="display: none;"></div>
    <!--====== PROPERTY PART ENDS ======-->


 
    <script type="text/javascript">
        var listaImoveis = []
        var usuario = null;
        $(function() {
            viewUsuario().then((response) => {
                usuario = response.data.usuario;

                nome.value = usuario.pessoa.nome
                ddd.value = usuario.pessoa.telefone.substring(0,2)
                celular.value = usuario.pessoa.telefone.substring(2,80)
                email.value = usuario.email
            })
        });

        function continuar(){
            atualizarUsuario({data: {nome: nome.value, email: email.value}})
        .then(
          response => {
           
            if(response.data.status){
                localStorage.setItem('usuario', JSON.stringify(response.data.usuario));
                usuario = response.data.usuario
                return Swal.fire({
                        icon: 'success',
                        title: 'Atualizado com sucesso',
                        showConfirmButton: true,
                    })
            } else {
                return Swal.fire({
                        icon: 'error',
                        title: 'Erro ao atualizar dados',
                        showConfirmButton: true,
                    })
            }
        })
        }
    </script>
    
    <!--====== GO TO TOP PART ENDS ======-->




