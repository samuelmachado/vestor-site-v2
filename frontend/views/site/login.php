<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='fas fa-user form-control-feedback login_icon-user'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span id='key' class='gira fas fa-key form-control-feedback login_icon-key'><span id='olho' class='fas fa-eye login_icon-eye'></span>"
];
?>
<div class="bg-overlay"></div>
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b><img class="login_logo" src="imgs/véstor.png"></b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>
        <div class="row">
            <!-- /.col -->
            <!-- <div class="col-xs-8">
            </div> -->
            <div class="login_button">
                <?= Html::submitButton('Entrar', ['class' => 'btn btnLogin btn-flat btn btn-flat-block btn btn-flat-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>


        <?php ActiveForm::end(); ?>

        <!-- /.social-auth-links -->
        <div class="row text-center">
            <br><a class="forgot-password" href="index.php?r=site/reset">Esqueci minha senha</a><br>
        </div>
        <div class="login_developed-by">
          <!-- <p class="">Desenvolvido pela Véstor</p> -->
        </div>
    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->

<script>
    var senha = $('#loginform-password');
    var olho= $("#olho");

    olho.mousedown(function() {
    senha.attr("type", "text");
    });

    olho.mouseup(function() {
    senha.attr("type", "password");
    });
    // para evitar o problema de arrastar a imagem e a senha continuar exposta,
    //citada pelo nosso amigo nos comentários
    $("#olho").mouseout(function() {
    $("#loginform-password").attr("type", "password");
    });
</script>

<style media="screen">

</style>
