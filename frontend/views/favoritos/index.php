    <style type="text/css">
        .orange {
            color: #ff8800;
            background: transparent;
        }
       .js-choose:hover {
    background-color:#ff8800 !important;
    color: white !important;
    }
    .icones {
        max-width: 76%;
    }


    .banner-area .banner-content .rent-box .tab-content .for-rent-content {
            background: transparent;
            padding: 10px 0px 0px 0px;

            box-shadow: none;
        }
    
        .banner-content span, h1 {
            color: white;
            font-family: "Nunito";
            letter-spacing: 3.12px;
            font-weight: bold;
            font-size: 104px;
        }

        .banner-content .text-vestor {
            font-size: 43px;
            letter-spacing: 1.29px;
            color:white;
        }
        .title {
            z-index: 20 !important;
        }
        button {
            background-color: #ff8800 !important;
        }
        .h4-input {
            font-family: "Nunito";
            color: white;
        }
        p {
            font-family: "Nunito";
            color:#575757;
        }
        /* .banner-area .banner-content .rent-box .tab-content .for-rent-content .item .input-box input {
            width: 170px;
        } */
        .text-white {
           color: white; 
           font-family: "Nunito";
        }
        .text-reverse {
            color: #A90C44; 
            font-family: "Nunito";
        }
        .title-banner {
            font-family: "Nunito";
            color: white;
            font-size: 34px !important;
        }
      

        .item input {
            margin-top: 33px !important;
    	    height: 39px !important;
        }
    </style>
  


    <section class="property-grid-area property-grid-3-area   mt-50 pb-120">
        <div class="container-fluid" >
            <div class="row justify-content-center">
                <?= $this->render('../layouts/include-menu') ?>
                <div class="col-lg-9">
                    <div class="row imoveis">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="tmpDiv" style="display: none;"></div>




<script>
let favoritos = obterFavoritos()
console.warn(favoritos)

for(let i = 0; i < favoritos.length; i++) {
    let imovel = favoritos[i];
    console.log(imovel);
    $(".imoveis").append(`
     <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="listing-item mt-40" >
                <div class="listing-content white-bg" >
                    <h5 class="subtitle-vestor text-center mb-2">CÓD 12345</h5>
                    <img src="../imgs/casa.jpg" style="border-radius: 23px;">
                    <span class="center text endereco"> Rua das Caieras Santana - São José dos Campos</span>
                    <h3 class="center orange aluguel"></h3>
                    <div class="list">
                        <ul >
                            <li><img src="../imgs/icon_metragem.svg" class="text-center icones"><br><span class="m2"></span></li>
                            <li><img src="../imgs/icon_quarto.svg" class="text-center icones"><br><span class="quartos"></span></li>
                            <li><img src="../imgs/icon_vaga.svg" class="text-center icones"><br><span class="vagas"></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    `)
    $(".codigo").text(`CÓD ${imovel.id}`)
    $(".aluguel").text(`R$ ${imovel.valor}`)
    $(".condominio").text(`+ Condomínio: R$ ${imovel.valorCondominio}`)
    $(".iptu").text(`+ IPTU: R$ ${imovel.valorIPTU}`)
    $(".seguro-incendio").text(`+ Seguro Incêndio: R$ ${imovel.valorSeguroIncendio}`)
    $(".m2").text(`${imovel.area}m²`)
    $(".quartos").text(`${imovel.quarto} quartos`)
    $(".vagas").text(`${imovel.vagaGaragem} vagas`)
    $(".endereco").text(`${imovel.enderecoCompleto}`)
}

    
</script>