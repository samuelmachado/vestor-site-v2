<?php 
use kartik\number\NumberControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

function getSelecione($opcao, $model){
    $p = Yii::$app->request->post('selecionado');
    if($p == $opcao || ($opcao == 'sim' && $model->gerenciarCarencia3 > 0))
        return 'selected';
    return '';
}
function getCarencia($opcao, $model){
    if($opcao == Yii::$app->request->post('Sessao[gerenciarCarencia3]') || $model->gerenciarCarencia3 == $opcao)
        return 'selected';
    return '';
}
?>

<style>
    .gerenciar-etapa1 .input-carencia {
        display: none;
    }
</style>
<?php $form = ActiveForm::begin([
    'id' => 'formAluno',
    'options' => ['enctype' => 'multipart/form-data'],
    'encodeErrorSummary' => false,
    'errorSummaryCssClass' => 'help-block',

  ]); ?>
<div class="container gerenciar-etapa1">
    <div class="row">
        <div class="col-md-12">
            <?= $this->render('../layouts/aviso') ?>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-md-6">
            Foi acordado alguma carência para o contrato?<br>
            <select id="inicioCarencia" name="selecionado" class="form-control">
                <option value="">--SELECIONE--</option>
                <option value="sim" <?= getSelecione('sim', $model) ?>>Sim</option>
                <option value="não" <?= getSelecione('não', $model) ?>>Não</option>
            </select>
        </div>
        
        <div class="col-md-6">
   

            <div class="input-carencia">
                Selecione a carência<br>
                <select name="Sessao[gerenciarCarencia3]" id="gerenciarCarencia" class="form-control" value="<?= $model->gerenciarCarencia3 ?>">
                    <option value="">--SELECIONE--</option>    
                    <option value="1" <?= getCarencia('1', $model) ?>>1</option>    
                    <option value="2" <?= getCarencia('2', $model) ?>>2</option>
                    <option value="3" <?= getCarencia('3', $model) ?>>3</option>
                    <option value="4" <?= getCarencia('4', $model) ?>>4</option>
                    <option value="5" <?= getCarencia('5', $model) ?>>5</option>
                    <option value="6" <?= getCarencia('6', $model) ?>>6</option>
                </select>
            </div>

        </div>
    </div>
    
    <div class="form-group center mt-5">
        <a class="btn btn-01" href="<?= Url::toRoute(['gerenciar/etapa2', 'id' =>  $model->id]) ?>"  >Anterior</a>&nbsp;&nbsp;
        <?= Html::submitButton('Próximo', ['class' => 'btn btn-01']) ?>
  	</div>
</div>

  <?php ActiveForm::end(); ?>

  <script>
    $("#inicioCarencia").change(function() {
        carencia();
    })

    function carencia(){
        let opcao = $("#inicioCarencia").val();
        if(opcao == 'sim'){
            $('.input-carencia').css('display','block')
        } else {
            $("#gerenciarCarencia").val(null)
            $('.input-carencia').css('display','none')
        }
    }

    carencia();
</script>
