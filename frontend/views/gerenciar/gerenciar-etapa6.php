<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<style>
    .btn-primary {
        border-radius: 50px;
        height: 30px;
        cursor:pointer;
        color: white !important;
    }
    .btn-danger {
        cursor:pointer;
        color: white !important;
        border-radius: 50px;
        font-weight: bold;
        font-size: 16px;
        color: white;
        height: 30px;
        width: 170px;
        display: flex;
        justify-content: center;
        align-items: center;
        margin: 0 15px;
    }
</style>
<?php $form = ActiveForm::begin([
    'id' => 'formAluno',
    'options' => ['enctype' => 'multipart/form-data'],
    'encodeErrorSummary' => false,
    'errorSummaryCssClass' => 'help-block',

]); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Itens de comum acordo</h3>
            <table class="table table-hover table-bordered table-striped">
                <tr>
                    <th>item</td>
                    <th>item de comum acordo</th>
                </tr>
                <?php if(!$camposAcordo['inicioContrato']): ?>
                <tr>
                    <td>Início do contrato</td>
                    <td><?= $model->gerenciarData1 ?></td>
                </tr>
                <?php endif; ?>
                <?php if(!$camposAcordo['valorAluguel']): ?>
                <tr>
                    <td>Valor do aluguel</td>
                    <td>R$ <?= $model->valor2 ?></td>
                </tr>
                <?php endif; ?>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>Itens de discordância</h3>
            <table id="tb" class="table table-bordered">
                <tr>
                    <td></td>
                    <td align="center">Valor do anúncio</td>
                    <td align="center">Sugerido por você</td>
                    <td align="center">Sugerido pelo proprietário</td>
                    <td align="center"><b>Você concorda com o proprietário?</b></td>
                </tr>

                <?php if(!$camposAcordo['inicioContrato']): ?>
                <tr>
                    <td align="center">Data de início do contrato</td>
                    <td align="center">-</td>
                    <td align="center"><?= Yii::$app->formatter::asData($model->dataEtapa1) ?></td>
                    <td align="center"><?= Yii::$app->formatter::asData($model->gerenciarData1) ?></td>
                    <td><a campo="inicio_contrato" class="btn btn-primary">Concordo</a><a campo="inicio_contrato" class="btn btn-danger">Discordo</a></td>
                </tr>
                <?php endif; ?>
                <?php if(!$camposAcordo['valorAluguel']): ?>
                <tr>
                    <td align="center">Valor do aluguel</td>
                    <td align="center">R$ <?= $model->visita->anuncio->imovel->valor ?> </td>    
                    <td align="center">R$ <?= $model->valor2 ?> </td>
                    <td align="center">R$ <?= $model->gerenciarValor2 ?></td>
                    <td><a campo="valor_aluguel" class="btn btn-primary">Concordo</a><a campo="valor_aluguel" class="btn btn-danger">Discordo</a></td>
                </tr>
                <?php endif; ?>
                <tr>
                    <td align="center">Desconto</td>
                    <td align="center">-</td>    
                    <td align="center">-</td>
                    <td align="center"><?= $model->hasDesconto() ?></td>
                    <td><a campo="desconto" class="btn btn-primary">Concordo</a><a campo="desconto" class="btn btn-danger">Discordo</a></td>
                </tr>
                <tr>
                    <td align="center">Carência</td>
                    <td align="center">-</td>    
                    <td align="center">-</td>
                    <td align="center"><?= $model->hasCarencia() ?></td>
                    <td><a campo="carencia" class="btn btn-primary">Concordo</a><a campo="carencia" class="btn btn-danger">Discordo</a></td>
                </tr>
            </table>
        </div>
    </div>

    <input type="hidden" name="Sessao[carencia7]" id="carencia" value="" >
    <input type="hidden" name="Sessao[valorAluguel7]" id="valor_aluguel" value="" >
    <input type="hidden" name="Sessao[desconto7]" id="desconto" value="" >

    <div class="row">
        <div class="col-md-12">
            <div class="form-group center mt-5">
                <a class="btn btn-01" href="<?= Url::toRoute(['gerenciar/index', 'id' =>  $model->id]) ?>">Anterior</a>&nbsp;&nbsp;
                <button class="btn btn-01" id="submit">Finalizar</button>

            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
var concordo = [];
var discordo = [];

$(document).ready(function() {
    $('#submit').click(() =>{
        let opcoes = concordo.concat(discordo);
        if(opcoes.length < 3){
            return Swal.fire(
                'Atenção',
                'Responda todas as perguntas 😁',
                'warning'
            ); 
        } else {
            $("form").submit();
        }
    
    })
})
$(".btn-primary").click(function(){
    // Bingo! É isso aí...mais um cadinho e chegaremos lá!
    Swal.fire({
        title: 'Tem certeza?',
        text: "Bingo! É isso aí...mais um cadinho e chegaremos lá!. Tem certeza que está de acordo?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {
            concordo.push($(this).attr('campo'))
            $(this).closest('td').html("Respondido")
        }
    })
})
$(".btn-danger").click(function(){ 
    //Óoouuu.... nem tudo são flores, mas um ajustezinho daqui e outro ali, firmaremos essa relação!
    Swal.fire({
        title: 'Tem certeza?',
        text: "Óoouuu.... nem tudo são flores, mas um ajustezinho daqui e outro ali, firmaremos essa relação. Um agente Véstor será designado para resolver o conflito.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {
            discordo.push($(this).attr('campo'))
            $(this).closest('td').html("Respondido")
            $(`#${$(this).attr('campo')}`).val($(this).attr('campo'))
        }
    })
   
})


$('#tb td').mouseover(function(){
	processarFundo($(this), '#E4E4E4', '#E4E4E4','#E4E4E4');
	processarFonte($(this), 'bold');

});
$('#tb td').mouseleave(function(){
	processarFundo($(this));
	processarFonte($(this));

});
function processarFundo(el, fistColor='white', secondColor='white', thirdColor='white'){
	var td = $(el)
	let tdIndex = $(el).index()
	let tr = $(el).closest('tr')

	// PINTA A LINHA
	$(tr).find('td').each(function()  {
		$(this).css("background-color", thirdColor)
	})

	// PINTA A PRIMEIRA COLUNA DA LINHA ATUAL
	let firstCol = tr.find('td')[0];
	$(firstCol).css("background-color",secondColor)
	$(td).css("background-color",thirdColor)
	// console.log(tdIndex)

	
	// PINTA A COLUNA QUE VOCÊ ESTÁ DA PRIMEIRA LINHA
	let tdsFirstRow = $('#tb tr')[0];
	let tdSelected = $(tdsFirstRow).find('td')[tdIndex];
	$(tdSelected).css('background-color', fistColor)
}

function processarFonte(el, fistColor='normal'){
	var td = $(el)
	let tdIndex = $(el).index()
	let tr = $(el).closest('tr')

	// PINTA A LINHA
	$(tr).find('td').each(function()  {
		$(this).css("font-weight", fistColor)
	})

	// PINTA A PRIMEIRA COLUNA DA LINHA ATUAL
	let firstCol = tr.find('td')[0];
	$(firstCol).css("font-weight",fistColor)
	$(td).css("font-weight",fistColor)
	// console.log(tdIndex)

	
	// PINTA A COLUNA QUE VOCÊ ESTÁ DA PRIMEIRA LINHA
	let tdsFirstRow = $('#tb tr')[0];
	let tdSelected = $(tdsFirstRow).find('td')[tdIndex];
	$(tdSelected).css('font-weight', fistColor)
}
</script>