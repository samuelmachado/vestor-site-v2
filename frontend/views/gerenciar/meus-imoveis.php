<?php
use yii\helpers\Url;

?>
<div class="row visita">
    <div class="col-md-12">
        <div class="row mt-3 ml-5">
            <div class="row">
                <div class="col-md-8">
                        <img src="imgs/casa.jpg" style="border-radius: 30px;" alt="">
                </div>
                <div class="col-md-4 ">
                    <div class="bg-rounded listed-box white-bg box mt-80" style="border-radius: 30px;background: white;;">
                    <?= $imovel->enderecoPadrao ?>
                    </div>
                    <div style="padding-left:30px;padding-right:30px;padding-top:5px;">
                    <?php
                        if(!$sessoes){
                            print '<h6>Não há nenhum processo de locação ativo</h6>';
                        }
                   
                        foreach($sessoes as $sessao) {
                            $url = Yii::$app->LocacaoUrl::proprietario($sessao->ultimaEtapa, $sessao->id);
                            if($url){ ?>
                                <b class="btn-minha-visita" ><a  class="btn-minha-visita" href="<?=  Url::toRoute($url);  ?>">Processo de locação</a></b>
                            <?php 
                            }
                        }    
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 