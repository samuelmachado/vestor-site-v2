<?php 
use kartik\number\NumberControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
function getSelecione($opcao, $model){
    $p = Yii::$app->request->post('selecionado');
    if($p == $opcao || ($opcao == 'sim' && $model->gerenciarTempoDesconto4 > 0))
        return 'selected';
    return '';
}
function getCarencia($opcao, $model){
    if($opcao == Yii::$app->request->post('Sessao[gerenciarCarencia3]') || $model->gerenciarCarencia3 == $opcao)
        return 'selected';
    return '';
}

?>
<style>
    .gerenciar-etapa1 .input-carencia, .desconto-valor {
        display: none;
    }
</style>

<?php $form = ActiveForm::begin([
    'id' => 'formAluno',
    'options' => ['enctype' => 'multipart/form-data'],
    'encodeErrorSummary' => false,
    'errorSummaryCssClass' => 'help-block',

  ]); ?>
<div class="container gerenciar-etapa1">
    <div class="row">
        <div class="col-md-12">
            <?= $this->render('../layouts/aviso') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            Foi acordado algum <b>desconto</b> para o contrato?<br>
            <select id="inicioCarencia" name="selecionado" class="form-control">
                <option value="">--SELECIONE--</option>
                <option value="sim" <?= getSelecione('sim', $model) ?>>Sim</option>
                <option value="não" <?= getSelecione('não', $model) ?>>Não</option>
            </select>
        </div>
        <div class="col-md-4">
            <div class="input-carencia">
                por quanto tempo? (meses)<br>
                <select name="Sessao[gerenciarTempoDesconto4]" id="gerenciarTempoDesconto4" class="form-control">
                    <option value="">--SELECIONE--</option>    
                    <option value="1" <?= getCarencia('1', $model) ?>>1</option>    
                    <option value="2" <?= getCarencia('2', $model) ?>>2</option>
                    <option value="3" <?= getCarencia('3', $model) ?>>3</option>
                    <option value="4" <?= getCarencia('4', $model) ?>>4</option>
                    <option value="5" <?= getCarencia('5', $model) ?>>5</option>
                    <option value="6" <?= getCarencia('6', $model) ?>>6</option>
                    <option value="7" <?= getCarencia('7', $model) ?>>7</option>    
                    <option value="8" <?= getCarencia('8', $model) ?>>8</option>
                    <option value="9" <?= getCarencia('9', $model) ?>>9</option>
                    <option value="10" <?= getCarencia('10', $model) ?>>10</option>
                    <option value="11" <?= getCarencia('11', $model) ?>>11</option>
                    <option value="12" <?= getCarencia('12', $model) ?>>12</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="input-carencia">
                Qual o valor do desconto?<br>
                <div class="desconto-valor">
                    <input type="text" name="Sessao[gerenciarValorDesconto4]"class="form-control money"  value="<?= $model->gerenciarValorDesconto4 ?>" >
                </div>
            </div>
        </div>
    </div>

    <div class="form-group center mt-5">
        <a class="btn btn-01" href="<?= Url::toRoute(['gerenciar/etapa3', 'id' =>  $model->id]) ?>"  >Anterior</a>&nbsp;&nbsp;
        <?= Html::submitButton('Próximo', ['class' => 'btn btn-01']) ?>
  	</div>
</div>




<?php ActiveForm::end(); ?>

  <script>
    $("#inicioCarencia").change(function() {
        gerenciarCarencia();
    })

    $("#gerenciarTempoDesconto4").change(() => {
        gerenciarTempo()
    });

    function gerenciarTempo(){
        let desc = $("#gerenciarTempoDesconto4").val()
        if(!desc){
            $("#sessao-gerenciarvalordesconto4-disp").val("")
            $(".desconto-valor").css('display','none')
            $("#sessao-gerenciarvalor2-disp").val("")
        } else {
            $(".desconto-valor").css('display','block')
        }
    }
    function gerenciarCarencia(){
        let opcao = $('#inicioCarencia').val();

        if(opcao == 'sim'){
            $('.input-carencia').css('display','block')
        } else {
            $("#gerenciarTempoDesconto4").val(null)
            $("#sessao-gerenciarvalor2-disp").val("")
            $('.input-carencia').css('display','none')
        }
    }
    gerenciarCarencia();
    gerenciarTempo();
</script>

