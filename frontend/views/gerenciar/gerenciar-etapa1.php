<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

function getSelecione($opcao, $model){
    $p = Yii::$app->request->post('selecionado');
    if($opcao == 'não'){
        if($model->ultimaEtapa != 4 && ($p == $opcao || ($model->gerenciarData1 != $model->dataEtapa1)) )
            return 'selected';
    }
    if($opcao == 'sim'){
        if($p == $opcao || ($model->gerenciarData1 == $model->dataEtapa1))
        return 'selected';
    }
    return '';
}


?>
<style>
    .gerenciar-etapa1 .date-input {
        display: none;
    }
</style>
<?php $form = ActiveForm::begin([
    'id' => 'formAluno',
    'options' => ['enctype' => 'multipart/form-data'],
    'encodeErrorSummary' => false,
    'errorSummaryCssClass' => 'help-block',

  ]); ?>
<div class="container gerenciar-etapa1">
    <div class="row">
        <div class="col-md-12">
            <?= $this->render('../layouts/aviso') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
                Preencha os campos abaixo para prosseguirmos com a geração do contrato 
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            O futuro inquilino(a) preencheu o seguinte início do contrato <?= Yii::$app->formatter::asData($model->dataEtapa1) ?><br>
            Você concorda com o início desse contrato?<br>
            <select id="inicioContrato" name="selecionado" class="form-control">
                <option value="">--SELECIONE--</option>
                <option value="sim" <?= getSelecione('sim', $model) ?>>Sim</option>
                <option value="não" <?= getSelecione('não', $model) ?>>Não</option>
            </select>

            <div class="date-input">
                Escolha a data de início do contrato<br>
                <input type="text" name="Sessao[gerenciarData1]" class="form-control" id="datepicker" placeholder="DD/MM/AAAA" value="<?= $model->gerenciarData1 ? Yii::$app->formatter::asData($model->gerenciarData1) : Yii::$app->formatter::asData($model->dataEtapa1)  ?>" data-date-format="dd/mm/yyyy">
            </div> 

        </div>
    </div>
    <div class="form-group mt-5 center">
        <?= Html::submitButton('Próximo', ['class' => 'btn btn-01']) ?>
  	</div>
</div>

  <?php ActiveForm::end(); ?>
<script>
    $("#inicioContrato").change(function() {
        processar();
    })

    function processar(){
        let opcao = $("#inicioContrato").val();
        if(opcao == 'não'){
            $('.date-input').css('display','block')
            $("#datepicker").val("<?= $model->gerenciarData1 ? Yii::$app->formatter::asData($model->gerenciarData1) : Yii::$app->formatter::asData($model->dataEtapa1) ?>")
        } else {
            $("#datepicker").val("")
            $('.date-input').css('display','none')
        }
    }
</script>

<script>
    $('#datepicker').datepicker({
        daysOfWeekDisabled: [6,0],
        format: "dd/mm/yyyy",
        language: "pt-BR",
        orientation: 'auto',
        startDate: 'today',
        autoclose: true,
    });

</script>