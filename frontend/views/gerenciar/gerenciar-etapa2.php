<?php 
use kartik\number\NumberControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<?php $form = ActiveForm::begin([
    'id' => 'formAluno',
    'options' => ['enctype' => 'multipart/form-data'],
    'encodeErrorSummary' => false,
    'errorSummaryCssClass' => 'help-block',

]); ?>
<div class="container">
<?= $this->render('../layouts/aviso') ?>

    <div class="row">
        <div class="col-md-6">
            O futuro inquilino(a) informou que o valor do aluguel acordado é de <b><?= $model->valor2 ?></b> mas o valor original do anúncio é <b><?= $model->visita->anuncio->imovel->valor ?></b>, confirme o valor do aluguel acordado.
        </div>
        <div class="col-md-6">
            <input type="text" name="Sessao[gerenciarValor2]"class="form-control money"  value="<?= $model->gerenciarValor2 ?>" >
        </div>
      <div class="col-md-12">
        <div class="form-group center  mt-5 ">
            <a class="btn btn-01" href="<?= Url::toRoute(['gerenciar/etapa1', 'id' =>  $model->id]) ?>"  >Anterior</a>&nbsp;&nbsp;
            <button class="btn btn-01" id="submit">Continuar</button>
        </div>
      </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<script>
    $(document).ready(function() {
        $('#submit').click(() =>{
            $("form").submit();

            // let valor = $("#sessao-gerenciarvalor2-disp").val();
            // if(!valor){
            //     Swal.fire({
            //     title: 'Atenção!',
            //     text: 'Preencha o valor do aluguel',
            //     icon: 'warning',
            //     confirmButtonText: 'Ok!'
            //     })
            //     // e.preventDefault();
            // } else {
            //     Swal.fire({
            //     title: 'Tem certeza?',
            //     text: `O valor informado pelo inquilino(a) foi de <?= $model->valor2 ?> e o seu é de ${valor}. Você confirma o valor?`,
            //     icon: 'question',
            //     showCancelButton: true,
            //     cancelButtonColor: '#d33',
            //     confirmButtonColor: '#3085d6',
            //     cancelButtonText: 'Não! vou rever',
            //     confirmButtonText: `Sim! o valor é XX`,
            //     }).then((result) => {
            //         if (result.value) {
            //             $("form").submit();
            //         }
            //     })
            // }

        })

    });
  
</script>