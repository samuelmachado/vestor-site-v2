<?php

use common\models\TipoDocumento;
use kartik\number\NumberControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>
<style>
    .gerenciar-etapa1 .input-carencia, .desconto-valor {
        display: none;
    }
    .dropzone {
        border: #982557 2px dotted;
    }
    .x{
            z-index: 900;
            display: grid;
            grid-template: 1fr/auto;
            grid-column-gap: 8px;
            grid-auto-flow: column;
            position: fixed;
            left: 50%;
            bottom: 24px;
            -webkit-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            transform: translateX(-50%);
            padding: 8px;
            border-radius: 28px;
            /* background-color: #fff; */
            /* box-shadow: 0 2px 4px 0 rgba(0,0,0,.16); */
        }
    .dropzone {
        background: white;
        border-radius: 5px;
        border: 2px dashed #ff8800;
        border-image: none;
        /* max-width: 90%; */
        margin-left: auto;
        margin-right: auto;
        background: #fafafa;
    }
    .dz-message {
        color:#982557;
    }
    .white {
        color: white !important;
    }
</style>


<div class="container gerenciar-etapa1">
    <div class="row">
        <div class="col-md-12">
            Anexe seus documentos para continuarmos o processo
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <DIV id="dropzone">
                <label for="">CPF/RG</label>
                <FORM class="dropzone needsclick" id="uploadRgCPF" action="upload?id=<?= $model->id?>&tipo=<?= TipoDocumento::TIPO_RG_CPF?>">
                    <DIV class="dz-message needsclick">    
                    Clique aqui ou arraste os arquivos para essa caixa.<BR>
                    <SPAN class="note needsclick">Lembre-se de anexar um <strong>CPF/RG</strong> legíveis</SPAN>
                    </DIV>
                </FORM>
                <?php 
                   print '<table class="table table-striped table-bordered" id="docRGCPF">';
                   foreach($model->documentoRGCPF as $documento){ 
                       $tipo = substr($documento->arquivo, -3);                            
                       echo Yii::$app->fileTable->display($documento,"delete-doc?id=".$documento->id);
                    
                   }
                   print '</table>';
                   ?>
            </DIV>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <DIV id="dropzone">
                <label for="">Comprovante de endereço</label>
                <FORM class="dropzone needsclick" id="uploadCompEnd" action="upload?id=<?= $model->id?>&tipo=<?= TipoDocumento::TIPO_COMP_END?>">
                    <DIV class="dz-message needsclick">    
                    Clique aqui ou arraste os arquivos para essa caixa.<BR>
                    <SPAN class="note needsclick">Lembre-se de anexar um <strong>comprovante de endereço</strong> legível</SPAN>
                    </DIV>
                </FORM>
                <?php 
                   print '<table class="table table-striped table-bordered" id="docCompEnd">';
                   foreach($model->documentoCompEnd as $documento){ 
                       $tipo = substr($documento->arquivo, -3);                            
                       echo Yii::$app->fileTable->display($documento,"delete-doc?id=".$documento->id);
                    
                   }
                   print '</table>';
                   ?>
            </DIV>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <DIV id="dropzone">
                <label for="">Comprovante de renda</label>
                <FORM class="dropzone needsclick" id="uploadCompRenda" action="upload?id=<?= $model->id?>&tipo=<?= TipoDocumento::TIPO_COMP_RENDA?>">
                    <DIV class="dz-message needsclick">    
                    Clique aqui ou arraste os arquivos para essa caixa.<BR>
                    <SPAN class="note needsclick">Lembre-se de anexar um <strong>comprovante de renda</strong> legível</SPAN>
                    </DIV>
                </FORM>
                <?php 
                   print '<table class="table table-striped table-bordered" id="docCompRenda">';
                   foreach($model->documentoCompRenda as $documento){ 
                       $tipo = substr($documento->arquivo, -3);                            
                       echo Yii::$app->fileTable->display($documento,"delete-doc?id=".$documento->id);
                    
                   }
                   print '</table>';
                   ?>
            </DIV>
        </div>
    </div>
    <div class="form-group mt-5 x">
        <button id="finalizar" class="btn btn-01 pf-3 "  style="box-shadow: 0 2px 4px 0 rgba(0,0,0,.16); ">Finalizar Processo</button>
  	</div>
</div>






<script>
var p = <?= isset($_GET['id']) ? $_GET['id'] : '0' ?>;

function addRow(div, item){
    $('#'+div).append(
            `<tr>
                <td>
                    <a href="${item.arquivo}" target="_blank">${item.nome}</a>
                </td>
                <td align="center" filename="delete-doc?id=${item.id}" class="file-thumb" delete-url="delete-doc?id=${item.id}" >
                    <i class="fa fa-2x fa-times xclose"></i>
                </td>
            </tr>`
        )
}
Dropzone.options.uploadRgCPF = {
 success: function(file, done) {
    console.log(done)
    done.forEach((item) => {
      addRow('docRGCPF', item)
    })
  }
 }


 Dropzone.options.uploadCompEnd = {
 success: function(file, done) {
    console.log(done)
    done.forEach((item) => {
      addRow('docCompEnd', item)
    })
  }
 }

 Dropzone.options.uploadCompRenda = {
 success: function(file, done) {
    console.log(done)
    done.forEach((item) => {
      addRow('docCompRenda', item)
    })
  }
 }



 $("#finalizar").click(function() {
    let docRgCpf = $('#docRGCPF tr').length;
    let docCompEnd = $('#docCompEnd tr').length;
    let docCompRenda = $('#docCompRenda tr').length;
    console.warn(docRgCpf, docCompEnd, docCompRenda)
    if(docRgCpf == 0)
        return Swal.fire(
            'Faltou o RG/CPF',
            'Não se esqueça de anexar os documentos 😁',
            'warning'
        );
    if(docCompEnd == 0)
        return Swal.fire(
            'Faltou o comprovante de endereço',
            'Não se esqueça de anexar os documentos 😁',
            'warning'
    );
    if(docCompRenda == 0)
        return Swal.fire(
            'Faltou o comprovante de renda',
            'Não se esqueça de anexar os documentos 😁',
            'warning'
    );
     
    return Swal.fire({
        title: 'Atenção!',
        html: "<u>Não será possível anexar mais arquivos</u> até que a nossa análise esteja completa",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Finalizar',
        cancelButtonText: 'Voltar mais tarde',
    }).then((result) => {
        if (result.value) {
            finalizarProcesso({id: p}).then(response => {
                
                return Swal.fire({
                    title:'',
                    html:'Agora é só aguardar 😁',
                    icon:'success',
                    timer: 2000,
                    timerProgressBar: true,
                  }).then(() => {
                    window.location.href = '../imoveis/aguarde?tipo=3'
                  })
            });
        }
    })
 })
</script>
