
<style>
        .orange {
        color: #ff8800;
        background: transparent;
    }
       .js-choose:hover {
    background-color:#ff8800 !important;
    color: white !important;
    }
    .icones {
        max-width: 76%;
    }
    .bg-rounded {
        background:#982557 !important;
        padding:20px;
        color:#fff;
        font-weight: bold;
    }
    .hide {
        display: none;
    }
    .btn-minha-visita {
        cursor: pointer;
        color:#982557;
    }
</style>
<?php
// foreach($imoveis as $imovel) { 
//                                     print_r($imovel['sessoes']);
//                                     print '<br><br>';                                 
//                               } 
?>
<section class="property-grid-area property-grid-3-area   mt-50 pb-120">
        <div class="container-fluid" >
            <div class="row justify-content-center">
                <?= $this->render('../layouts/include-menu') ?>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="row mt-3 ml-5">
                            <h4>Alugando</h4>
                        </div>
                        <div class="listing-item mt-40">
                                <div class="listing-content white-bg conteudo">
                                    <div class="row mt-3 ml-5">
                                    <?php 
                                        if(!$imoveisAlugando){
                                            print '
                                            <div class="alert alert-secondary" role="alert">
                                                <h6>
                                                    Você não tem nenhum processo de locação ativo.
                                                </h6>
                                            </div>
                                            ';
                                        }                                        
                                    ?>
                                    <?php foreach($imoveisAlugando as $sessao) { ?>
                                        <?= $this->render('imoveis-alugando', [
                                            'imovel' => $sessao->visita->anuncio->imovel,
                                            'sessao' => $sessao,
                                            'tipo' => 'inquilino'
                                        ]) ?>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row mt-3 ml-5">
                                <h4>Meus imóveis</h4>
                            </div>
                            <div class="listing-item mt-40">
                                <div class="listing-content white-bg conteudo">
                                    <div class="row mt-3 ml-5">
                                        <?php 
                                            if(!$meusImoveis){
                                                print '
                                                <div class="alert alert-secondary" role="alert">
                                                    <h6>
                                                        Você não tem nenhum imóvel cadastrado.
                                                    </h6>
                                                </div>
                                                ';
                                            }                                        
                                        ?>
                                        <?php foreach($meusImoveis as $imovel) { ?>
                                            <?= $this->render('meus-imoveis', [
                                                'imovel' => $imovel['imovel'],
                                                'sessoes' => $imovel['sessoes'],
                                                'tipo' => 'proprietario'
                                            ]) ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?= $this->render('../layouts/modal') ?>

    <script>
        var dataVisita = null;
        var horaVisita = null;
        var imovel = null;
        var usuario = null;
        var telefone = null;
        var idVisita =  null;
        $(function() {
            $(".showReagendar").click(function() {
                idVisita = $(this).attr('visita');
                viewImovel({id: $(this).attr('imovel')}).then((response) => {
                    imovel = response.data
                    proximaEtapa(1)
                });
                // loadPartial({
                //         partial: 'model-confirmacao.php',
                //         data: null,
                //         divResponse: '.modal-content'
                // })
                $('#extraLargeModal').modal('show');
            })
        });
      function proximaEtapa(proximaEtapa){
            usuario = JSON.parse(localStorage.getItem('usuario'));
            $(".modal-content").html("")

            if(proximaEtapa == 1) {
                loadPartial({ 
                    partial: 'model-agendamento-data.php',
                    data: {imovel: imovel},
                    divResponse: '.modal-content'
                })
            }
            if1: if(proximaEtapa == 2){          
                // Se já existe conta e está logado
                if(usuario && usuario.authKey){
                    proximaEtapa = 4
                    break if1;
                }
                
                loadPartial({
                    partial: 'model-acesse-ou-crie.php',
                    data: null,
                    divResponse: '.modal-content'
                })
            }
            if(proximaEtapa == 3){
                loadPartial({
                    partial: 'model-pin.php',
                    data: null,
                    divResponse: '.modal-content'
                })
            }
            if2: if(proximaEtapa == 4){
                // Se existe usuário e ele não tem conta
                    if(usuario && usuario.cadastroFinalizado == 1){
                    console.warn('ooooi entrou');
                    proximaEtapa = 0
                    break if2;
                }
                loadPartial({
                    partial: 'model-confirmacao.php',
                    data: {editar: idVisita},
                    divResponse: '.modal-content'
                })
            }

            if(proximaEtapa == 0){
                loadPartial({
                    partial: 'model-cadastre-se.php',
                    data: null,
                    divResponse: '.modal-content'
                })
            }
            console.warn('NEXT')
        }
        // if(imovel.gerenciarBotao == 1){
        //     $("#avaliar").show();
        //     $("#alugar").show();
        // } else {
        //     $("#reagendar").show();
        //     $("#cancelar").show();
        // }

        $(".cancelar").click(function() {
          
    
            Swal.fire({
                title: 'Cancelar visita',
                text: "Deseja realmente cancelar a visita?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Sim',
                cancelButtonText: 'Não',
                reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        cancelarVisita({id: $(this).attr('visita')})
                        .then(
                        response => {
                            if(response.data.status){
                                Swal.fire(
                                'Cancelado!',
                                'A visita foi cancelada',
                                'success'
                                )
                                // location.reload()
                                $(this).closest( ".visita" ).remove()
                                // $( this ).parent().parent().parent().remove();
                            } else {
                                Swal.fire(
                                'Erro!',
                                'Não foi possível cancelar a visita',
                                'error'
                                )
                            }
                        },
                        error => {
                            console.log(error);
                        }
                        );
                    }
                })
        })
    </script>