<?php
use yii\helpers\Url;

?>
<div class="row visita">
    <div class="col-md-12" style="background: red">
        <div class="row mt-3 ml-5">
            <div class="row">
                <div class="col-md-8">
                        <img src="imgs/casa.jpg" style="border-radius: 30px;" alt="">
                </div>
                <div class="col-md-4 ">
                    <div class="bg-rounded listed-box white-bg box mt-80" style="border-radius: 30px;background: white;;">
                    <?= $imovel->enderecoPadrao ?>
                    </div>
                    <div style="padding-left:30px;padding-right:30px;padding-top:5px;">
                      <?php
                        if(!$sessao){
                            print '<h6>Não há nenhum processo de locação ativo</h6>';
                        } 
                        if($sessao->ultimaEtapa >= 4 && $sessao->ultimaEtapa < 9){
                            print '<h6>Aguarde o proprietário fazer a parte dele.</h6>';
                        } else {
                            $url = Yii::$app->LocacaoUrl::inquilino($sessao->ultimaEtapa, $sessao->id);
                            
                            ?>
                            <b class="btn-minha-visita" ><a  class="btn-minha-visita" href="<?=  Url::toRoute($url);  ?>">Continuar Processo</a></b>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 