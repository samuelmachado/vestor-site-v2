<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <?php $form = ActiveForm::begin([
            'id' => 'formAluno',
            'options' => ['enctype' => 'multipart/form-data'],
            'encodeErrorSummary' => false,
            'errorSummaryCssClass' => 'help-block',

        ]); ?>
        Considerações importantes<br>
        • O pagamento é feito todo dia 15 de cada mês<br>
        • Enviaremos as informações da proposta para o futuro inquilino(a) analisar<br><br><br>
        <B>Início do contrato:</B> <?= Yii::$app->formatter::asData($model->gerenciarData1) ?><br>
        <B>Fim do contrato:</B> <?php  $p = Yii::$app->formatter::addMonths(30,$model->gerenciarData1); echo Yii::$app->formatter::asData($p); ?><br>
        <B>Duração:</B> 30 meses<br>
        <B>Valor do Aluguel:</b> <?= Yii::$app->formatter::asBRL($model->gerenciarValor2) ?><br>
        <?php
            '<B>Desconto: '.$model->hasCarencia().'</B>';
        ?><br>
        <?php 
            '<B>Desconto: '.$model->hasDesconto().'</B>';
        ?><br>
        <div class="form-group center mt-5">
            <a class="btn btn-01" href="<?= Url::toRoute(['gerenciar/etapa4', 'id' =>  $model->id]) ?>"  >Anterior</a>&nbsp;&nbsp;
            <?= Html::submitButton('Finalizar', ['class' => 'btn btn-01']) ?>
        </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>