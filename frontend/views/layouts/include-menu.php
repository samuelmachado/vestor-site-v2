<?php
use yii\helpers\Url;
?>
<div class="col-lg-3 col-md-7 col-sm-9 bg_side" style="padding: 0px 0px 0px 0px;" >
    <div class="property-sidebar">
        <div class="city-box box mt-30 ml-10 corner-orange">
            <div class="city-item mt-25">
                <ul>
                    <li>
                        <a href="<?= Url::to(['perfil/index']) ?>">
                            <h4 class="title-vestor">
                                MEU PERFIL
                            </h4>
                        </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['favoritos/index']) ?>">
                            <h4 class="title-vestor">
                                FAVORITOS
                            </h4>
                        </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['minhas-visitas/index']) ?>">
                            <h4 class="title-vestor">
                                MINHAS VISITAS
                            </h4>
                        </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['gerenciar/index']) ?>">
                            <h4 class="title-vestor">
                                MEUS IMÓVEIS
                            </h4>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="menu-fundo">
        </div>
    </div>
</div>