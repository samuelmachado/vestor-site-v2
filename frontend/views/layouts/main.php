<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title>Véstor</title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>
<header class="header-area header-page ">
        <div class="navigation">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand" href="index.html">
                                <img src="<?= \Yii::$app->request->baseUrl ?>/imgs/véstor.png" style="padding-left:10px; width:100px; height:30px;" alt="Logo">
                            </a> <!-- logo -->

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarFive" aria-controls="navbarFive" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                            </button> <!-- navbar toggler -->
                            <div class="collapse navbar-collapse sub-menu-bar" id="navbarFive" >
                                    <div class="col-md-6 offset-md-3 ">
                                        <div class="input-group">
                                            <input type="text" class="form-control border-input" placeholder="BUSQUE AQUI SEU IMÓVEL">
                                            <button><img class="img-responsive lupa" src="<?= \Yii::$app->request->baseUrl ?>/imgs/icon_lupa.svg"></button>
                                          </div>
                                    </div>                 
                            </div>

                            <div class="navbar-btn d-none d-sm-inline-block">
                                <!-- <a class="main-btn ml-20" href="#">SUbmit property</a> -->
                                <button class="btn btn-login" id="login">LOGIN</button>
                                <a class="btn btn-login" id="perfil" style="display: none; color:#fff; cursor:pointer;" href="<?= Url::to(['perfil/index']) ?>"  >MEU PERFIL</a>
                            </div>
                        </nav> <!-- navbar -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div>
        <!-- <br> -->
    </header>
    <div class="wrap" > 
    <br><br>  <br><br>
        <br><br>
       <?= $content ?>
      
    </div>
<?php $this->endBody() ?>
<footer class="footer-area mt-10">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 order-2 order-lg-1 col-md-6 col-sm-9">
                    <div class="">
                        <h4 class="title"><img src="<?= \Yii::$app->request->baseUrl ?>/imgs/véstor.png" style="width: 50%;"></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-copyright d-block d-sm-flex justify-content-between align-items-center">
                            <p class="text-center title-3">© Véstor Ltda • Todos os direitos reservados • CRECI</p>
                            <ul>
                                <li><a href="#"><i class="fab fa-facebook-f  gray"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter gray"></i></a></li>
                                <li><a href="#"><i class="fab fa-behance gray"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in gray"></i></a></li>
                                <li><a href="#"><i class="fab fa-youtube gray"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>

<?php $this->endPage() ?>


<script>
    let token = localStorage.getItem('token');
    if(token){
        $('#perfil').show();
        $('#login').hide();
    }
    // $('.money').mask('000.000.000.000.000,00', {reverse: true});
    $( document ).ready(function() {
        
});

</script>