
 <div class="row">
	<div class="col-md-12">
		<p>Preencha os campos abaixo para prosseguirmos com a geração do contrato.</p>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<?php if(\Yii::$app->session->hasFlash('flashMessage')): ?>
			<div class="alert alert-danger" role="alert">
				<?= \Yii::$app->session->getFlash('flashMessage') ?>
			</div>
		<?php endif; ?>
	</div>
</div>