<?php 
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>



<?php $form = ActiveForm::begin([
    'id' => 'formAluno',
    'options' => ['enctype' => 'multipart/form-data'],
    'encodeErrorSummary' => false,
    'errorSummaryCssClass' => 'help-block',

  ]); ?>
	<div class="container">
		<?= $this->render('../layouts/aviso') ?>
		<div class="row">
			<div class="col-md-4">
			<p><b>Escolha a data de início do contrato</b></p><br>
				<div class="input-group date">
			
					<input type="text" name="Sessao[dataEtapa1]"class="form-control" id="datepicker" placeholder="DD/MM/AAAA" value="<?= $model->dataEtapa1 ?>"data-date-format="dd/mm/yyyy">
				</div>
			</div>
		</div>
    </div>
	<div class="form-group center" >
		<a class="btn btn-01" href="<?= Url::toRoute(['minhas-visitas/index']) ?>"  >Voltar</a>'
		<?= Html::submitButton('Continuar', ['class' => 'btn btn-01 pull-right']) ?>
	</div>
	  
  <?php ActiveForm::end(); ?>

    <script>
		$('#datepicker').datepicker({
			format: "dd/mm/yyyy",
			language: "pt-BR",
			orientation: 'auto',
			startDate: 'today',
			autoclose: true,
		});

    </script>