<?php 
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      Considerações importantes<br>
    • O pagamento é feito todo dia 15 de cada mês<br>
    • Enviaremos as informações da proposta para o proprietário analisar<br>
    <hr>      
    <B>Início do contrato:</B> <?= Yii::$app->formatter::asData($model->dataEtapa1) ?><br>
    <B>Fim do contrato:</B> <?php  $p = Yii::$app->formatter::addMonths(30,$model->dataEtapa1); echo Yii::$app->formatter::asData($p); ?><br>
    <B>Duração</B> 30 meses<br>
    <B>Valor do Aluguel</b> <?= Yii::$app->formatter::asBRL($model->valor2) ?>
    <br>
    <?php $form = ActiveForm::begin([
        'id' => 'formAluno',
        'options' => ['enctype' => 'multipart/form-data'],
        'encodeErrorSummary' => false,
        'errorSummaryCssClass' => 'help-block',

      ]); ?>
          <div class="form-group center mt-30">
            <a class="btn btn-01" href="<?= Url::toRoute(['imoveis/etapa2', 'id' =>  $model->id]) ?>"  >Anterior</a>'
            <?= Html::submitButton('Próximo', ['class' => 'btn btn-01']) ?>
          </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>