  
    <style>
        .white-bg {
            background: transparent;
        }

        .x{
            display: grid;
            grid-template: 1fr/auto;
            grid-column-gap: 8px;
            grid-auto-flow: column;
            position: fixed;
            left: 50%;
            bottom: 24px;
            -webkit-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            transform: translateX(-50%);
            padding: 8px;
            border-radius: 28px;
            background-color: #fff;
            box-shadow: 0 2px 4px 0 rgba(0,0,0,.16);
        }

        .my-float{
            margin-top:22px;
        }
        .orange {
            color: #ff8800;
            background: transparent;
        }

    </style>
    <style>
     
    </style>
        <!--====== PROPERTY PART START ======-->
    
        <section class="property-grid-area mt-50">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row mt-100">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="icon_fotos col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                    <img class="icon-header" src="../imgs/visualizar_imovel/icon_fotos.svg" class="img-responsive" alt="">
                                    <div class="icon-header-text">Fotos</div>
                                </div>
                                <div class="icon_360 col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                    <img class="icon-header " src="../imgs/visualizar_imovel/icon_360.svg" class="img-responsive" alt="">
                                    <div class="icon-header-text">360º</div>
                                </div>
                                <div class="icon_mapa col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                    <img class="icon-header " src="../imgs/visualizar_imovel/icon_mapa.svg" class="img-responsive" alt="">
                                    <div class="icon-header-text">Mapa</div>
                                </div>
                                <div class="icon_vistarua col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                    <img class="icon-header " src="../imgs/visualizar_imovel/icon_vistarua.svg" class="img-responsive" alt="">
                                    <div class="icon-header-text">Vista da rua</div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3 pb-100">
                        <div class="row">
                            <div class="col-md-8">
                                <img src="../imgs/casa.jpg" style="border-radius: 30px;" alt="">
                                <div class="property-top-text mt-20 corner-orange">
                                    <div class="pl-10">
                                        <h3 class="subtitle-vestor">Apartamento em Santana</h3>
                                        <h3 class="subtitle-vestor">São José dos Campos</h3>
                                        <span class="text">Rua das Peônias</span>
                                    </div>
                                </div>

                                <div class="spacific-details-item white-bg mt-30" style="background: white !important;">
                                    <div class="item-title">
                                        <h3 class="h3-vestor">+ Detalhes</h3>
                                    </div>
                                    <div class="spacific-details-list">
                                        <div class="row">
                                            <div class="col-lg-6 col-6">
                                                <div class="list d-flex">
                                                    <ul class="ml-40">
                                                        <li><span>Armários</span></li>
                                                        <li><span>Suíte</span></li>
                                                        <li><span>Banheiros com box</span></li>
                                                        <li><span>Suíte</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-6">
                                                <div class="list d-flex">
                                                    <ul class="ml-40">
                                                        <li><span>Armários</span></li>
                                                        <li><span>Suíte</span></li>
                                                        <li><span>Banheiros com box</span></li>
                                                        <li><span>Suíte</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="spacific-details-item white-bg mt-30" style="background: white !important;">
                                    <div class="item-title">
                                        <h3 class="h3-vestor">O que eu preciso para alugar este Imóvel?</h3>
                                    </div>
                                    <div class="spacific-details-list">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="list d-flex">
                                                    <ul class="ml-40">
                                                        <li><span>RG</span></li>
                                                        <li><span>CPF</span></li>
                                                        <li><span>Comprovante de residência</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-12  text-center mt-20">
                                                <button class="btn btn-02 " style="width: auto;">Renda maior ou igual a R$ 3700,00</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="listed-box white-bg box mt-80" style="border-radius: 30px;background: white;;">
                                    <div class="title-item" >
                                        <H5 class="subtitle-vestor text-center codigo" ></H5>
                                        <h5 class="text-center pt-10">Aluguel</h5>
                                        <h3 class="center orange aluguel" style="font-size: 50px;"></h3>
                                    </div>
                                    <div class="listed-list pl-60 pt-20" style="margin-top:0px; border-top:none;">
                                        <ul>
                                            
                                            <li class="condominio"></li>
                                            <li class="iptu"></li>
                                            <li class="seguro-incendio"></li>
                                        </ul>
                                    
                                    </div>
                                    <div class="listed-list" style="margin-top: 0px;">
                                        <ul>
                                          <li><hr></li>  

                                            <li class="title-2 text-center">R$ 2700,00</li>
                                        </ul>
                                    </div>
                                    
                                    <div class="pb-20 text-center ">
                                        <button class="btn btn-01 show" >AGENDAR VISITA</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
        <section class="banner-area bg_cover degrade pb-50 pt-20">
            <div class="container">
                <h3 class="text-white pt-50 pt text-center">
                  Aqui tem um carousel top<br />
               
                </h3>
              </div>
              <div class="container">
               
                
              </div>
        </section>
    </section>
    
    <!--====== PROPERTY PART ENDS ======-->
 
    <!--====== FOOTER PART START ======-->
    
 
    <div href="#" class="x" style="    align-items: center;z-index:900;">
        <i class="favorito pl-1 pr-1 far fa-heart fa-3x" style="color:#FF8800;cursor:pointer;"></i>
        <button class="btn btn-01 text-center show" >AGENDAR VISITA</button>
        <i class="pl-1 pr-1 fab fa-whatsapp fa-3x" style="color:#FF8800"></i>

    </div>
    <?= $this->render('../layouts/modal') ?>


        
    <style>
      
    </style>

    <script type="text/javascript">
        var dataVisita = null;
        var horaVisita = null;
        var imovel = null;
        var usuario = null;
        var telefone = null;
 

        $(".favorito").click(function() {
            let favoritos = obterFavoritos()
            if(favoritos){
                let esteImovel = favoritos.find((item) => item.id == imovel.imovel.id)
                if(esteImovel){
                    favoritos = favoritos.filter((item) => item.id != imovel.imovel.id)
                    $(`.favorito`).removeClass('fas')
                    $(`.favorito`).addClass('far')
                } else {
                    $(`.favorito`).removeClass('far')
                    $(`.favorito`).addClass('fas')
                    favoritos.push(imovel.imovel)
                }
            } else {
                $(`.favorito`).removeClass('far')
                $(`.favorito`).addClass('fas')
                favoritos = [imovel.imovel];
            }
            salvarFavoritos(favoritos)
        });

        $(".show").click(() => {
            proximaEtapa(1)
            // loadPartial({
            //         partial: 'model-confirmacao.php',
            //         data: null,
            //         divResponse: '.modal-content'
            // })
            $('#extraLargeModal').modal('show');
        })
        

        function proximaEtapa(proximaEtapa){
            usuario = JSON.parse(localStorage.getItem('usuario'));
            $(".modal-content").html("")

            if(proximaEtapa == 1) {
                loadPartial({
                    partial: 'model-agendamento-data.php',
                    data: {imovel:imovel},
                    divResponse: '.modal-content'
                })
            }
            if1: if(proximaEtapa == 2){          
                // Se já existe conta e está logado
                if(usuario && usuario.authKey){
                    proximaEtapa = 4
                    break if1;
                }
                 
                loadPartial({
                    partial: 'model-acesse-ou-crie.php',
                    data: null,
                    divResponse: '.modal-content'
                })
            }
            if(proximaEtapa == 3){
                loadPartial({
                    partial: 'model-pin.php',
                    data: null,
                    divResponse: '.modal-content'
                })
            }
            if2: if(proximaEtapa == 4){
                // Se existe usuário e ele não tem conta
                    if(usuario && usuario.cadastroFinalizado == 1){
                    console.warn('ooooi entrou');
                    proximaEtapa = 0
                    break if2;
                }
                loadPartial({
                    partial: 'model-confirmacao.php',
                    data: null,
                    divResponse: '.modal-content'
                })
            }

            if(proximaEtapa == 0){
                loadPartial({
                    partial: 'model-cadastre-se.php',
                    data: null,
                    divResponse: '.modal-content'
                })
            }
            console.warn('NEXT')
        }

        function getImovel() {
            console.warn('***** getImovel() *****')
            var param = getParams()
            // Se o imóvel no localstorage não é o mesmo que o usário abriu
            viewImovel({id: param.id}).then((response) => {
                imovel = response.data
                localStorage.setItem('imovelAtivo', JSON.stringify(imovel))
                $(".codigo").text(`CÓD ${imovel.imovel.id}`)
                $(".aluguel").text(`R$ ${imovel.imovel.valor}`)
                $(".condominio").html(`<span class="orange" style="font-weight: bold;">+</span> Condomínio: R$ ${imovel.imovel.valorCondominio}`)
                $(".iptu").html(`<span class="orange" style="font-weight: bold;">+</span> IPTU: R$ ${imovel.imovel.valorIPTU}`)
                $(".seguro-incendio").html(`<span class="orange" style="font-weight: bold;">+</span> Seguro Incêndio: R$ ${imovel.imovel.valorSeguroIncendio}`)
                $(".m2").text(`${imovel.imovel.area}m²`)
                $(".quartos").text(`${imovel.imovel.quarto} quartos`)
                $(".vagas").text(`${imovel.imovel.vagaGaragem} vagas`)
                let favoritos = obterFavoritos()
                if(favoritos){
                    let esteImovel = favoritos.find((item) => item.id == imovel.imovel.id)
                    if(esteImovel){
                        $(`.favorito`).removeClass('far')
                        $(`.favorito`).addClass('fas')
                    }
                    console.warn(favoritos)
                }
            }).catch((e) => {
                console.log(e)
            }); 
        }
        $(function() {
            imovel = JSON.parse(localStorage.getItem('imovelAtivo'));
            getImovel()

            console.warn(imovel)
      
        });
    </script>