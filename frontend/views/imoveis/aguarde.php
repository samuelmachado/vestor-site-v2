<?php
use yii\helpers\Url;
?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
        <div class="warning alert-danger center" role="alert">
            <h6>
                <?php
                if(isset($_GET['tipo'])){
                    switch($_GET['tipo']){
                        case 1: print 'Aguarde a resposta do proprietário'; break;
                        case 2: print 'Aguarde a resposta do futuro inquilino'; break;
                        case 3: print 'Aguarde a resposta dos nossos agentes'; break;
                        default: print 'aguarde'; break;
                    }
                }
                ?>
            </h6>
        </div>
        <div class="form-group center mt-30">
            <a class="btn btn-01" href="<?= Url::toRoute(['gerenciar/index']) ?>"  >Ir para meus imóveis</a>
        </div>
    </div>
  </div>
</div>