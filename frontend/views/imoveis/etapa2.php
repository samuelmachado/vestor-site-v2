<?php 
use kartik\number\NumberControl;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php $form = ActiveForm::begin([
    'id' => 'formAluno',
    'options' => ['enctype' => 'multipart/form-data'],
    'encodeErrorSummary' => false,
    'errorSummaryCssClass' => 'help-block',

  ]); ?>
<div class="container">
    <?= $this->render('../layouts/aviso') ?>
    <div class="row">
        <div class="col-md-12">
            <p><b>O valor do aluguel do anúncio é de <?= $model->visita->anuncio->imovel->valor ?> Confirme o valor do aluguel combinado na visita:</b></p><br>
        </div>
        <div class="col-md-6">
            <div class="input-group date">
                <input type="text" name="Sessao[valor2]"class="form-control money"  value="<?= $model->valor2 ?>"data-date-format="dd/mm/yyyy">
            </div>
        </div>
    </div>
</div>
<div class="form-group center mt-30" >
    <a class="btn btn-01" href="<?= Url::toRoute(['imoveis/etapa1', 'id' =>  $model->idVisita]) ?>"  >Anterior</a>'
    <?= Html::submitButton('Próximo', ['class' => 'btn btn-01']) ?>
</div>
<?php ActiveForm::end(); ?>