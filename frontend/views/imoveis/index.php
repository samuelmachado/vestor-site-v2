<?php
use yii\helpers\Html;
use kartik\select2\Select2;
use common\models\CEP;
use yii\helpers\ArrayHelper;
?>
    <style type="text/css">
        .orange {
            color: #ff8800;
            background: transparent;
        }
       .js-choose:hover {
    background-color:#ff8800 !important;
    color: white !important;
    }
    .icones {
        max-width: 76%;
    }
    </style>
  

    <section class="property-grid-area property-grid-3-area   mt-50 pb-120">
        <div class="container-fluid" >
            <div class="row justify-content-center">
                <div class="col-lg-3 col-md-7 col-sm-9 bg_side" style="padding: 0px 0px 0px 0px;" >
                <?= Html::beginForm(['imoveis/index'], 'GET', ['id' => 'formFilter']); ?>
                    <div class="property-sidebar">
                      
                        <div class="city-box box mt-30 corner-orange">
                            <div class="city-item mt-25">
                                <form action="#">
                                    <h4 class="title-vestor">ONDE</h4>
                                    <div class="input-box">
                                    <?= Select2::widget([
                                        'name' => 'bairro',
                                        'data' => ArrayHelper::map(CEP::getAll(), 'bairro.id', 'bairro.nome','cidade.nome'),
                                        'options' => [
                                            'placeholder' => 'Selecione o bairro',
                                            'multiple' => true,
                                        ],
                                    ]);
                                    ?>
                                    </div>
                                </form>
                            </div>
                            <div class="city-item mt-25">
                                <form action="#">
                                    <h4 class="title-vestor">TIPO DE IMÓVEL</h4>
                                    <div class="input-box">
                                        <input type="text" class="input-vestor" placeholder="Selecione">
                                    </div>
                                </form>
                            </div>
                            <div class="city-item mt-25">
                                <form action="#">
                                    <h4 class="title-vestor">VALOR</h4>
                                    <div class="input-box">
                                        <input type="number" name="valor_minimo" class="input-vestor" placeholder="Mínimo">
                                        <input type="number" name="valor_maximo"class="input-vestor" placeholder="Máximo">
                                    </div>
                                </form>
                            </div>
                        </div>
                    <?php Html::endForm(); ?>
                        <div class="list-box box mt-30 corner-orange">
                            <div class="title-item">
                                <h4 class="title-vestor">QUARTOS</h4>
                            </div>
                            <div class="list-item mt-15">
                                <div class="form__pills js-parking-spots-quantity">
                                    <ul class="filter-pills js-container">
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="1">1+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="2">2+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="3">3+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="4">4+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="5">5+</button>
                                      </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="list-box box mt-30 corner-orange">
                            <div class="title-item">
                                <h4 class="title-vestor">BANHEIROS</h4>
                            </div>
                            <div class="list-item mt-15">
                                <div class="form__pills js-parking-spots-quantity">
                                    <ul class="filter-pills js-container">
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="1">1+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="2">2+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="3">3+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="4">4+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="5">5+</button>
                                      </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="list-box box mt-30 corner-orange">
                            <div class="title-item">
                                <h4 class="title-vestor">VAGAS</h4>
                            </div>
                            <div class="list-item mt-15">
                                <div class="form__pills js-parking-spots-quantity">
                                    <ul class="filter-pills js-container">
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="1">1+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="2">2+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="3">3+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="4">4+</button>
                                      </li>
                                      <li class="filter-pills__item">
                                        <button class="filter-pills__button js-choose" type="button" data-value="5">5+</button>
                                      </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="list-box box mt-30 corner-orange">
                            <div class="title-item">
                                <h4 class="title-vestor">DETALHES</h4>
                            </div>
                            <div class="list-item mt-15">
                                <ul>
                                    <!-- <li><a href="#"><span>Popular</span> <span>(10)</span></a></li> -->
                                    <li><a href="#"><span>Armários embutidos</span> </a></li>
                                    <li><a href="#"><span>Suíte</span> </a></li>
                                    <li><a href="#"><span>Armários embutidos</span> </a></li>
                                    <li><a href="#"><span>Suíte</span> </a></li>
                                    <li><a href="#"><span>Armários embutidos</span> </a></li>
                                    <li><a href="#"><span>Suíte</span> </a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="menu-fundo">
                        </div>
                        <button type="submit"c v>Buscar</button>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="property-grid-item conteudo" >
                            <div class="col-lg-9">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="property-grid-item">
                                <?php foreach($model as $m):?>
                                <div class="row justify-content-center">
                                    <div class="col-md-12">
                                        <div class="listing-item mt-40">
                                            <div class="listing-content white-bg">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="row">
                                                                    <div class="icon_fotos col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                                                        <img class="icon-header" src="../v2/imgs/visualizar_imovel/icon_fotos.svg" class="img-responsive" alt="">
                                                                        <div class="icon-header-text">Fotos</div>
                                                                    </div>
                                                                    <div class="icon_360 col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                                                        <img class="icon-header " src="../v2/imgs/visualizar_imovel/icon_360.svg" class="img-responsive" alt="">
                                                                        <div class="icon-header-text">360º</div>
                                                                    </div>
                                                                    <div class="icon_mapa col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                                                        <img class="icon-header " src="../v2/imgs/visualizar_imovel/icon_mapa.svg" class="img-responsive" alt="">
                                                                        <div class="icon-header-text">Mapa</div>
                                                                    </div>
                                                                    <div class="icon_vistarua col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                                                        <img class="icon-header " src="../v2/imgs/visualizar_imovel/icon_vistarua.svg" class="img-responsive" alt="">
                                                                        <div class="icon-header-text">Vista da rua</div>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-3">
                                                            <div class="row">
                                                                <div class="col-md-8">
                                                                    <img src="imgs/casa.jpg" style="border-radius: 30px;" alt="">
                                                                </div>
                                                                <div class="col-md-4 ">
                                                                    <div class="listed-box white-bg box mt-80" style="border-radius: 30px;background: white;;">
                                                                        <div class="title-item" >
                                                                            <H5 class="subtitle-vestor text-center" >CÓD <?=$m->id?></H5>
                                                                            <h5 class="text-center pt-10">Aluguel</h5>
                                                                            <h3 class="center orange">R$ <?=$m->valor?></h3>
                                                                        </div>
                                                                        <div class="listed-list" style="margin-top:0px; border-top:none;">
                                                                            <ul>
                                                                                <li><span class="orange" style="font-weight: bold;">+</span> Condomínio: R$ <?=$m->valorCondominio?></li>
                                                                                <li><span class="orange" style="font-weight: bold;">+</span> IPTU: R$ <?= $m->valorIPTU?></li>
                                                                                <li><span class="orange" style="font-weight: bold;">+</span> Seguro Incêndio: R$ <?=$m->valorSeguroIncendio?></li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="listed-list" style="margin-top: 0px;">
                                                                            <ul>
                                                                                <li class="title-2 text-center">R$ <?=$m->valor?></li>
                                                                            </ul>
                                                                        </div>
                                                                        
                                                                        <div class="mt-10 text-center ">
                                                                            <button class="btn btn-01 ">AGENDAR VISITA</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="list">
                                                                    <ul >
                                                                        <li><img src="imgs/icon_metragem.svg" class="text-center"><br><span><?= $m->area?>m²</span></li>
                                                                        <li><img src="imgs/icon_quarto.svg" class="text-center"><br><span><b><?= $m->quarto?> quartos</b></span></li>
                                                                        <li><img src="imgs/icon_vaga.svg" class="text-center"><br><span><b><?= $m->quarto?> vagas</b></span></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach;?>
                                <?php echo \yii\widgets\LinkPager::widget(['pagination' => $pages,]);?>
                            </div>
                        </div>
                    </div>
                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <div id="tmpDiv" style="display: none;"></div>



