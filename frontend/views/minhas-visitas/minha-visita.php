<?php
use yii\helpers\Url;

?>
<div class="row visita">
    <div class="col-md-12">
        <div class="row mt-3 ml-5">
            <div class="row">
                <div class="col-md-8">
                        <img src="../imgs/casa.jpg" style="border-radius: 30px;" alt="">
                </div>
                <div class="col-md-4 ">
                    <div class="bg-rounded listed-box white-bg box mt-80" style="border-radius: 30px;background: white;;">
                    <?= $visita->id ?>
                    <?= Yii::$app->dateCheck->diaSemana($visita->dataVisita) ?>, <?= Yii::$app->formatter::asData($visita->dataVisita) ?> às <?= Yii::$app->formatter::asHora($visita->hora) ?>
                    <?= $visita->anuncio->imovel->enderecoPadrao ?>
                    </div>
                    <div style="padding-left:30px;padding-right:30px;padding-top:5px;">
                        <?php
                        if (Yii::$app->dateCheck->pastDate(date('Y-m-d H:i:s'), $visita->dataVisita.' '.$visita->hora))
                        {
                        ?>
                            <b class="btn-minha-visita" id="avaliar" visita="<?= $visita->id ?>"  imovel="<?= $visita->anuncio->idImovel ?>">Avaliar</b>
                            <b class="btn-minha-visita float-right" id="alugar"><a  class="btn-minha-visita" href="<?= Url::toRoute(['imoveis/redirecionar', 'id' =>  $visita->id]) ?>">Alugar</a></b>
                            <?php } else { ?>
                            <b class="btn-minha-visita showReagendar" id="reagendar" visita="<?= $visita->id ?>"  imovel="<?= $visita->anuncio->idImovel ?>">Reagendar</b>
                            <b class="btn-minha-visita float-right cancelar"  visita="<?= $visita->id ?>">Cancelar</b>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    //     $("#datahora").html(`${getDay({dia: dataVisita, shortDay: false})} ${getDateBr(dataVisita)} às ${horaVisita}` )

</script>