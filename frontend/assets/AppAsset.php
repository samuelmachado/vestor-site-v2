<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
 
    public $css = [
        // 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
        'css/bootstrap.min.css',
        'css/site.css',
        'css/font-awesome5.css',
        'css/bootstrap-datepicker.standalone.min.css',
        'css/dropzone.min.css'
        // 'css/vue-select.css'
    ];
    public $js = [
        'js/jquery-3.3.1.min.js',
        'js/popper.min.js',
        'js/bootstrap.min.js',
        'js/sweetalert2@9.js',
        // 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
        'js/axios.min.js',
        'js/vestor.js',
        'js/helpers.js',
        'js/bootstrap-datepicker.min.js',
        'js/bootstrap-datepicker.pt-BR.min.js',
        'js/jquery.mask.min.js',
        'js/dropzone.min.js',
        'js/dropzone-config.js'
        // 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js',
        // 'js/vue-select.min.js'
    ];
    public $depends = [
      
    
     
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];

    // public $css = [
    //     'css/site.css',
    //     'css/jquery.loadingModal.min.css',
    //     'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
    //     'css/adminLTE_Devell.css',
    //     'css/skin-blue.css',
    //     'css/fontawesome.min.css'
    // ];
    // public $js = [
    //     'js/ajax-modal-popup.js',
    //     'js/jquery.loadingModal.min.js',
    //     'js/mascaras.js',
    //     'https://maps.googleapis.com/maps/api/js?key=AIzaSyA6Wz2SwRFLAEZJlzsSczhp07istHAdUlA&libraries=places',
    //     'js/jquery.geocomplete.js',
    //     'js/confirm.js'
    // ];
    // public $depends = [
    //     'yii\web\YiiAsset',
    //     'yii\bootstrap\BootstrapAsset',
    // ];
    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
