<style>
.agendamento p {
    color:white;
}
.agendamento p .inverso {
    color: #ff8800;
}
.agendamento span {
    background-color: #ff8800;
    width: 70px;
    height: 70px;
    border-radius: 50%;
    overflow: hidden;
    float: left;
    margin-left: 2em;
    margin-top: 1em;
    cursor:pointer;
}

.agendamento span .inverso {
    background-color: white;
    width: 70px;
    height: 70px;
    border-radius: 50%;
    overflow: hidden;
    float: left;
    margin-left: 2em;
    margin-top: 1em;
}

.agendamento span h6 {
    color: white;
    font-weight: 100;
    font-size: 1em;
    padding-top: 1em;
}

.agendamento_horario span {
    background-color: #710023;
    color: white;
    font-weight: 800;
    border-radius: 30px;
    border: none;
    margin-top: 0.8em;
    margin-bottom: 1em;
    margin-left: 2em;
    min-width: 8%;
    height: 1%;
}
.agendamento_horario .hora-ativa  {
    background-color: #fff;
    border: solid #710023 2px; 
}
    .hora-ativa p {
    color:#710023 !important;
}
.agendamento_horario p {
    justify-content: center;
    color: white;
    font-weight: 600;
    font-size: 1em !important;
    margin-top: -0.8em;
    padding: 0;
    padding-top: 1em;
    padding-left: 1em;
    padding-right: 1em;
    margin-bottom: 0px;
}
.bola-ativa p {
    color: #ff8800;
    
}
.bola-ativa span {
    background-color: white; 
    
}
.agendamento .bola-ativa {
    border: solid #ff8800 2px; 
    background-color: white;
    width: 70px;
    height: 70px;
    border-radius: 50%;
    overflow: hidden;
    float: left;
    margin-left: 2em;
    margin-top: 1em;
}

.bola-ativa h6, b {
    color: #ff8800 !important;
}
.agendamento_horario {
    cursor:pointer;
}
.condizRealidade, .atendimento, .resposta {
    display: none;;
}
</style>
<div class="modal-body">
    <section class=" bg_cover degrade pb-50" style="height: auto;">
        <div class="container">
            <h3 class="text-white pt-50 text-center">
                    <img src="../imgs/logo-branco.png" style="padding-left:10px; width:100px; height:30px;" alt="Logo">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
            </h3>
        </div>
    </section>
    <div class="text-center ">
        <h3 class="mt-10 h3-vestor">
            Avalie sua experiência
        </h3>

        <div class="atendimento">
            <p class="mt-3" >Como foi o atendimento do corretor?</p>
            <div class="col-md-12 text-center pb-10">
                    <button type="button" class="btn-large btn-global" style="height:auto;" onclick="atendimento('BOM')">BOM</button>
                    <button type="button" class="btn-large btn-global ml-2" style="height:auto;" onclick="atendimento('RUIM')">RUIM</button>
            </div>
        </div>
        <div class="condizRealidade">
            <p class="mt-3" >As fotos do anúncio condizem com a realidade?</p>
            <div class="col-md-12 text-center pb-10">
                    <button type="button" class="btn-large btn-global" style="height:auto;" onclick="condizRealidade('SIM')">SIM</button>
                    <button type="button" class="btn-large btn-global ml-2" style="height:auto;" onclick="condizRealidade('NÃO')">NÃO</button>
            </div>
        </div>

        <h3 class="mt-10 h3-vestor resposta">
            Obrigado por responder
        </h3>
    </div>
   
</div>
    

</div>

<script type="text/javascript">
    var imovelPost = <?= json_encode($_POST['imovel']['imovel']) ?>;

    if(!imovelPost.anuncio.condizRealidade){
        $(".condizRealidade").show();
    }
    if(!imovelPost.anuncio.atendimento){
        $(".atendimento").show();
    }
    resposta();
    function condizRealidade(opcao){
        postCondizRealidade({id: imovelPost.anuncio.id, data: opcao}).then((response) => {
            $(".condizRealidade").hide();
            
            imovelPost.anuncio.condizRealidade = opcao;
            resposta();
            // console.log(response)
        });
        // console.warn(opcao)
    }

    function atendimento(opcao){
        // console.warn(opcao)
        postAtendimento({id: imovelPost.anuncio.id, data: opcao}).then((response) => {

            $(".atendimento").hide();
            imovelPost.anuncio.atendimento = opcao;
            resposta();
            // console.log(response)
        });
    }

    function resposta(){
        if(imovelPost.anuncio.condizRealidade && imovelPost.anuncio.atendimento){
        $(".resposta").show();
    }
    }
</script>