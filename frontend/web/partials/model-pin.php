<style>
    .form-control {
        padding-left: 18px !important;
    }
</style>
<div class="modal-body ">
    <section class=" bg_cover degrade pb-50" style="height: auto;">
        <div class="container">
            <h3 class="text-white pt-50 text-center">
                <img src="../imgs/logo-branco.png" style="padding-left:10px; width:100px; height:30px;" alt="Logo">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                    <span aria-hidden="true">&times;</span>
                </button>
            </h3>
        </div>
    </section>
    <h3 class="text-center h3-vestor mt-10">
        Confirmação de acesso
    </h3>

    <div class="mt-5 row ">
        <div class="col-md-2 offset-md-2 col-3 confirmacao_visita">
            <input type="text" id="pin1" onchange="setPin(1, this.value)" class="form-control center-inside-column" maxlength="1">
        </div>
        <div class="col-md-2 col-sm-3 col-3  confirmacao_visita">
            <input type="text" id="pin2" onchange="setPin(2, this.value)" class="form-control center-inside-column" maxlength="1">
        </div>
        <div class="col-md-2  col-sm-3 col-3  confirmacao_visita">
            <input type="text" id="pin3" onchange="setPin(3, this.value)" class="form-control center-inside-column" maxlength="1">
        </div>
        <div class="col-md-2  col-sm-3 col-3 confirmacao_visita">
            <input type="text" id="pin4" onchange="setPin(4, this.value)" class="form-control center-inside-column" maxlength="1">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center mt-50">
            <button type="button" class="btn-01" style="height:auto;" onclick="continuar()">CONTINUAR</button>
        </div>
        <div class="col-md-12 text-center mt-20">
            <h4 class="text-center h3-vestor mt-10">
                Não recebi o código
            </h4>
        </div>
    </div>
</div>

<script>
     function setPin(pin, value){
         if(!isNumber(value)){
            $(`#pin${pin}`).val("");
             $(`#pin${pin}`).focus();
            return Swal.fire({
                icon: 'warning',
                title: 'Preencha o campo com números',
                showConfirmButton: false,
                timer: 1000,
                timerProgressBar: true,
            })
         }

         

         nextPin = pin + 1
         if(nextPin == 5)
            nextPin = 1
         $(`#pin${nextPin}`).focus();
     }
     function continuar(){
         if(!pin1.value || !pin2.value || !pin3.value || !pin4.value){
            return Swal.fire({
                icon: 'warning',
                title: 'Preencha todos os campos',
                showConfirmButton: false,
                timerProgressBar: true,
                timer: 1000
            })
         }


         console.log()
        let pins = pin1.value+pin2.value+pin3.value+pin4.value;
         autenticar(pins)
     }  

     function autenticar(pin){
        enviarPin({telefone: telefone, pin})
        .then(
          response => {
            if(response.data.status == false){
              pin1.value = "";
              pin2.value = "";
              pin3.value = "";
              pin4.value = "";
              
              return Swal.fire({
                icon: 'warning',
                title: response.data.message,
                showConfirmButton: false,
                timerProgressBar: true,
                timer: 1000
              })
            }
            localStorage.setItem('token', response.data.authKey);
            if(token){
                $('#perfil').show();
                $('#login').hide();
            }
            localStorage.setItem('usuario', JSON.stringify(response.data));
            autenticarUsuarioLocal({id: response.data.id}).then(response => {
                console.log('Autenticado localmente');
            });
            //
            //
            ///
            /////
            proximaEtapa(4)
            // this.diasDisponiveis = this.diasDisponiveis[1]
            // console.warn(this.diasDisponiveis)
             
          },
          error => {
            console.log(error);
          }
        )
     }
</script>