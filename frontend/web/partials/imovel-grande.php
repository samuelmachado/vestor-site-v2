  <div class="row justify-content-center" id="a">
    <div class="col-md-12">
        <div class="listing-item mt-40">
            <div class="listing-content white-bg">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="icon_fotos col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                        <img class="icon-header" src="imgs/visualizar_imovel/icon_fotos.svg" class="img-responsive" alt="">
                                        <div class="icon-header-text">Fotos</div>
                                    </div>
                                    <div class="icon_360 col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                        <img class="icon-header " src="imgs/visualizar_imovel/icon_360.svg" class="img-responsive" alt="">
                                        <div class="icon-header-text">360º</div>
                                    </div>
                                    <div class="icon_mapa col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                        <img class="icon-header " src="imgs/visualizar_imovel/icon_mapa.svg" class="img-responsive" alt="">
                                        <div class="icon-header-text">Mapa</div>
                                    </div>
                                    <div class="icon_vistarua col-lg-3 col-sm-6 col-xs-6 col-6 text-center mt-3" >
                                        <img class="icon-header " src="imgs/visualizar_imovel/icon_vistarua.svg" class="img-responsive" alt="">
                                        <div class="icon-header-text">Vista da rua</div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="row">
                                <div class="col-md-8">
                                    <img src="imgs/casa.jpg" style="border-radius: 30px;" alt="">
                                </div>
                                <div class="col-md-4 ">
                                    <div class="listed-box white-bg box mt-80" style="border-radius: 30px;background: white;;">
                                        <div class="title-item" >
                                            <H5 class="subtitle-vestor text-center codigo" ></H5>
                                            <h5 class="text-center pt-10">Aluguel</h5>
                                            <h3 class="center orange aluguel"></h3>
                                        </div>
                                        <div class="listed-list" style="margin-top:0px; border-top:none;">
                                            <ul>
                                                <li><span class="orange condominio" style="font-weight: bold;"></span></li>
                                                <li><span class="orange iptu" style="font-weight: bold;"></span></li>
                                                <li><span class="orange seguro-incendio" style="font-weight: bold;"></span></li>
                                            </ul>
                                        </div>
                                        <div class="listed-list" style="margin-top: 0px;">
                                            <ul>
                                                <li class="title-2 text-center">R$ 2700,00</li>
                                            </ul>
                                        </div>
                                        
                                        <div class="mt-10 text-center">
                                            <button class="btn btn-01" id="agendar">AGENDAR VISITA</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="list">
                                    <ul >
                                        <li><img src="imgs/icon_metragem.svg" class="text-center"><br><span class="m2"></span></li>
                                        <li><img src="imgs/icon_quarto.svg" class="text-center"><br><span class="quartos"></span></li>
                                        <li><img src="imgs/icon_vaga.svg" class="text-center"><br><span class="vagas"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    
        var imovel = <?= json_encode($_POST['imovel']) ?>;
        // console.log(imovel)  

        $('#agendar').click(() => {
            // console.log(1)
            localStorage.setItem('imovelAtivo', JSON.stringify(imovel));
            window.location.href = 'imoveis/view?id='+imovel.id
        })
        $(function() { 
            $(".codigo").text(`CÓD ${imovel.id}`)
            $(".aluguel").text(`R$ ${imovel.valor}`)
            $(".condominio").text(`+ Condomínio: R$ ${imovel.valorCondominio}`)
            $(".iptu").text(`+ IPTU: R$ ${imovel.valorIPTU}`)
            $(".seguro-incendio").text(`+ Seguro Incêndio: R$ ${imovel.valorSeguroIncendio}`)
            $(".m2").text(`${imovel.area}m²`)
            $(".quartos").text(`${imovel.quarto} quartos`)
            $(".vagas").text(`${imovel.vagaGaragem} vagas`)
          });
</script>