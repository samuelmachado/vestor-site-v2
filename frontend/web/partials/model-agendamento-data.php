<style>
.agendamento p {
    color:white;
}
.agendamento p .inverso {
    color: #ff8800;
}
.agendamento span {
    background-color: #ff8800;
    width: 70px;
    height: 70px;
    border-radius: 50%;
    overflow: hidden;
    float: left;
    margin-left: 2em;
    margin-top: 1em;
    cursor:pointer;
}

.agendamento span .inverso {
    background-color: white;
    width: 70px;
    height: 70px;
    border-radius: 50%;
    overflow: hidden;
    float: left;
    margin-left: 2em;
    margin-top: 1em;
}

.agendamento span h6 {
    color: white;
    font-weight: 100;
    font-size: 1em;
    padding-top: 1em;
}

.agendamento_horario span {
    background-color: #710023;
    color: white;
    font-weight: 800;
    border-radius: 30px;
    border: none;
    margin-top: 0.8em;
    margin-bottom: 1em;
    margin-left: 2em;
    min-width: 8%;
    height: 1%;
}
.agendamento_horario .hora-ativa  {
    background-color: #fff;
    border: solid #710023 2px; 
}
    .hora-ativa p {
    color:#710023 !important;
}
.agendamento_horario p {
    justify-content: center;
    color: white;
    font-weight: 600;
    font-size: 1em !important;
    margin-top: -0.8em;
    padding: 0;
    padding-top: 1em;
    padding-left: 1em;
    padding-right: 1em;
    margin-bottom: 0px;
}
.bola-ativa p {
    color: #ff8800;
    
}
.bola-ativa span {
    background-color: white; 
    
}
.agendamento .bola-ativa {
    border: solid #ff8800 2px; 
    background-color: white;
    width: 70px;
    height: 70px;
    border-radius: 50%;
    overflow: hidden;
    float: left;
    margin-left: 2em;
    margin-top: 1em;
}

.bola-ativa h6, b {
    color: #ff8800 !important;
}
.agendamento_horario {
    cursor:pointer;
}
</style>
<div class="modal-body">
    <section class=" bg_cover degrade pb-50" style="height: auto;">
        <div class="container">
            <h3 class="text-white pt-50 text-center">
    
                    <img src="../imgs/logo-branco.png" style="padding-left:10px; width:100px; height:30px;" alt="Logo">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                    </button>
            </h3>
        </div>
    </section>
    <div class="text-center ">
        <h3 class="mt-10 h3-vestor">
            Agora falta pouco para agendarmos sua visita
        </h3>
        <p class="mt-3 h3-vestor" >Fevereiro / 2020</p>
        <p class="" >Escolha o dia</p>
    </div>
    <div class=" container ">          
    <div class="row text-center agendamento justify-content-md-center dias">
    </div>
    <p class="text-center mt-10" ><br>Escolha o horário</p>
    <div class="row text-center agendamento_horario justify-content-md-center">

    </div>    
</div>
    

</div>
<div class="modal-footer">
    <div class="col-md-12 text-center">
        <button type="button" class="btn-large btn-global" style="height:auto;" onclick="continuar()">CONTINUAR</button>
    </div>
</div>

<script type="text/javascript">
        //seleção de dia e hora
        var listaImoveis = []
        var dias = []
        var horariosDisponiveis = []
        var diaSelecionado = null, horarioSelecionado = null;
        var imovelPost = <?= json_encode($_POST['imovel']['imovel']) ?>;
        
        function continuar(){
            if(!diaSelecionado || !horarioSelecionado){
                return Swal.fire(
                'Ops!',
                'Selecione o <b>dia</b> e o <b>horário</b>.',
                'warning'
                )
            }
            // Evento da página de cima
            dataVisita = diaSelecionado
            horaVisita = horarioSelecionado
            proximaEtapa(2);
        }

        // SELEÇÃO DE  DIA E HORA
        function limparBolas(){
            for(let i = 0; i < dias.length; i++){
                $(`[dia=${i}]`).removeClass('bola-ativa')
            }
        }
        function limparHorarios(dia){
            for(let i = 0; i < horariosDisponiveis[dia].length; i++){
                $(`[horario=${i}]`).removeClass('hora-ativa')
            }
        }
        function selecionarHorario(dia, horario){
            limparHorarios(dia);
            horarioSelecionado = horariosDisponiveis[dia][horario]
            $(`[horario=${horario}]`).addClass('hora-ativa')
            console.log(horarioSelecionado)
        }
        function selecionarData(value){
            limparBolas()
            limparHorarios(value);
            horarioSelecionado = null
            $(`[dia=${value}]`).addClass('bola-ativa')
            diaSelecionado = dias[value]
            $(".agendamento_horario").text("")
            for(let i = 0; i < horariosDisponiveis[value].length; i++){
                $(".agendamento_horario").append(`
                    <span onclick="selecionarHorario(${value},${i})" horario="${i}">
                        <p>${horariosDisponiveis[value][i]}</p>
                    </span> 
                `);
            }
            
        }
        
        $(function() {
            // let imovel = JSON.parse(localStorage.getItem('imovelAtivo'));
            // var param = getParams()
            // Se o imóvel no localstorage não é o mesmo que o usário abriu
                viewImovel({id: imovelPost.id}).then((response) => {
                    imovel = response.data
                    console.log(imovel)
                    Object.entries(imovel.diasDisponiveis).forEach(entry => {
                        dias.push(new Date(entry[0] + ' '+entry[1][0]))
                        horariosDisponiveis.push(entry[1])
                    });
                    
                    for(let i = 0; i < dias.length; i++){
                        let d = dias[i];
                        $(".dias").append(`
                            <span onclick="selecionarData(${i})" dia="${i}">
                                <h6>${getDay({dia: d, shortDay: true})}</h6>
                                <p>${getDateBr(d)}</p>
                            </span>
                        `)
                    }

                    // <span>
                    // <p>9:30</p>
                    // </span>

                }).catch((e) => {
                    console.log(e)
                    // window.history.back();
                });     
            // var param = getParams()
            // // Se o imóvel no localstorage não é o mesmo que o usário abriu
            // if(!param || imovel.id != param.id){
            //     viewImovel({id: param.id}).then((response) => {
            //         imovel = response.data
            //         localStorage.setItem('imovelAtivo', JSON.stringify(imovel))
            //     }).catch((e) => {
            //         window.history.back();
            //     });                
            // } else {
            //     console.warn('## Pego do localstorage ##')
            //     console.warn(imovel.id, param.id)
            // }

            $(function() { 
                $(".codigo").text(`CÓD ${imovel.id}`)
                $(".aluguel").text(`R$ ${imovel.valor}`)
                $(".condominio").text(`+ Condomínio: R$ ${imovel.valorCondominio}`)
                $(".iptu").text(`+ IPTU: R$ ${imovel.valorIPTU}`)
                $(".seguro-incendio").text(`+ Seguro Incêndio: R$ ${imovel.valorSeguroIncendio}`)
                $(".m2").text(`${imovel.area}m²`)
                $(".quartos").text(`${imovel.quarto} quartos`)
                $(".vagas").text(`${imovel.vagaGaragem} vagas`)
            });

            console.warn(imovel)
            // imoveis({}).then((response) => {
            //     listaImoveis = response.data
            //     listaImoveis.forEach((item) => {
            //         loadPartial({
            //             partial: 'imovel-grande.php',
            //             data: {imovel:item},
            //             divResponse: '.conteudo'
            //         })
            //     })
            // });
        });
    </script>