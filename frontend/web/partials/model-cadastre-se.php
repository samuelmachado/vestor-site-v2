 <style>
     input {
         margin-top: 33px !important;
         height: 39px !important;
     }
 </style>

 <div class="modal-body">
     <section class=" bg_cover degrade pb-50" style="height: auto;">
         <div class="container">
             <h3 class="text-white pt-50 text-center">
                 <img src="../imgs/logo-branco.png" style="padding-left:10px; width:100px; height:30px;" alt="Logo">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                </button>
             </h3>
         </div>
     </section>
     <h3 class="text-center h3-vestor mt-10">
         Falta só mais um pouquinho
     </h3>

     <div class="row justify-content-center">
         <div class="col-md-4 col-7 col-sm-4 mt-2">
             <input type="text" id="nome" placeholder="Nome completo" class="form-control border-input-full ">
         </div>
     </div>
     <div class="row justify-content-center">
         <div class="col-md-4 col-7 col-sm-4 mt-2">
             <input type="text" id="email" placeholder="E-mail" class="form-control border-input-full ">
         </div>
     </div>
     <div class="row justify-content-center">
         <div class="col-md-4 col-7 col-sm-4 mt-2">
             <input type="text" id="cpf" placeholder="CPF" maxlength="14" onchange="changeCpf(this)" onblur="testarCpf(this)" onkeypress="MascaraCPF(this);" class="form-control border-input-full ">
         
            <div id="mensagem"></div>
         </div>
     </div>

     <div class="row">
         <div class="col-md-12 text-center mt-50">
             <button type="button" class="btn-global btn-large btn-global" style="height:auto;" onclick="continuar()">CONTINUAR</button>
         </div>
     </div>
 </div>
 <script>
     function changeCpf(cpf){
        $("#mensagem").html("")
     }
    function testarCpf(cpf){
       
        if(cpf.value.length == 0)
            return null;
        if(!ValidarCPF(cpf)){
        cpf.value = ""
        $("#cpf").focus()
        return Swal.fire({
                icon: 'warning',
                title: 'CPF inválido!',
                showConfirmButton: false,
                timer: 1000,
                timerProgressBar: true,
            })
        }
    }

    function continuar(){
        $("#mensagem").html("")
        if(!nome.value || !email.value || !cpf.value) {
            return Swal.fire({
                icon: 'warning',
                title: 'Preencha os campos',
                showConfirmButton: false,
                timer: 1000,
                timerProgressBar: true,
            })
        }
        atualizarUsuario({data: {nome: nome.value, cpf: cpf.value, email: email.value}})
        .then(
          response => {
           
            if(response.data.status){
                localStorage.setItem('usuario', JSON.stringify(response.data.usuario));
                usuario = response.data.usuario
                proximaEtapa(4)
            } else {
                if(response.data.error && response.data.error == 1001){
                    $("#mensagem").html(`Este CPF está associado ao email ${response.data.detalhes}, <b>clique aqui</b> para recuperar a senha`)
                    return Swal.fire({
                        icon: 'warning',
                        title: 'CPF já registrado',
                        showConfirmButton: false,
                        timer: 1000,
                        timerProgressBar: true,
                    })
                }
                
            }
            
            console.log(response)
            // //deu erro
            // if(response.data.code_error){
            //   this.viewImovel(this.imovel.id)
            // // sucesso
            // } 
            // this.zerar() 
            // if(response.data.status){
            //       this.$emit('closeModal')
            //       this.step = 23
            // }
             
            
          },
          error => {
            console.log(error); 
          }
        );
    }

 </script>


 