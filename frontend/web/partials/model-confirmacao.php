<div class="modal-body">
    <section class=" bg_cover degrade pb-50" style="height: auto;">
        <div class="container">
            <h3 class="text-white pt-50 text-center">
                <img src="../imgs/logo-branco.png" style="padding-left:10px; width:100px; height:30px;" alt="Logo">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
                </button>
            </h3>
        </div>
    </section>
    <div id="agendar" style="display: none;">
        <h3 class="text-center h3-vestor mt-10">
            Agora falta pouco para agendarmos sua visita
        </h3>
        <p class="text-center">
            Sua visita será agendada para

        </p>

        <div class="row justify-content-md-center">
            <div class="col-md-12 text-center">
                <button type="button" class="btn-global btn-large" style="height:auto;" id="datahora"></button>
            </div>
        </div>
        <div class="row justify-content-md-center">

            <div class="col-md-12 text-center">
                <h5 class="text-center h3-vestor mt-10" id="enderecoCompleto">
                </h5>
                <p class="text-center">
                    Certifique-se de que irá comparecer na data, horário e local marcados.</p>
            </div>

            <div class="col-md-12 text-center pb-10">
                <button type="button" class="btn-large btn-global" style="height:auto;" onclick="voltar()">VOLTAR</button>
                <button type="button" class="btn-large btn-global" style="height:auto;" onclick="continuar()">CONTINUAR</button>
            </div>
        </div>
    </div>

    <div id="mensagem" style="display: none;text-align:center">
    <br>Você já agendou uma visita para este imóvel.<br>
        <div class="col-md-12 text-center pb-10">
                <button type="button" class="btn-large btn-global" style="height:auto;" onclick="visualizarVisitas()">VISUALIZAR VISITAS</button>
        </div>
    </div>
</div>
<script>
    $("#enderecoCompleto").html(imovel.imovel.enderecoCompleto)
    $("#datahora").html(`${getDay({dia: dataVisita, shortDay: false})} ${getDateBr(dataVisita)} às ${horaVisita}` )
    var editar = false;
    editar = <?= isset($_POST['editar']) ? json_encode($_POST['editar']) : 'null' ?>;
    console.log(imovel)
    if(editar == null){
        visitaRepetida({idAnuncio: imovel.imovel.anuncio.id}).then((response) => {
            let data = response.data;
            if(data.status){
                $("#mensagem").show();
                $("#agendar").hide();
            } else {
                $("#agendar").show();
                $("#mensagem").hide();
            }
        }).catch(() => {
            $("#mensagem").hide();
            $("#agendar").show();
        })
    }
    function visualizarVisitas(){
        window.location.href = baseUrlFrontend+'minhas-visitas';
    }
    // console.warn(editar)
    function voltar(){
        dataVisita = null;
        horaVisita = null;
        proximaEtapa(1);
    }
    function continuar(){
        // console.warn('*');
        processarVisita()
    }
    
    function processarVisita(){
      // console.warn('processarVisita')
      novaVisita({data: {dia: dataVisita, hora: horaVisita, imovel: imovel, editar:editar}})
        .then(
          response => {
            Swal.fire(
                'Sucesso!',
                response.data.message,
                'success'
            )
            if(response.data.status){
                $('#extraLargeModal').modal('hide');
            } else {
                getImovel()
                proximaEtapa(1)

            }
            
            console.log(response)
            // //deu erro
            // if(response.data.code_error){
            //   this.viewImovel(this.imovel.id)
            // // sucesso
            // } 
            // this.zerar() 
            // if(response.data.status){
            //       this.$emit('closeModal')
            //       this.step = 23
            // }
             
            
          },
          error => {
            console.log(error); 
          }
        );
    }
    // console.log(imovel)
</script>