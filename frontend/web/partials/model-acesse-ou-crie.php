<div class="modal-body">
<section class=" bg_cover degrade pb-50" style="height: auto;">
    <div class="container">
        <h3 class="text-white pt-50 text-center">
            <img src="../imgs/logo-branco.png" style="padding-left:10px; width:100px; height:30px;" alt="Logo">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                        <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
</section>
<h3 class="text-center h3-vestor mt-10">
    Acesso ou crie sua conta
</h3>
<p class="text-center">
    Não precisa de senha, é só digitar seu celular<br>
    Mais segurança e comodidade pra você
</p>
<div class="row justify-content-md-center">
        <div class="col-2 hide-desktop"></div>
        <div class="col-md-2 col-sm-3  col-3 mt-2">
            <input type="text" id="ddd" placeholder="DDD"  value="12" class="form-control border-input-full ">
        </div>
        <div class="col-md-5 col-sm-5 col-5 mt-2">
            <input type="text" id="celular" placeholder="Celular" value="12"  class="form-control border-input-full float-right">
    </div>
        <div class="text-center mt-2 col-md-12">
            <p>Você vai receber um código de confirmação no seu celular</p>
        </div>
</div>

<div class="row">
    <div class="col-md-12 text-center mt-50">
        <button type="button" class="btn-global btn-large btn-global" style="height:auto;" onclick="continuar()">RECEBER CÓDIGO POR SMS</button>
    </div>
    <div class="col-md-12 text-center mt-20">
        <p class="text-center mt-10">Ao continuar você concorda com os <span class="h3-vestor">Termos de uso</span> e <span class="h3-vestor">Política de privacidade</span></p>
        <h4 class="  mt-10">
        </h4>
    </div>
</div>
</div>

<script>
    function continuar(){
        let ddd = $("#ddd").val();
        let celular = $("#celular").val();
        console.log(ddd,celular)
        let pin;
        if(!ddd || !celular){
            return Swal.fire(
            'Ops!',
            'Preencha o <b>DDD</b> e o <b>número de celular</b>.',
            'warning'
            )
        }
  
        receberSMS()
     
    }

    function receberSMS(){
      gerarSessao({telefone: ddd.value +  celular.value})
        .then(
          response => {
            
            console.log(response)
            pin = response.data.data.pin
            // Evento da página de cima
            telefone = ddd.value + celular.value
            proximaEtapa(3);
          },
          error => {
            console.log(error); 
          }
        );
    }
</script>