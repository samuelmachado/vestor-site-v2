// / import store from '@/store'
//     'Authorization': store.state.app.token ? store.state.app.token : ''
// before a request is made start the nprogress

// axios.interceptors.response.use((response) => {
//   return response
// }, 
// function (error) {
//   // const originalRequest = error.config;
//   //&& !originalRequest._retry
//   if (error.response.status === 401) {
//     localStorage.removeItem('token');
//       // originalRequest._retry = true;
//       // return axios.post('/auth/token',
//       //     {
//       //       "refresh_token": localStorageService.getRefreshToken()
//       //     })
//       //     .then(res => {
//       //         if (res.status === 201) {
//       //             // 1) put token to LocalStorage
//       //             localStorageService.setToken(res.data);

//       //             // 2) Change Authorization header
//       //             axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorageService.getAccessToken();

//       //             // 3) return originalRequest object with Axios.
//       //             return axios(originalRequest);
//       //         }
//       //     })
//   }
// });


let b64DecodeUnicode = str =>
  decodeURIComponent(
    Array.prototype.map.call(atob(str), c =>
      '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    ).join(''))

let parseJwt = async token =>
  await JSON.parse(
    b64DecodeUnicode(
      token.split('.')[1].replace('-', '+').replace('_', '/')
    )
  )

var baseUrlFrontend = "http://localhost/vestor-site-v2/frontend/web/"
let local = () => axios.create({
  baseURL: baseUrlFrontend,
  timeout: 6 * 1000,
  headers: {
  }
})

let instance = () => axios.create({
  baseURL: 'http://localhost/vestor-site-v2/backend/web/index.php/api/',
  timeout: 6 * 1000,
  headers: {
  }
})

let instanceAuthenticated = () => axios.create({
  baseURL: 'http://localhost/vestor-site-v2/backend/web/index.php/api/',
  timeout: 6 * 1000,
  headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
})



let loadPartial = ({partial, data, divResponse}) => {
    $(divResponse).html("");
    $.ajax({
        type: "POST",   
        url: "http://localhost/vestor-site-v2/frontend/web/partials/"+partial,
        data: data,
        success : function(response)
        {
            $(divResponse).prepend(response);
        }
    })
}
let getDateBr = (dia) => {
  return dia.toLocaleDateString('pt-BR').substr(0,5)
}
let getDay = ({dia, shortDay=false}) => {
  if(!dia)
    return '';
  switch(dia.getDay()) {
    case 6: return shortDay ? 'DOM' : 'Domingo'; 
    case 0: return shortDay ? 'SEG' : 'Segunda'; 
    case 1: return shortDay ? 'TER' : 'Terça';
    case 2: return shortDay ? 'QUA' : 'Quarta';
    case 3: return shortDay ? 'QUI' : 'Quinta';
    case 4: return shortDay ? 'SEX' : 'Sexta'; 
    case 5: return shortDay ? 'SÁB' : 'Sábado'; 
    default: return '-';
  }
}
const isNumber = value => {
  if(parseInt(value) === 0){
    return true
  } 
  if(parseInt(value)){
    return true
  }
  // First: Check typeof and make sure it returns number
  // This code coerces neither booleans nor strings to numbers,
  // although it would be possible to do so if desired.
  if (typeof value !== 'number') {
    return false
  }
  // Reference for typeof:
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
  // Second: Check for NaN, as NaN is a number to typeof.
  // NaN is the only JavaScript value that never equals itself.
  if (value !== Number(value)) {
    return false
  }
  // Reference for NaN: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/isNaN
  // Note isNaN() is a broken function, but checking for self-equality works as NaN !== NaN
  // Alternatively check for NaN using Number.isNaN(), an ES2015 feature that works how one would expect

  // Third: Check for Infinity and -Infinity.
  // Realistically we want finite numbers, or there was probably a division by 0 somewhere.
  if (value === Infinity || value === !Infinity) {
    return false
  }
  return true
}
const gerarSessao = ({ telefone }) => {
  return instance().post('pins/gerar', {
    telefone: telefone.toString(),
    isTeste: true
  })
}

const enviarPin = ({ pin, telefone,usuario }) => {
  return instance().post('pins/autenticar', {
    pin,
    telefone,
    usuario
  })
}

const visitaRepetida = ({ idAnuncio }) => {
  return instanceAuthenticated().get(`imoveis/visita-repetida?idAnuncio=${idAnuncio}`)
}

const enviarProposta = ({ data }) => {
  return instanceAuthenticated().post('contratos/novo', data)
}
const salvarFavoritos = (data) => {
  data = JSON.stringify(data);
  localStorage.setItem('favoritos', data)
}
const obterFavoritos = () => {
  data = localStorage.getItem('favoritos')
  if(!data)
    return null
  return JSON.parse(data);
}
const imoveis = ({ filter = null, page=1 }) => {
  // console.warn(filter);

  if(filter){
    filter = toFormUrlEncoded(filter)
    // console.warn(filter);
  } 
  return instance().get(`imoveis/index?fields=id,area,vagaGaragem,quarto,endereco.completo,endereco.bairro.nome,endereco.cidade.nome,valor,thumbnails.arquivo,banheiro,valorCondominio,valorIPTU,valorSeguroIncendio,area,vagaGaragem&page=${page}&${filter}`, {
    page
  })
}

const viewImovel = ({ id }) => {
  return instance().get(`imoveis/view?id=${id}`)
}

const cancelarVisita = ({ id }) => {
  return instanceAuthenticated().get(`visitas/cancelar?id=${id}`)
}
const viewUsuario = () => {
  return instanceAuthenticated().get(`usuario/view`)
}

const getVisitas = () => {
  return instanceAuthenticated().get(`visitas/minhas-visitas`)
}

const novaVisita = ({ data }) => {
  return instanceAuthenticated().post(`visitas/create`, {data})
}

const atualizarUsuario = ({ data }) => {
  return instanceAuthenticated().put(`usuario/update`, {data})
}

const autenticarUsuarioLocal = ({id}) => {
  return local().get(`site/login?id=${id}`)
}
const finalizarProcesso = ({id}) => {
  return local().get(`gerenciar/finalizar-etapa?id=${id}`)
}
const postCondizRealidade = ({ data, id }) => {
  return local().post(`minhas-visitas/condiz-realidade?id=${id}`, {data})
}
const postAtendimento = ({ data, id }) => {
  return local().post(`minhas-visitas/atendimento?id=${id}`, {data})
}
const getMeusImoveis = () => {
  return instanceAuthenticated().get(`imoveis/meus-imoveis?fields=id,area,vagaGaragem,quarto,endereco.completo,endereco.bairro.nome,endereco.cidade.nome,valor,thumbnails.arquivo,banheiro,valorCondominio,valorIPTU,valorSeguroIncendio,area,vagaGaragem,contrato.id,contrato.valorAluguel,contrato.inicio,contrato.fim,idProprietarioUsuario`)
}

const reagendarVisita = ({ data }) => {
  return instanceAuthenticated().post(`visitas/regendar`, {data})
}
const getParams = () => {
  let paramObj = {}
  let params = window.location.href.split( '?' );
  params = params[1].split('&');
  params.forEach((param) => {
    // id=1
    keyvalue = param.split('=')
    // {id: 1}
    paramObj[keyvalue[0]] = keyvalue[1]
  })
  return paramObj;
}
/**
 * @param {Object} object
 * @return {string}
 */
 const toFormUrlEncoded = (object) => {
  return Object.entries(object)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join('&');
};
// export {parseJwt, getMeusImoveis, enviarProposta, instance, gerarSessao, enviarPin, imoveis, viewImovel, novaVisita, viewUsuario, getVisitas, cancelarVisita,reagendarVisita }
 