var totalsize = 0.0;

Dropzone.prototype.defaultOptions = {

    ...Dropzone.prototype.defaultOptions,
    ...{
      autoProcessQueue: true,
      parallelUploads: 1,
      acceptedFiles:"image/jpeg,image/png,image/gif,image/jpg,application/pdf",
      maxFilesize: 2,
      dictDefaultMessage: "Solte aqui os arquivos para enviar", // Drop files here to upload";
      dictFallbackMessage: "Seu navegador não suporta uploads de arrastar e soltar.", // Your browser does not support drag'n'drop file uploads.";
      dictFallbackText: "Por favor, use o formulário abaixo para enviar seus arquivos como antigamente.", // Please use the fallback form below to upload your files like in the olden days.";
      dictFileTooBig: "O arquivo é muito grande ({{filesize}}MB). Tamanho máximo permitido: {{maxFilesize}}MB.", // File is too big ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.";
      dictInvalidFileType: "Você não pode fazer upload de arquivos desse tipo.", // You can't upload files of this type.";
      dictResponseError: "O servidor respondeu com o código {{statusCode}}.", // Server responded with {{statusCode}} code.";
      dictCancelUpload: "Cancelar envio", // Cancel upload";
      dictCancelUploadConfirmation: "Tem certeza de que deseja cancelar este envio?", // Are you sure you want to cancel this upload?";
      dictRemoveFile: "Remover arquivo", // Remove file";
      dictRemoveFileConfirmation: null, // null;
      dictMaxFilesExceeded: "Você só pode fazer upload de {{maxFiles}} arquivos.", // You can only upload {{maxFiles}} files.";
    }
  }


 window.onload = function() {
    $('.file-thumb img').on("click", function(data) {    
    window.open($(this).parent().attr('filename'));
    })
    $('body').on("click", '.file-thumb',function(data) {
        console.warn("!")
        Swal.fire({
            title: 'Atenção!',
            text: "Tem certeza que deseja excluir o arquivo?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
        }).then((result) => {
            if (result.value) {
                thumb = $(this).parent();
                url = $(this).attr('delete-url');
                console.log(url)
                idFile = thumb.attr('id-file');
                params = {'id': idFile}
                $.post(url, params, function(result){
                    console.log(result)
                    console.log($(this).parent())
                    thumb.remove();
                    Swal.fire('Excluído!', 'Arquivo foi excluído.', 'success')
                });
            }
        })
    })
    }


    $(document).ready(function($){

        $('.money').mask('000.000.000.000.000,00', {reverse: true});
      });