<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Notificacao".
 *
 * @property int $id
 * @property string $agendada
 * @property int $idTipoNotificacao
 * @property int $idUsuarioReceptor
 * @property string $texto
 * @property string $textoPush
 * @property string|null $dataEnviado
 * @property string|null $dataLido
 *
 * @property TipoNotificacao $idTipoNotificacao0
 * @property Usuario $idUsuarioReceptor0
 */
class Notificacao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Notificacao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'agendada', 'idTipoNotificacao', 'idUsuarioReceptor', 'texto', 'textoPush'], 'required'],
            [['id', 'idTipoNotificacao', 'idUsuarioReceptor'], 'integer'],
            [['agendada', 'dataEnviado', 'dataLido'], 'safe'],
            [['texto'], 'string', 'max' => 500],
            [['textoPush'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['idTipoNotificacao'], 'exist', 'skipOnError' => true, 'targetClass' => TipoNotificacao::className(), 'targetAttribute' => ['idTipoNotificacao' => 'id']],
            [['idUsuarioReceptor'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuarioReceptor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agendada' => 'Agendada',
            'idTipoNotificacao' => 'Id Tipo Notificacao',
            'idUsuarioReceptor' => 'Id Usuario Receptor',
            'texto' => 'Texto',
            'textoPush' => 'Texto Push',
            'dataEnviado' => 'Data Enviado',
            'dataLido' => 'Data Lido',
        ];
    }

    /**
     * Gets query for [[IdTipoNotificacao0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoNotificacao0()
    {
        return $this->hasOne(TipoNotificacao::className(), ['id' => 'idTipoNotificacao']);
    }

    /**
     * Gets query for [[IdUsuarioReceptor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarioReceptor0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuarioReceptor']);
    }
}
