<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "UsuarioPerfil".
 *
 * @property int $id
 * @property int $idUsuario
 * @property int $idPerfil
 *
 * @property Perfil $idPerfil0
 * @property Usuario $idUsuario0
 */
class UsuarioPerfil extends \yii\db\ActiveRecord
{
    const PERFIL_CLIENTE = 1;

    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        $unsets = [
            'id',
            'idPerfil',

        ];
        foreach($unsets as $unsetField)
            unset($fields[$unsetField]);

        $fields['perfil'] = 'perfil';

        return $fields;
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'UsuarioPerfil';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idUsuario', 'idPerfil'], 'required'],
            [['idUsuario', 'idPerfil'], 'integer'],
            [['idUsuario', 'idPerfil'], 'unique', 'targetAttribute' => ['idUsuario', 'idPerfil']],
            [['idPerfil'], 'exist', 'skipOnError' => true, 'targetClass' => Perfil::className(), 'targetAttribute' => ['idPerfil' => 'id']],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idUsuario' => 'Id Usuario',
            'idPerfil' => 'Id Perfil',
        ];
    }

    /**
     * Gets query for [[IdPerfil0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerfil()
    {
        return $this->hasOne(Perfil::className(), ['id' => 'idPerfil']);
    }

    /**
     * Gets query for [[IdUsuario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }

    public static function criarCliente(&$usuario){
        $model = new UsuarioPerfil();
        $model->idUsuario = $usuario->id;
        $model->idPerfil = self::PERFIL_CLIENTE;
        $model->save();
    }
}
