<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "LocalVistoria".
 *
 * @property int $id
 * @property string $nome
 *
 * @property VistoriaRelatorio[] $vistoriaRelatorios
 */
class LocalVistoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'LocalVistoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['nome'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
        ];
    }

    /**
     * Gets query for [[VistoriaRelatorios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVistoriaRelatorios()
    {
        return $this->hasMany(VistoriaRelatorio::className(), ['idVistoriaLocal' => 'id']);
    }
}
