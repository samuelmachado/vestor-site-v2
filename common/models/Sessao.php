<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Usuario".
 *

 */
class Sessao extends \yii\db\ActiveRecord
{
    public $password;
    
    // public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Sessao';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['desconto7','valorAluguel7','carencia7','idUsuarioAlugando','dataEtapa1','idVisita','ultimaEtapa','valor2','gerenciarData1','gerenciarValor2','gerenciarCarencia3','gerenciarTempoDesconto4','gerenciarValorDesconto4'], 'safe'],
            // [['perfil', 'idPessoa'], 'integer'],
            // [['ultimoAcesso', 'criado', 'atualizado'], 'safe'],
            // [['login'], 'string', 'max' => 30],
            // [['recoveryKey', 'authKey', 'senha'], 'string', 'max' => 255],
            // [['email','nome'], 'string', 'max' => 100],
            // [['firebase'], 'string', 'max' => 200],
            // [['login'], 'unique'],
            // [['recoveryKey'], 'unique'],
            // [['authKey'], 'unique'],
            // [['email'], 'unique'],
            // [['idPessoa'], 'unique'],
            // [['idPessoa'], 'exist', 'skipOnError' => true, 'targetClass' => Pessoa::className(), 'targetAttribute' => ['idPessoa' => 'id']],
        ];
    }
    public function beforeSave($insert)
    {
       
        if($this->valor2)
            $this->valor2 = Yii::$app->formatter::asDecimalBRL($this->valor2);
        if($this->gerenciarValor2)
            $this->gerenciarValor2 = Yii::$app->formatter::asDecimalBRL($this->gerenciarValor2);
        if($this->gerenciarValorDesconto4)
            $this->gerenciarValorDesconto4 = Yii::$app->formatter::asDecimalBRL($this->gerenciarValorDesconto4);
        // if($this->valor2)
        //     $this->valor2 = Yii::$app->formatter::asDecimalBRL($this->valor2);
        // if($this->valor2)
        //     $this->valor2 = Yii::$app->formatter::asDecimalBRL($this->valor2);
        if (parent::beforeSave($insert)) {
            return true;
        }
        return false;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Cód.',
            // 'login' => 'Login',
            // 'perfil' => 'Perfil',
            // 'idPessoa' => 'Id Pessoa',
            // 'recoveryKey' => 'Chave de recuperação',
            // 'authKey' => 'Chave de autenticação',
            // 'senha' => 'Senha',
            // 'email' => 'Email',
            // 'ultimoAcesso' => 'Último acesso',
            // 'firebase' => 'Firebase',
            // 'criado' => 'Criado',
            // 'atualizado' => 'Atualizado',
        ];
    }

    public function getDocumentoRGCPF()
    {
        return $this->hasMany(Documento::className(), ['idSessao' => 'id'])->where(['idTipoDocumento' => TipoDocumento::TIPO_RG_CPF]);
    }

    
    public function getDocumentoCompEnd()
    {
        return $this->hasMany(Documento::className(), ['idSessao' => 'id'])->where(['idTipoDocumento' => TipoDocumento::TIPO_COMP_END]);
    }

    
    public function getDocumentoCompRenda()
    {
        return $this->hasMany(Documento::className(), ['idSessao' => 'id'])->where(['idTipoDocumento' => TipoDocumento::TIPO_COMP_RENDA]);
    }
    public function getVisita()
    {
        return $this->hasOne(Visita::className(), ['id' => 'idVisita']);
    }

    public function hasDesconto(){
        if($this->gerenciarValorDesconto4){
            $lblMes = 'mês';
            if($this->gerenciarTempoDesconto4 > 1)
                $lblMes = 'meses'; 
            return $this->gerenciarValorDesconto4.' por '.$this->gerenciarTempoDesconto4.' '.$lblMes;
        } else {
            return 'Não tem';
        }
    }

    public function hasCarencia(){
        if($this->gerenciarCarencia3){
            $lblMes = 'mês';
            if($this->gerenciarCarencia3 > 1)
                $lblMes = 'meses';
            return $this->gerenciarCarencia3.' '.$lblMes;
        } else {
            return 'Não tem';
        }
    }

}
