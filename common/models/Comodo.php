<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Comodo".
 *
 * @property int $id
 * @property string|null $nome
 * @property string|null $sigla
 *
 * @property VistoriaRelatorio[] $vistoriaRelatorios
 */
class Comodo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Comodo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 50],
            [['sigla'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'sigla' => 'Sigla',
        ];
    }

    /**
     * Gets query for [[VistoriaRelatorios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVistoriaRelatorios()
    {
        return $this->hasMany(VistoriaRelatorio::className(), ['idComodo' => 'id']);
    }
}
