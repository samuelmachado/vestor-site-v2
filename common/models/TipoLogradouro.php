<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "TipoLogradouro".
 *
 * @property int $id
 * @property string $abreviacao
 * @property string $nome
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Cep[] $ceps
 */
class TipoLogradouro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TipoLogradouro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['abreviacao', 'nome'], 'required'],
            [['criado', 'atualizado'], 'safe'],
            [['abreviacao'], 'string', 'max' => 5],
            [['nome'], 'string', 'max' => 50],
            [['abreviacao', 'nome'], 'unique', 'targetAttribute' => ['abreviacao', 'nome']],
        ];
    }
    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        $unsets = [
            'atualizado',
            'criado'
        ];
        foreach($unsets as $unsetField)
            unset($fields[$unsetField]);
        
        return $fields;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abreviacao' => 'Abreviacao',
            'nome' => 'Nome',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[Ceps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCeps()
    {
        return $this->hasMany(Cep::className(), ['idTipoLogradouro' => 'id']);
    }
}
