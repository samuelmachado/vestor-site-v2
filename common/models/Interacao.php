<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Interacao".
 *
 * @property int $id Código
 * @property int|null $idUsuario Usuário
 * @property int $idAnuncio Anúncio
 * @property int $tipo Tipo Ex. Like View Loved
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Anuncio $idAnuncio0
 * @property Usuario $idUsuario0
 */
class Interacao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Interacao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idUsuario', 'idAnuncio', 'tipo'], 'integer'],
            [['idAnuncio', 'tipo'], 'required'],
            [['criado', 'atualizado'], 'safe'],
            [['idAnuncio'], 'exist', 'skipOnError' => true, 'targetClass' => Anuncio::className(), 'targetAttribute' => ['idAnuncio' => 'id']],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idUsuario' => 'Usuário',
            'idAnuncio' => 'Anúncio',
            'tipo' => 'Tipo Ex. Like View Loved',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[IdAnuncio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAnuncio0()
    {
        return $this->hasOne(Anuncio::className(), ['id' => 'idAnuncio']);
    }

    /**
     * Gets query for [[IdUsuario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }
}
