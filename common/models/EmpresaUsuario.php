<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "EmpresaUsuario".
 *
 * @property int $id Código
 * @property int $idEmpresa Empresa
 * @property int $idUsuario Usuário
 * @property int $perfil Perfil
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Usuario $idUsuario0
 * @property Empresa $idEmpresa0
 */
class EmpresaUsuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'EmpresaUsuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEmpresa', 'idUsuario'], 'required'],
            [['idEmpresa', 'idUsuario', 'perfil'], 'integer'],
            [['criado', 'atualizado'], 'safe'],
            [['idEmpresa', 'idUsuario'], 'unique', 'targetAttribute' => ['idEmpresa', 'idUsuario']],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuario' => 'id']],
            [['idEmpresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['idEmpresa' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idEmpresa' => 'Empresa',
            'idUsuario' => 'Usuário',
            'perfil' => 'Perfil',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[IdUsuario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }

    /**
     * Gets query for [[IdEmpresa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresa0()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'idEmpresa']);
    }
}
