<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Lead".
 *
 * @property int $id
 * @property string|null $nome
 * @property string|null $ddd
 * @property string|null $telefone
 * @property string|null $email
 * @property int|null $idCidade
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Cidade $idCidade0
 */
class Lead extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Lead';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCidade','nome','ddd','telefone'], 'required'],
            [['idCidade'], 'integer'],
            [['criado', 'atualizado'], 'safe'],
            [['nome'], 'string', 'max' => 255],
            [['ddd'], 'string', 'max' => 2],
            [['telefone'], 'string', 'max' => 11],
            [['email'], 'string', 'max' => 50],
            [['idCidade'], 'exist', 'skipOnError' => true, 'targetClass' => Cidade::className(), 'targetAttribute' => ['idCidade' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'ddd' => 'Ddd',
            'telefone' => 'Telefone',
            'email' => 'Email',
            'idCidade' => 'Id Cidade',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[IdCidade0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCidade()
    {
        return $this->hasOne(Cidade::className(), ['id' => 'idCidade']);
    }
}
