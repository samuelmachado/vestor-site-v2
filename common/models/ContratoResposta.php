<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ContratoResposta".
 *
 * @property int $id
 * @property int $idContratoOpcao
 * @property int $idContrato
 * @property int $aprovado
 * @property string $dataAprovacao
 *
 * @property Contrato $idContrato0
 * @property ContratoOpcao $idContratoOpcao0
 */
class ContratoResposta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ContratoResposta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idContratoOpcao', 'idContrato', 'dataAprovacao'], 'required'],
            [['idContratoOpcao', 'idContrato', 'aprovado'], 'integer'],
            [['dataAprovacao'], 'safe'],
            [['idContrato'], 'exist', 'skipOnError' => true, 'targetClass' => Contrato::className(), 'targetAttribute' => ['idContrato' => 'id']],
            [['idContratoOpcao'], 'exist', 'skipOnError' => true, 'targetClass' => ContratoOpcao::className(), 'targetAttribute' => ['idContratoOpcao' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idContratoOpcao' => 'Id Contrato Opcao',
            'idContrato' => 'Id Contrato',
            'aprovado' => 'Aprovado',
            'dataAprovacao' => 'Data Aprovacao',
        ];
    }

    /**
     * Gets query for [[IdContrato0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdContrato0()
    {
        return $this->hasOne(Contrato::className(), ['id' => 'idContrato']);
    }

    /**
     * Gets query for [[IdContratoOpcao0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdContratoOpcao0()
    {
        return $this->hasOne(ContratoOpcao::className(), ['id' => 'idContratoOpcao']);
    }
}
