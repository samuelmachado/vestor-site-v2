<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Regiao".
 *
 * @property int $id
 * @property string|null $regiao
 * @property string $sigla
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property RegiaoBairro[] $regiaoBairros
 * @property Bairro[] $idBairros
 */
class Regiao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Regiao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sigla'], 'required'],
            [['criado', 'atualizado'], 'safe'],
            [['regiao'], 'string', 'max' => 20],
            [['sigla'], 'string', 'max' => 3],
            [['regiao', 'sigla'], 'unique', 'targetAttribute' => ['regiao', 'sigla']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'regiao' => 'Regiao',
            'sigla' => 'Sigla',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[RegiaoBairros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegiaoBairros()
    {
        return $this->hasMany(RegiaoBairro::className(), ['idRegiao' => 'id']);
    }

    /**
     * Gets query for [[IdBairros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdBairros()
    {
        return $this->hasMany(Bairro::className(), ['id' => 'idBairro'])->viaTable('RegiaoBairro', ['idRegiao' => 'id']);
    }
}
