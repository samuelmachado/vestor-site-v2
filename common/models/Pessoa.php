<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Pessoa".
 *
 * @property int $id Código
 * @property string $nome Nome
 * @property string $RG RG
 * @property int $orgaoEmissor Órgão Emissor
 * @property int $CPF CPF
 * @property string $dataNascimento Data de nascimento
 * @property int|null $celular Celular
 * @property int|null $contatarPorWhatsapp Contar somente via Whatsapp
 * @property int|null $telefone Telefone
 * @property int|null $celularValidado
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Imovel[] $imovels
 * @property PessoaContrato[] $pessoaContratos
 * @property Contrato[] $idContratos
 * @property Usuario $usuario
 */
class Pessoa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Pessoa';
    }
    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        $unsets = [
            'RG',
            'CPF',
            'orgaoEmissor',
            'celular',
            'contatarPorWhatsapp',
            'dataNascimento',
            'celularValidado',
            'criado'
        ];
        foreach($unsets as $unsetField)
            unset($fields[$unsetField]);
        return $fields;
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefone'] , 'required'],
            [['nome', 'RG', 'orgaoEmissor', 'CPF', 'dataNascimento'], 'required', 'on' => 'update'],
            [['orgaoEmissor', 'celular', 'contatarPorWhatsapp', 'celularValidado'], 'integer'],
            [['nome', 'RG', 'orgaoEmissor', 'CPF','dataNascimento', 'criado', 'atualizado','telefone','CPF'], 'safe'],
            [['nome'], 'string', 'max' => 200],
            [['RG'], 'string', 'max' => 20],
            [['CPF'], 'string', 'max' => 11],
            // [['telefone'], 'string', 'max' => 11],
            [['CPF','telefone'], 'unique'],
            [['RG', 'orgaoEmissor'], 'unique', 'targetAttribute' => ['RG', 'orgaoEmissor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'nome' => 'Nome',
            'RG' => 'RG',
            'orgaoEmissor' => 'Órgão Emissor',
            'CPF' => 'CPF',
            'dataNascimento' => 'Data de nascimento',
            'celular' => 'Celular',
            'contatarPorWhatsapp' => 'Contar somente via Whatsapp',
            'telefone' => 'Telefone',
            'celularValidado' => 'Celular Validado',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[Imovels]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImovels()
    {
        return $this->hasMany(Imovel::className(), ['idProprietarioPF' => 'id']);
    }

    /**
     * Gets query for [[PessoaContratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaContratos()
    {
        return $this->hasMany(PessoaContrato::className(), ['idPessoa' => 'id']);
    }

    /**
     * Gets query for [[IdContratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdContratos()
    {
        return $this->hasMany(Contrato::className(), ['id' => 'idContrato'])->viaTable('PessoaContrato', ['idPessoa' => 'id']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['idPessoa' => 'id']);
    }
}
