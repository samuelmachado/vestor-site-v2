<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Opcional".
 *
 * @property int $id Código
 * @property int $tipo Tipo
 * @property string $nome Opcional
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property ImovelOpcional[] $imovelOpcionals
 */
class Opcional extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Opcional';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo'], 'integer'],
            [['nome'], 'required'],
            [['criado', 'atualizado'], 'safe'],
            [['nome'], 'string', 'max' => 50],
            [['tipo', 'nome'], 'unique', 'targetAttribute' => ['tipo', 'nome']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'tipo' => 'Tipo',
            'nome' => 'Opcional',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[ImovelOpcionals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImovelOpcionals()
    {
        return $this->hasMany(ImovelOpcional::className(), ['idOpcional' => 'id']);
    }
}
