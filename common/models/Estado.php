<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Estado".
 *
 * @property int $id Código
 * @property string $nome Nome
 * @property string $sigla UF
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Cep[] $ceps
 * @property Cidade[] $cidades
 */
class Estado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Estado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'sigla'], 'required'],
            [['criado', 'atualizado'], 'safe'],
            [['nome'], 'string', 'max' => 50],
            [['sigla'], 'string', 'max' => 2],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        $unsets = [
            'atualizado',
            'criado'
        ];
        foreach($unsets as $unsetField)
            unset($fields[$unsetField]);
        
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'nome' => 'Nome',
            'sigla' => 'UF',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[Ceps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCeps()
    {
        return $this->hasMany(Cep::className(), ['idEstado' => 'id']);
    }

    /**
     * Gets query for [[Cidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCidades()
    {
        return $this->hasMany(Cidade::className(), ['idEstado' => 'id']);
    }
}
