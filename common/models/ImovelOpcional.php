<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ImovelOpcional".
 *
 * @property int $id Código
 * @property int $idImovel Imóvel
 * @property int $idOpcional Opcional
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Imovel $idImovel0
 * @property Opcional $idOpcional0
 */
class ImovelOpcional extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ImovelOpcional';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idImovel', 'idOpcional'], 'required'],
            [['idImovel', 'idOpcional'], 'integer'],
            [['criado', 'atualizado'], 'safe'],
            [['idImovel'], 'exist', 'skipOnError' => true, 'targetClass' => Imovel::className(), 'targetAttribute' => ['idImovel' => 'id']],
            [['idOpcional'], 'exist', 'skipOnError' => true, 'targetClass' => Opcional::className(), 'targetAttribute' => ['idOpcional' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idImovel' => 'Imóvel',
            'idOpcional' => 'Opcional',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[IdImovel0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdImovel0()
    {
        return $this->hasOne(Imovel::className(), ['id' => 'idImovel']);
    }

    /**
     * Gets query for [[IdOpcional0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdOpcional0()
    {
        return $this->hasOne(Opcional::className(), ['id' => 'idOpcional']);
    }
}
