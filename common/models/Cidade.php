<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Cidade".
 *
 * @property int $id Código
 * @property int $idEstado Estado
 * @property string $nome Nome
 * @property int $validado Validado
 * @property string|null $criado
 *
 * @property Bairro[] $bairros
 * @property Cep[] $ceps
 * @property Estado $idEstado0
 */
class Cidade extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Cidade';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEstado', 'nome'], 'required'],
            [['idEstado', 'validado'], 'integer'],
            [['criado'], 'safe'],
            [['nome'], 'string', 'max' => 50],
            [['idEstado', 'nome'], 'unique', 'targetAttribute' => ['idEstado', 'nome']],
            [['idEstado'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['idEstado' => 'id']],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        $unsets = [
            'atualizado',
            'criado',
            'validado',
            'idEstado',
        ];
        foreach($unsets as $unsetField)
            unset($fields[$unsetField]);
        
        return $fields;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idEstado' => 'Estado',
            'nome' => 'Nome',
            'validado' => 'Validado',
            'criado' => 'Criado',
        ];
    }

    /**
     * Gets query for [[Bairros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBairros()
    {
        return $this->hasMany(Bairro::className(), ['idCidade' => 'id']);
    }

    /**
     * Gets query for [[Ceps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCeps()
    {
        return $this->hasMany(Cep::className(), ['idCidade' => 'id']);
    }

    /**
     * Gets query for [[IdEstado0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstado0()
    {
        return $this->hasOne(Estado::className(), ['id' => 'idEstado']);
    }
}
