<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Complemento".
 *
 * @property int $id Código
 * @property string $tipo Tipo
 * @property string $descricao Descrição
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Empresa[] $empresas
 * @property Imovel[] $imovels
 */
class Complemento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Complemento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo', 'descricao'], 'required'],
            [['criado', 'atualizado'], 'safe'],
            [['tipo'], 'string', 'max' => 10],
            [['descricao'], 'string', 'max' => 20],
            [['tipo'], 'unique'],
            [['descricao'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'tipo' => 'Tipo',
            'descricao' => 'Descrição',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[Empresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresas()
    {
        return $this->hasMany(Empresa::className(), ['idComplemento' => 'id']);
    }

    /**
     * Gets query for [[Imovels]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImovels()
    {
        return $this->hasMany(Imovel::className(), ['idComplemento' => 'id']);
    }
}
