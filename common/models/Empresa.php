<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Empresa".
 *
 * @property int $id Código
 * @property string|null $razaoSocial Razão social
 * @property string|null $nomeFantasia Nome fantasia
 * @property string $cnpj CNPJ
 * @property string|null $telefoneComercial Telefone Comercial
 * @property int $idCEP Endereço
 * @property int|null $idComplemento Complemento
 * @property int $numeroLogradouro
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Documento[] $documentos
 * @property Cep $idCEP0
 * @property Complemento $idComplemento0
 * @property EmpresaUsuario[] $empresaUsuarios
 * @property Usuario[] $idUsuarios
 * @property Imovel[] $imovels
 * @property PessoaContrato[] $pessoaContratos
 * @property Contrato[] $idContratos
 */
class Empresa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cnpj', 'idCEP', 'numeroLogradouro'], 'required'],
            [['idCEP', 'idComplemento', 'numeroLogradouro'], 'integer'],
            [['criado', 'atualizado'], 'safe'],
            [['razaoSocial', 'nomeFantasia'], 'string', 'max' => 255],
            [['cnpj'], 'string', 'max' => 20],
            [['telefoneComercial'], 'string', 'max' => 15],
            [['cnpj'], 'unique'],
            [['idCEP'], 'exist', 'skipOnError' => true, 'targetClass' => Cep::className(), 'targetAttribute' => ['idCEP' => 'id']],
            [['idComplemento'], 'exist', 'skipOnError' => true, 'targetClass' => Complemento::className(), 'targetAttribute' => ['idComplemento' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'razaoSocial' => 'Razão social',
            'nomeFantasia' => 'Nome fantasia',
            'cnpj' => 'CNPJ',
            'telefoneComercial' => 'Telefone Comercial',
            'idCEP' => 'Endereço',
            'idComplemento' => 'Complemento',
            'numeroLogradouro' => 'Numero Logradouro',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[Documentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(Documento::className(), ['idEmpresa' => 'id']);
    }

    /**
     * Gets query for [[IdCEP0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCEP0()
    {
        return $this->hasOne(Cep::className(), ['id' => 'idCEP']);
    }

    /**
     * Gets query for [[IdComplemento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdComplemento0()
    {
        return $this->hasOne(Complemento::className(), ['id' => 'idComplemento']);
    }

    /**
     * Gets query for [[EmpresaUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresaUsuarios()
    {
        return $this->hasMany(EmpresaUsuario::className(), ['idEmpresa' => 'id']);
    }

    /**
     * Gets query for [[IdUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['id' => 'idUsuario'])->viaTable('EmpresaUsuario', ['idEmpresa' => 'id']);
    }

    /**
     * Gets query for [[Imovels]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImovels()
    {
        return $this->hasMany(Imovel::className(), ['idProprietarioPJ' => 'id']);
    }

    /**
     * Gets query for [[PessoaContratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaContratos()
    {
        return $this->hasMany(PessoaContrato::className(), ['idEmpresa' => 'id']);
    }

    /**
     * Gets query for [[IdContratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdContratos()
    {
        return $this->hasMany(Contrato::className(), ['id' => 'idContrato'])->viaTable('PessoaContrato', ['idEmpresa' => 'id']);
    }
}
