<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Usuario".
 *
 * @property int $id Cód.
 * @property string $login Login
 * @property int|null $perfil Perfil
 * @property int|null $idPessoa
 * @property string|null $recoveryKey Chave de recuperação
 * @property string|null $authKey Chave de autenticação
 * @property string $senha Senha
 * @property string|null $email Email
 * @property string|null $ultimoAcesso Último acesso
 * @property string|null $firebase Firebase
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Documento[] $documentos
 * @property EmpresaUsuario[] $empresaUsuarios
 * @property Empresa[] $idEmpresas
 * @property Interacao[] $interacaos
 * @property Notificacao[] $notificacaos
 * @property Pessoa $idPessoa0
 * @property UsuarioPerfil[] $usuarioPerfils
 * @property Perfil[] $idPerfils
 * @property Visita[] $visitas
 * @property VisitaAgendamento[] $visitaAgendamentos
 * @property Vistoria[] $vistorias
 */
class Usuario extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $password;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Usuario';
    }
    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        $unsets = [
            'login',
            'senha',

        ];
        foreach($unsets as $unsetField)
            unset($fields[$unsetField]);

        $fields['pessoa'] = 'pessoa';
        $fields['perfis'] = 'perfis';

        return $fields;
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPessoa'], 'required'],
            [['perfil', 'idPessoa','cadastroFinalizado'], 'integer'],
            [['ultimoAcesso', 'criado', 'atualizado','cadastroFinalizado'], 'safe'],
            [['login'], 'string', 'max' => 30],
            [[ 'authKey', 'senha'], 'string', 'max' => 255],
			[['authKey'], 'string', 'max' => 500 ],
            [['email','nome'], 'string', 'max' => 100],
            [['firebase'], 'string', 'max' => 200],
            [['login'], 'unique'],
            [['recoveryKey'], 'unique'],
            [['authKey'], 'unique'],
            [['email'], 'unique'],
            [['idPessoa'], 'unique'],
            [['idPessoa'], 'exist', 'skipOnError' => true, 'targetClass' => Pessoa::className(), 'targetAttribute' => ['idPessoa' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Cód.',
            'login' => 'Login',
            'perfil' => 'Perfil',
            'idPessoa' => 'Id Pessoa',
            'recoveryKey' => 'Chave de recuperação',
            'authKey' => 'Chave de autenticação',
            'senha' => 'Senha',
            'email' => 'Email',
            'ultimoAcesso' => 'Último acesso',
            'firebase' => 'Firebase',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    public static function decryptID(){
        $cookies = Yii::$app->request->cookies;
        $data = $cookies->getValue('u', '');
        return $id = Yii::$app->getSecurity()->decryptByPassword($data, 'bananastempotassio');
    }

    public static function findByUsername($username)
    {
        //'status' => self::STATUS_ATIVO]
        return static::findOne(['email' => $username]);
    }

    public static function findByLogin($username)
    {
        //'status' => self::STATUS_ATIVO]
        return static::findOne(['email' => $username]);
    }


    public function getAuthKey()
    {
        return $this->authKey;
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }


    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        //echo Yii::$app->security->generatePasswordHash($password);

        return Yii::$app->security->validatePassword($password, $this->passwordHash);
    }

    public static function findIdentity($id)
    {
        // return static::findOne(['id' => $id, 'status' => self::STATUS_ATIVO, 'idPerfil' => self::PERFIL_SUPER_ADMIN]);
    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['id' => $token->getClaim('uid')]);
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function setPassword($password)
    {
        $this->passwordHash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->passwordResetToken = Yii::$app->security->generateRandomString() . '_' . time();
    }
    public static function findByTelefone($telefone)
    {
        $pessoa = Pessoa::find()->where([
            'telefone' => $telefone,
        ])->one();
      
        if(!$pessoa)
            return null;
        return static::findOne([
            'idPessoa' => $pessoa->id,
        ]);
    }
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'passwordResetToken' => $token,
            'status' => self::STATUS_ATIVO,
        ]);
    }
    public function removePasswordResetToken()
    {
        $this->passwordResetToken = null;
    }
    
    /**
     * Gets query for [[Documentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(Documento::className(), ['idUsuarioCriador' => 'id']);
    }

    /**
     * Gets query for [[EmpresaUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresaUsuarios()
    {
        return $this->hasMany(EmpresaUsuario::className(), ['idUsuario' => 'id']);
    }

    /**
     * Gets query for [[IdEmpresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresas()
    {
        return $this->hasMany(Empresa::className(), ['id' => 'idEmpresa'])->viaTable('EmpresaUsuario', ['idUsuario' => 'id']);
    }

    /**
     * Gets query for [[Interacaos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInteracaos()
    {
        return $this->hasMany(Interacao::className(), ['idUsuario' => 'id']);
    }

    /**
     * Gets query for [[Notificacaos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNotificacaos()
    {
        return $this->hasMany(Notificacao::className(), ['idUsuarioReceptor' => 'id']);
    }

    /**
     * Gets query for [[IdPessoa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPessoa()
    {
        return $this->hasOne(Pessoa::className(), ['id' => 'idPessoa']);
    }

    /**
     * Gets query for [[UsuarioPerfils]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerfis()
    {
        return $this->hasMany(UsuarioPerfil::className(), ['idUsuario' => 'id']);
    }

    /**
     * Gets query for [[IdPerfils]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPerfils()
    {
        return $this->hasMany(Perfil::className(), ['id' => 'idPerfil'])->viaTable('UsuarioPerfil', ['idUsuario' => 'id']);
    }

    /**
     * Gets query for [[Visitas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVisitas()
    {
        return $this->hasMany(Visita::className(), ['idUsuarioCorretor' => 'id']);
    }

    /**
     * Gets query for [[VisitaAgendamentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVisitaAgendamentos()
    {
        return $this->hasMany(VisitaAgendamento::className(), ['idUsuarioVisitante' => 'id']);
    }

    /**
     * Gets query for [[Vistorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVistorias()
    {
        return $this->hasMany(Vistoria::className(), ['idUsuarioVistoriador' => 'id']);
    }

    public static function generateToken($id){
        $jwt = Yii::$app->jwt;
        $signer = $jwt->getSigner('HS256');
        $key = $jwt->getKey();
  
        $time = time();
        $token = $jwt->getBuilder()
            ->issuedBy('https://vestor.com.br')// Configures the issuer (iss claim)
            ->permittedFor('https://vestor.com.br')// Configures the audience (aud claim)
            ->identifiedBy('vestor-core', true)// Configures the id (jti claim), replicating as a header item
            ->issuedAt($time)// Configures the time that the token was issue (iat claim)
            ->expiresAt($time + 3600 * 24 * 7)// Configures the expiration time of the token (exp claim)
            ->withClaim('uid', $id)// Configures a new claim, called "uid"
            ->getToken($signer, $key); // Retrieves the generated token
    
        return (string) $token;
    }
}
