<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "VistoriaRelatorio".
 *
 * @property int $id
 * @property int $idVistoria
 * @property int $idVistoriaLocal
 * @property int $idComodo
 * @property string $descricao
 *
 * @property Comodo $idComodo0
 * @property LocalVistoria $idVistoriaLocal0
 * @property Vistoria $idVistoria0
 */
class VistoriaRelatorio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'VistoriaRelatorio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idVistoria', 'idVistoriaLocal', 'idComodo', 'descricao'], 'required'],
            [['idVistoria', 'idVistoriaLocal', 'idComodo'], 'integer'],
            [['descricao'], 'string'],
            [['idComodo'], 'exist', 'skipOnError' => true, 'targetClass' => Comodo::className(), 'targetAttribute' => ['idComodo' => 'id']],
            [['idVistoriaLocal'], 'exist', 'skipOnError' => true, 'targetClass' => LocalVistoria::className(), 'targetAttribute' => ['idVistoriaLocal' => 'id']],
            [['idVistoria'], 'exist', 'skipOnError' => true, 'targetClass' => Vistoria::className(), 'targetAttribute' => ['idVistoria' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idVistoria' => 'Id Vistoria',
            'idVistoriaLocal' => 'Id Vistoria Local',
            'idComodo' => 'Id Comodo',
            'descricao' => 'Descricao',
        ];
    }

    /**
     * Gets query for [[IdComodo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdComodo0()
    {
        return $this->hasOne(Comodo::className(), ['id' => 'idComodo']);
    }

    /**
     * Gets query for [[IdVistoriaLocal0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdVistoriaLocal0()
    {
        return $this->hasOne(LocalVistoria::className(), ['id' => 'idVistoriaLocal']);
    }

    /**
     * Gets query for [[IdVistoria0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdVistoria0()
    {
        return $this->hasOne(Vistoria::className(), ['id' => 'idVistoria']);
    }
}
