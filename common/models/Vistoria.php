<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Vistoria".
 *
 * @property int $id
 * @property int $status 1=agendado, 2=realizado, 3=homologado com anexos,4=aceito
 * @property int $idContrato
 * @property int $idUsuarioVistoriador
 * @property string $dataVistoria
 * @property string|null $validadoProprietario
 * @property string|null $observacoes
 *
 * @property Contrato $idContrato0
 * @property Usuario $idUsuarioVistoriador0
 * @property VistoriaRelatorio[] $vistoriaRelatorios
 */
class Vistoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Vistoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'idContrato', 'idUsuarioVistoriador'], 'integer'],
            [['idContrato', 'idUsuarioVistoriador', 'dataVistoria'], 'required'],
            [['dataVistoria', 'validadoProprietario'], 'safe'],
            [['observacoes'], 'string'],
            [['idContrato'], 'exist', 'skipOnError' => true, 'targetClass' => Contrato::className(), 'targetAttribute' => ['idContrato' => 'id']],
            [['idUsuarioVistoriador'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuarioVistoriador' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => '1=agendado, 2=realizado, 3=homologado com anexos,4=aceito',
            'idContrato' => 'Id Contrato',
            'idUsuarioVistoriador' => 'Id Usuario Vistoriador',
            'dataVistoria' => 'Data Vistoria',
            'validadoProprietario' => 'Validado Proprietario',
            'observacoes' => 'Observacoes',
        ];
    }

    /**
     * Gets query for [[IdContrato0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdContrato0()
    {
        return $this->hasOne(Contrato::className(), ['id' => 'idContrato']);
    }

    /**
     * Gets query for [[IdUsuarioVistoriador0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarioVistoriador0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuarioVistoriador']);
    }

    /**
     * Gets query for [[VistoriaRelatorios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVistoriaRelatorios()
    {
        return $this->hasMany(VistoriaRelatorio::className(), ['idVistoria' => 'id']);
    }
}
