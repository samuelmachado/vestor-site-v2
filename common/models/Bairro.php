<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Bairro".
 *
 * @property int $id Código
 * @property int $idCidade Cidade
 * @property string $nome Nome
 * @property int|null $validado Validado
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Cidade $idCidade0
 * @property Cep[] $ceps
 * @property Cep[] $ceps0
 * @property RegiaoBairro[] $regiaoBairros
 * @property Regiao[] $idRegiaos
 */
class Bairro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Bairro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCidade', 'nome'], 'required'],
            [['idCidade', 'validado'], 'integer'],
            [['criado', 'atualizado'], 'safe'],
            [['nome'], 'string', 'max' => 50],
            [['idCidade', 'nome'], 'unique', 'targetAttribute' => ['idCidade', 'nome']],
            [['idCidade'], 'exist', 'skipOnError' => true, 'targetClass' => Cidade::className(), 'targetAttribute' => ['idCidade' => 'id']],
        ];
    }
    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        $unsets = [
            'atualizado',
            'criado',
            'validado'
        ];
        foreach($unsets as $unsetField)
            unset($fields[$unsetField]);
        
        return $fields;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idCidade' => 'Cidade',
            'nome' => 'Nome',
            'validado' => 'Validado',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[IdCidade0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCidade0()
    {
        return $this->hasOne(Cidade::className(), ['id' => 'idCidade']);
    }

    /**
     * Gets query for [[Ceps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCeps()
    {
        return $this->hasMany(Cep::className(), ['idBairro' => 'id']);
    }

    /**
     * Gets query for [[Ceps0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCeps0()
    {
        return $this->hasMany(Cep::className(), ['idBairro' => 'id']);
    }

    /**
     * Gets query for [[RegiaoBairros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegiaoBairros()
    {
        return $this->hasMany(RegiaoBairro::className(), ['idBairro' => 'id']);
    }

    /**
     * Gets query for [[IdRegiaos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdRegiaos()
    {
        return $this->hasMany(Regiao::className(), ['id' => 'idRegiao'])->viaTable('RegiaoBairro', ['idBairro' => 'id']);
    }
}
