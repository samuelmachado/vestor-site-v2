<?php

namespace common\models;

use Yii;
use Twilio\Rest\Client;

/**
 * This is the model class for table "Pin".
 *
 * @property int $id
 * @property string $telefone
 * @property string $pin
 * @property int $usado
 * @property string $atualizado
 * @property string $criado
 */
class Pin extends \yii\db\ActiveRecord
{
    public $isTeste = false;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Pin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefone'], 'required'],
            [['pin'], 'required', 'on' => 'autenticar'],

            [['usado'], 'integer'],
            [['atualizado', 'criado', 'pin', 'isTeste','telefone'], 'safe'],
            // [['telefone'], 'string', 'max' => 11],
            // [['pin'], 'string', 'max' => 4],
            // [['telefone'], 'unique'],
            [['pin'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telefone' => 'Telefone',
            'pin' => 'Pin',
            'usado' => 'Usado',
            'atualizado' => 'Atualizado',
            'criado' => 'Criado',
        ];
    }

    public static function searchPin($telefone, $meuPin=''){
        $pin = self::find();
        
        // if($telefone)
            // $pin->andWhere(['telefone' => trim($telefone)]);
        if($meuPin)
            $pin->andWhere(['pin' => $meuPin]);
        return  $pin->one();;
    }

    static public function gerar(&$model){
        // $model = new Pin();
        // $model->telefone = $telefone;
        // $model->pin = $pin;
        $twilio = '';
        if(!$model->isTeste){
            $twilio = self::enviar($model->telefone, $model->pin);
        }

        $response = [
                'id' => $model->id,
                'twilio' => [
                    'status' => $twilio ? $twilio->status : null,
                    'errorCode' => $twilio ? $twilio->errorCode: null,
                    'errorMessage' => $twilio ? $twilio->errorMessage : null
                ]
            ];

        if($model->isTeste){
            $response['telefone'] = $model->telefone;
            $response['pin'] = $model->pin;
        }
        return $response;
    }

    static public function enviar($telefone, $pin){
        // Your Account SID and Auth Token from twilio.com/console
        $sid = 'AC159e42540d3d9204030184be717f2b3f';
        $token = 'a1e507209d80ffab2925598a509b452b';
        $client = new Client($sid, $token);

        // Use the client to do fun stuff like send text messages!
        $message = $client->messages->create(
            // the number you'd like to send the message to
            '+55'.$telefone,
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => '+18509404699',
                // the body of the text message you'd like to send
                'body' => $pin.'👈 Seu PIN de autorização da Véstor'
            )
        );
        
        return $message;
    }
}
