<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "TipoDocumento".
 *
 * @property int $id Código
 * @property string $nome Nome
 *
 * @property Documento[] $documentos
 */
class TipoDocumento extends \yii\db\ActiveRecord
{
    const TIPO_THUMBNAILS = 1;
    const TIPO_RG_CPF = 2;
    const TIPO_COMP_END = 3;
    const TIPO_COMP_RENDA = 4;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TipoDocumento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['nome'], 'string', 'max' => 50],
            [['nome'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'nome' => 'Nome',
        ];
    }

    /**
     * Gets query for [[Documentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(Documento::className(), ['idTipoDocumento' => 'id']);
    }
}
