<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Documento".
 *
 * @property int $id Código
 * @property int|null $idTipoDocumento Tipo
 * @property int|null $idImovel Imóvel
 * @property int|null $idUsuarioCriador Usuário
 * @property int|null $idEmpresa Empresa
 * @property int|null $idContrato
 * @property string $formato Formato do arquivo
 * @property string $arquivo Arquivo
 * @property int|null $capa Capa
 * @property int|null $ordem Ordem
 *
 * @property Contrato $idContrato0
 * @property Empresa $idEmpresa0
 * @property Imovel $idImovel0
 * @property TipoDocumento $idTipoDocumento0
 * @property Usuario $idUsuarioCriador0
 */
class Documento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Documento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idSessao','idTipoDocumento', 'idImovel', 'idUsuarioCriador', 'idEmpresa', 'idContrato', 'capa', 'ordem'], 'integer'],
            [['arquivo'], 'required'],
            [['dataCadastro','nome'], 'safe'],
            [['formato'], 'string', 'max' => 10],
            [['arquivo'], 'string', 'max' => 200],
            [['idContrato'], 'exist', 'skipOnError' => true, 'targetClass' => Contrato::className(), 'targetAttribute' => ['idContrato' => 'id']],
            [['idEmpresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['idEmpresa' => 'id']],
            [['idImovel'], 'exist', 'skipOnError' => true, 'targetClass' => Imovel::className(), 'targetAttribute' => ['idImovel' => 'id']],
            [['idTipoDocumento'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumento::className(), 'targetAttribute' => ['idTipoDocumento' => 'id']],
            [['idUsuarioCriador'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuarioCriador' => 'id']],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        $unsets = [
            'idImovel',
            'idUsuarioCriador',
            'idEmpresa',
            'idImovel',
            'idContrato',
            'ordem'
        ];
        foreach($unsets as $unsetField)
            unset($fields[$unsetField]);


        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idTipoDocumento' => 'Tipo',
            'idImovel' => 'Imóvel',
            'idUsuarioCriador' => 'Usuário',
            'idEmpresa' => 'Empresa',
            'idContrato' => 'Id Contrato',
            'formato' => 'Formato do arquivo',
            'arquivo' => 'Arquivo',
            'capa' => 'Capa',
            'ordem' => 'Ordem',
        ];
    }

    /**
     * Gets query for [[IdContrato0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdContrato0()
    {
        return $this->hasOne(Contrato::className(), ['id' => 'idContrato']);
    }

    /**
     * Gets query for [[IdEmpresa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresa0()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'idEmpresa']);
    }

    /**
     * Gets query for [[IdImovel0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdImovel0()
    {
        return $this->hasOne(Imovel::className(), ['id' => 'idImovel']);
    }

    /**
     * Gets query for [[IdTipoDocumento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdTipoDocumento0()
    {
        return $this->hasOne(TipoDocumento::className(), ['id' => 'idTipoDocumento']);
    }

    /**
     * Gets query for [[IdUsuarioCriador0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarioCriador0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuarioCriador']);
    }
}
