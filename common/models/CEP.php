<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CEP".
 *
 * @property int $id Código
 * @property string $CEP CEP
 * @property int $idTipoLogradouro Tipo de logradouro
 * @property int $idLogradouro Logradouro
 * @property int $idBairro Bairro
 * @property int $idCidade Cidade
 * @property int $idEstado Estado
 * @property int $validado Validado
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Bairro $idBairro0
 * @property Bairro $idBairro1
 * @property Cidade $idCidade0
 * @property Estado $idEstado0
 * @property Logradouro $idLogradouro0
 * @property TipoLogradouro $idTipoLogradouro0
 * @property Empresa[] $empresas
 * @property Imovel[] $imovels
 */
class CEP extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CEP';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CEP', 'idTipoLogradouro', 'idLogradouro', 'idBairro', 'idCidade', 'idEstado'], 'required'],
            [['idTipoLogradouro', 'idLogradouro', 'idBairro', 'idCidade', 'idEstado', 'validado'], 'integer'],
            [['criado', 'atualizado'], 'safe'],
            [['CEP'], 'string', 'max' => 8],
            [['CEP'], 'unique'],
            [['idBairro'], 'exist', 'skipOnError' => true, 'targetClass' => Bairro::className(), 'targetAttribute' => ['idBairro' => 'id']],
            [['idCidade'], 'exist', 'skipOnError' => true, 'targetClass' => Cidade::className(), 'targetAttribute' => ['idCidade' => 'id']],
            [['idEstado'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['idEstado' => 'id']],
            [['idLogradouro'], 'exist', 'skipOnError' => true, 'targetClass' => Logradouro::className(), 'targetAttribute' => ['idLogradouro' => 'id']],
            [['idTipoLogradouro'], 'exist', 'skipOnError' => true, 'targetClass' => TipoLogradouro::className(), 'targetAttribute' => ['idTipoLogradouro' => 'id']],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        $unsets = [
            'idTipoLogradouro',
            'idLogradouro',
            'idBairro',
            'idCidade',
            'idEstado',
            'validado',
            'atualizado',
            'criado'
        ];
        foreach($unsets as $unsetField)
            unset($fields[$unsetField]);

        $fields['estado'] = 'estado';
        $fields['cidade'] = 'cidade';
        $fields['bairro'] = 'bairro';
        $fields['tipoLogradouro'] = 'tipoLogradouro';
        $fields['logradouro'] = 'logradouro';
        $fields['completo'] = 'completo';

        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'CEP' => 'CEP',
            'idTipoLogradouro' => 'Tipo de logradouro',
            'idLogradouro' => 'Logradouro',
            'idBairro' => 'Bairro',
            'idCidade' => 'Cidade',
            'idEstado' => 'Estado',
            'validado' => 'Validado',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }



    public function getCompleto(){
        return $this->tipoLogradouro->abreviacao. '. '.$this->logradouro->nome.', '.$this->bairro->nome.' - '.$this->cidade->nome;
    }

    public function getEnderecoSimples(){
        return $this->tipoLogradouro->abreviacao. '. '.$this->logradouro->nome.', '.$this->bairro->nome;
    }
    /**
     * Gets query for [[IdBairro1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBairro()
    {
        return $this->hasOne(Bairro::className(), ['id' => 'idBairro']);
    }

    /**
     * Gets query for [[IdCidade0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCidade()
    {
        return $this->hasOne(Cidade::className(), ['id' => 'idCidade']);
    }

    /**
     * Gets query for [[IdEstado0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(Estado::className(), ['id' => 'idEstado']);
    }

    /**
     * Gets query for [[IdLogradouro0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLogradouro()
    {
        return $this->hasOne(Logradouro::className(), ['id' => 'idLogradouro']);
    }

    /**
     * Gets query for [[IdTipoLogradouro0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoLogradouro()
    {
        return $this->hasOne(TipoLogradouro::className(), ['id' => 'idTipoLogradouro']);
    }

    /**
     * Gets query for [[Empresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresas()
    {
        return $this->hasMany(Empresa::className(), ['idCEP' => 'id']);
    }


    public function getAll()
    {
        $cep = Cep::find()
        ->joinWith('cidade')
        ->joinWith('bairro')
        ->joinWith('logradouro')
        ->joinWith('estado');

        return $cep->all();
    }

    /**
     * Gets query for [[Imovels]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getImovels()
    // {
    //     return $this->hasMany(Imovel::className(), ['idCEP' => 'id']);
    // }
}
