<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Respostas".
 *
 * @property int $id
 * @property int|null $idPessoa
 * @property string|null $nome
 * @property int|null $origem
 * @property string|null $key
 * @property string|null $tipo
 * @property string|null $text
 * @property string|null $json
 * @property string|null $numero
 * @property string|null $registrado
 *
 * @property Pessoa $idPessoa0
 */
class Respostas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Respostas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['idPessoa', 'origem'], 'integer'],
            // [['text', 'json'], 'string'],
            [['idPessoa','origem','registrado','text','json','numero','key','type','nome','tipo'], 'safe'],
            // [['nome'], 'string', 'max' => 100],
            // [['key', 'numero'], 'string', 'max' => 50],
            // [['tipo'], 'string', 'max' => 255],
            [['idPessoa'], 'exist', 'skipOnError' => true, 'targetClass' => Pessoa::className(), 'targetAttribute' => ['idPessoa' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idPessoa' => 'Id Pessoa',
            'nome' => 'Nome',
            'origem' => 'Origem',
            'key' => 'Key',
            'tipo' => 'Tipo',
            'text' => 'Text',
            'json' => 'Json',
            'numero' => 'Numero',
            'registrado' => 'Registrado',
        ];
    }

    /**
     * Gets query for [[IdPessoa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPessoa0()
    {
        return $this->hasOne(Pessoa::className(), ['id' => 'idPessoa']);
    }
}
