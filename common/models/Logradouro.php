<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Logradouro".
 *
 * @property int $id Código
 * @property string $nome Nome
 * @property int $validado Validado
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Cep[] $ceps
 */
class Logradouro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Logradouro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['validado'], 'integer'],
            [['criado', 'atualizado'], 'safe'],
            [['nome'], 'string', 'max' => 255],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        $unsets = [
            'validado',
            'atualizado',
            'criado'
        ];
        foreach($unsets as $unsetField)
            unset($fields[$unsetField]);

        return $fields;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'nome' => 'Nome',
            'validado' => 'Validado',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[Ceps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCeps()
    {
        return $this->hasMany(Cep::className(), ['idLogradouro' => 'id']);
    }
}
