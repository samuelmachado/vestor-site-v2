<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Imovel;

/**
 * ImovelSearch represents the model behind the search form of `common\models\Imovel`.
 */
class ImovelSearch extends Imovel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'idProprietarioPF', 'idProprietarioPJ', 'categoria', 'tipo', 'estrutura', 'idCEP', 'numeroLogradouro', 'idComplemento', 'quarto', 'vagaGaragem', 'banheiro', 'area', 'areaTerreno'], 'integer'],
            [['latitude', 'longitude', 'valor', 'valorCondominio', 'valorIPTU', 'valorSeguroIncendio', 'valorRendaMinima'], 'number'],
            [['valorComplemento', 'descricao', 'linkStreetView', 'criado', 'atualizado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Imovel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'idProprietarioPF' => $this->idProprietarioPF,
            'idProprietarioPJ' => $this->idProprietarioPJ,
            'categoria' => $this->categoria,
            'tipo' => $this->tipo,
            'estrutura' => $this->estrutura,
            'idCEP' => $this->idCEP,
            'numeroLogradouro' => $this->numeroLogradouro,
            'idComplemento' => $this->idComplemento,
            'quarto' => $this->quarto,
            'vagaGaragem' => $this->vagaGaragem,
            'banheiro' => $this->banheiro,
            'area' => $this->area,
            'areaTerreno' => $this->areaTerreno,
            'valor' => $this->valor,
            'valorCondominio' => $this->valorCondominio,
            'valorIPTU' => $this->valorIPTU,
            'valorSeguroIncendio' => $this->valorSeguroIncendio,
            'valorRendaMinima' => $this->valorRendaMinima,
            'criado' => $this->criado,
            'atualizado' => $this->atualizado,
        ]);

        $query->andFilterWhere(['like', 'valorComplemento', $this->valorComplemento])
            ->andFilterWhere(['like', 'descricao', $this->descricao])
            ->andFilterWhere(['like', 'linkStreetView', $this->linkStreetView]);

        return $dataProvider;
    }
}
