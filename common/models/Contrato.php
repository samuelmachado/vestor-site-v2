<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Contrato".
 *
 * @property int $id
 * @property int $idImovel
 * @property float $valorAluguel
 * @property int $duracao
 * @property string|null $observacaoImpresso
 * @property float $indiceReajuste
 * @property float $valorAdministracao
 * @property float|null $valorCondominio
 * @property float|null $condicaoLocacao Concedido algum desconto para o locatário por um X tempo
 * @property int|null $duracaoCondicaoLocacao Duracao meses
 * @property int $finalidadeLocacao Residencial/Comercial
 * @property string $inicio Inicio do contrato
 * @property string $fim fim do contrato
 * @property int $prazoMinimoPermanencia
 * @property int $diaPagamento
 *
 * @property Imovel $idImovel0
 * @property ContratoResposta[] $contratoRespostas
 * @property Documento[] $documentos
 * @property PessoaContrato[] $pessoaContratos
 * @property Empresa[] $idEmpresas
 * @property Pessoa[] $idPessoas
 * @property Vistoria[] $vistorias
 */
class Contrato extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Contrato';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idImovel', 'valorAluguel', 'duracao', 'inicio', 'fim','idUsuarioInquilino'], 'required'],
            [['idImovel', 'duracao', 'duracaoCondicaoLocacao', 'finalidadeLocacao', 'prazoMinimoPermanencia', 'diaPagamento'], 'integer'],
            [['valorAluguel', 'indiceReajuste', 'valorAdministracao', 'valorCondominio', 'condicaoLocacao'], 'number'],
            [['observacaoImpresso'], 'string'],
            [['inicio', 'fim'], 'safe'],
            [['idImovel'], 'exist', 'skipOnError' => true, 'targetClass' => Imovel::className(), 'targetAttribute' => ['idImovel' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idImovel' => 'Id Imovel',
            'valorAluguel' => 'Valor Aluguel',
            'duracao' => 'Duracao',
            'observacaoImpresso' => 'Observacao Impresso',
            'indiceReajuste' => 'Indice Reajuste',
            'valorAdministracao' => 'Valor Administracao',
            'valorCondominio' => 'Valor Condominio',
            'condicaoLocacao' => 'Concedido algum desconto para o locatário por um X tempo',
            'duracaoCondicaoLocacao' => 'Duracao meses',
            'finalidadeLocacao' => 'Residencial/Comercial',
            'inicio' => 'Inicio do contrato',
            'fim' => 'fim do contrato',
            'prazoMinimoPermanencia' => 'Prazo Minimo Permanencia',
            'diaPagamento' => 'Dia Pagamento',
        ];
    }

    /**
     * Gets query for [[IdImovel0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdImovel0()
    {
        return $this->hasOne(Imovel::className(), ['id' => 'idImovel']);
    }

    /**
     * Gets query for [[ContratoRespostas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratoRespostas()
    {
        return $this->hasMany(ContratoResposta::className(), ['idContrato' => 'id']);
    }

    /**
     * Gets query for [[Documentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(Documento::className(), ['idContrato' => 'id']);
    }

    /**
     * Gets query for [[PessoaContratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPessoaContratos()
    {
        return $this->hasMany(PessoaContrato::className(), ['idContrato' => 'id']);
    }

    /**
     * Gets query for [[IdEmpresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresas()
    {
        return $this->hasMany(Empresa::className(), ['id' => 'idEmpresa'])->viaTable('PessoaContrato', ['idContrato' => 'id']);
    }

    /**
     * Gets query for [[IdPessoas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPessoas()
    {
        return $this->hasMany(Pessoa::className(), ['id' => 'idPessoa'])->viaTable('PessoaContrato', ['idContrato' => 'id']);
    }

    /**
     * Gets query for [[Vistorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVistorias()
    {
        return $this->hasMany(Vistoria::className(), ['idContrato' => 'id']);
    }
}
