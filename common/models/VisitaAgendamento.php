<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "VisitaAgendamento".
 *
 * @property int $id
 * @property int $disponivel Se a visita está disponível ou já foi agendada
 * @property int $idAnuncio
 * @property int|null $idUsuarioVisitante
 * @property string $data
 * @property string|null $confirmadoVisitante
 * @property string|null $confirmadoProprietario
 *
 * @property Anuncio $idAnuncio0
 * @property Usuario $idUsuarioVisitante0
 */
class VisitaAgendamento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'VisitaAgendamento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['disponivel', 'idAnuncio', 'idUsuarioVisitante'], 'integer'],
            [['idAnuncio', 'data'], 'required'],
            [['data', 'confirmadoVisitante', 'confirmadoProprietario'], 'safe'],
            [['idAnuncio'], 'exist', 'skipOnError' => true, 'targetClass' => Anuncio::className(), 'targetAttribute' => ['idAnuncio' => 'id']],
            [['idUsuarioVisitante'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuarioVisitante' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'disponivel' => 'Se a visita está disponível ou já foi agendada',
            'idAnuncio' => 'Id Anuncio',
            'idUsuarioVisitante' => 'Id Usuario Visitante',
            'data' => 'Data',
            'confirmadoVisitante' => 'Confirmado Visitante',
            'confirmadoProprietario' => 'Confirmado Proprietario',
        ];
    }

    /**
     * Gets query for [[IdAnuncio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAnuncio0()
    {
        return $this->hasOne(Anuncio::className(), ['id' => 'idAnuncio']);
    }

    /**
     * Gets query for [[IdUsuarioVisitante0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarioVisitante0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuarioVisitante']);
    }
}
