<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "PessoaContrato".
 *
 * @property int $id
 * @property int|null $idPessoa
 * @property int|null $idEmpresa
 * @property int $idContrato
 * @property int $tipo Proprietario/Locatario
 *
 * @property Contrato $idContrato0
 * @property Empresa $idEmpresa0
 * @property Pessoa $idPessoa0
 */
class PessoaContrato extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PessoaContrato';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPessoa', 'idEmpresa', 'idContrato', 'tipo'], 'integer'],
            [['idContrato', 'tipo'], 'required'],
            [['idPessoa', 'idContrato'], 'unique', 'targetAttribute' => ['idPessoa', 'idContrato']],
            [['idEmpresa', 'idContrato'], 'unique', 'targetAttribute' => ['idEmpresa', 'idContrato']],
            [['idContrato'], 'exist', 'skipOnError' => true, 'targetClass' => Contrato::className(), 'targetAttribute' => ['idContrato' => 'id']],
            [['idEmpresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['idEmpresa' => 'id']],
            [['idPessoa'], 'exist', 'skipOnError' => true, 'targetClass' => Pessoa::className(), 'targetAttribute' => ['idPessoa' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idPessoa' => 'Id Pessoa',
            'idEmpresa' => 'Id Empresa',
            'idContrato' => 'Id Contrato',
            'tipo' => 'Proprietario/Locatario',
        ];
    }

    /**
     * Gets query for [[IdContrato0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdContrato0()
    {
        return $this->hasOne(Contrato::className(), ['id' => 'idContrato']);
    }

    /**
     * Gets query for [[IdEmpresa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresa0()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'idEmpresa']);
    }

    /**
     * Gets query for [[IdPessoa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPessoa0()
    {
        return $this->hasOne(Pessoa::className(), ['id' => 'idPessoa']);
    }
}
