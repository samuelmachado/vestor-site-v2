<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Anuncio".
 *
 * @property int $id Código
 * @property int $idImovel Imóvel
 * @property int $visualizacoes
 * @property int $origem Origem Site/Corretor
 * @property string $descricao Descrição
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Imovel $idImovel0
 * @property Interacao[] $interacaos
 * @property Visita[] $visitas
 * @property VisitaAgendamento[] $visitaAgendamentos
 */
class Anuncio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Anuncio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idImovel', 'visualizacoes', 'origem', 'descricao'], 'required'],
            [['idImovel', 'visualizacoes', 'origem'], 'integer'],
            [['descricao'], 'string'],
            [['criado', 'atualizado','ativo','atendimento','condizRealidade'], 'safe'],
            [['idImovel'], 'exist', 'skipOnError' => true, 'targetClass' => Imovel::className(), 'targetAttribute' => ['idImovel' => 'id']],
        ];
    }
    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        // unset($fields['id']); 
        // $fields['imovel'] = 'imovel';
        // $fields['endereco'] = 'cep';
        // $fields['thumbnails'] = 'thumbnails';
        return $fields;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idImovel' => 'Imóvel',
            'visualizacoes' => 'Visualizacoes',
            'origem' => 'Origem Site/Corretor',
            'descricao' => 'Descrição',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[IdImovel0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImovel()
    {
        return $this->hasOne(Imovel::className(), ['id' => 'idImovel']);
    }

    /**
     * Gets query for [[Interacaos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInteracaos()
    {
        return $this->hasMany(Interacao::className(), ['idAnuncio' => 'id']);
    }

    /**
     * Gets query for [[Visitas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVisitas()
    {
        return $this->hasMany(Visita::className(), ['idAnuncio' => 'id']);
    }

    /**
     * Gets query for [[VisitaAgendamentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVisitaAgendamentos()
    {
        return $this->hasMany(VisitaAgendamento::className(), ['idAnuncio' => 'id']);
    }
}
