<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Perfil".
 *
 * @property int $id
 * @property string|null $nome
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property UsuarioPerfil[] $usuarioPerfils
 * @property Usuario[] $idUsuarios
 */
class Perfil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Perfil';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['criado', 'atualizado'], 'safe'],
            [['nome'], 'string', 'max' => 50],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        $unsets = [
            'criado',
            'atualizado'
        ];
        foreach($unsets as $unsetField)
            unset($fields[$unsetField]);

   

        return $fields;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[UsuarioPerfils]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioPerfils()
    {
        return $this->hasMany(UsuarioPerfil::className(), ['idPerfil' => 'id']);
    }

    /**
     * Gets query for [[IdUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['id' => 'idUsuario'])->viaTable('UsuarioPerfil', ['idPerfil' => 'id']);
    }
}
