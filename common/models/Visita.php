<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Visita".
 *
 * @property int $id Código
 * @property int $status Status
 * @property int $idUsuarioCorretor Corretor
 * @property int $idAnuncio Anúncio
 * @property int $contadorReagendamento Contador de reagendamento
 * @property int $mostraImovel Quem mostrao imovel
 * @property int|null $visitaConfirmada Visita confirmada
 * @property string $dataVisita Data
 * @property string $dataEntregaChave Quando a chave foi entregue no posto de atentimento
 * @property string $dataDevolucaoChave Quando a chave foi devolvida
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Anuncio $idAnuncio0
 * @property Usuario $idUsuarioCorretor0
 */
class Visita extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Visita';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'idUsuarioCorretor', 'idAnuncio', 'contadorReagendamento', 'mostraImovel', 'visitaConfirmada','idUsuarioAgendado'], 'integer'],
            [['idAnuncio','hora', 'dataVisita'], 'required'],
            // [['idUsuarioCorretor', 'idAnuncio', 'contadorReagendamento', 'dataVisita', 'dataEntregaChave', 'dataDevolucaoChave'], 'required'],

            [['dataVisita', 'dataEntregaChave', 'dataDevolucaoChave', 'criado', 'atualizado','hora'], 'safe'],
            [['idAnuncio'], 'exist', 'skipOnError' => true, 'targetClass' => Anuncio::className(), 'targetAttribute' => ['idAnuncio' => 'id']],
            [['idUsuarioCorretor'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuarioCorretor' => 'id']],
            [['idUsuarioAgendado'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuarioAgendado' => 'id']],

        ];
    }
    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        // unset($fields['id']); 
        $fields['anuncio'] = 'anuncio';
        // $fields['imovel'] = 'imovel';
        // $fields['endereco'] = 'cep';
        // $fields['thumbnails'] = 'thumbnails';
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'status' => 'Status',
            'idUsuarioCorretor' => 'Corretor',
            'idAnuncio' => 'Anúncio',
            'contadorReagendamento' => 'Contador de reagendamento',
            'mostraImovel' => 'Quem mostrao imovel',
            'visitaConfirmada' => 'Visita confirmada',
            'dataVisita' => 'Data',
            'dataEntregaChave' => 'Quando a chave foi entregue no posto de atentimento',
            'dataDevolucaoChave' => 'Quando a chave foi devolvida',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
            'hora' => 'Hora',
        ];
    }

    /**
     * Gets query for [[IdAnuncio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnuncio()
    {
        return $this->hasOne(Anuncio::className(), ['id' => 'idAnuncio']);
    }

    /**
     * Gets query for [[IdUsuarioCorretor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarioCorretor0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuarioCorretor']);
    }
}
