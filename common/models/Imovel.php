<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Imovel".
 *
 * @property int $id Código
 * @property float|null $latitude Latitude
 * @property float|null $longitude Longitude
 * @property int|null $idProprietarioPF Proprietário PF
 * @property int|null $idProprietarioPJ Proprietário PJ
 * @property int $categoria Categoria
 * @property int $tipo Tipo
 * @property int $estrutura Estrutura
 * @property int $idCEP CEP
 * @property int $numeroLogradouro Número
 * @property int|null $idComplemento Complemento
 * @property string|null $valorComplemento Valor do complemento
 * @property string|null $descricao Descrição
 * @property int $quarto Quartos
 * @property int $vagaGaragem Vagas na garagem
 * @property int $banheiro Banheiros
 * @property int $area Área construída
 * @property int|null $areaTerreno Área do terreno
 * @property float $valor Valor
 * @property float|null $valorCondominio Condomínio
 * @property float|null $valorIPTU IPTU
 * @property float|null $valorSeguroIncendio Seguro incêndio
 * @property float $valorRendaMinima Comprovação mínima de renda
 * @property string|null $linkStreetView Link do Street View
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Anuncio[] $anuncios
 * @property Contrato[] $contratos
 * @property Documento[] $documentos
 * @property Cep $idCEP0
 * @property Complemento $idComplemento0
 * @property Empresa $idProprietarioPJ0
 * @property Pessoa $idProprietarioPF0
 * @property ImovelOpcional[] $imovelOpcionals
 */
class Imovel extends \yii\db\ActiveRecord
{
    public $endereco;
    /**
     * {@inheritdoc}
     */

    const FIELDS_EQUALS_TO = [
        'id',
        'categoria'
    ];

    const FIELDS_GREATHER_OR_EQUAL = [
        'quarto',
        'banheiro',
        'vagaGaragem',
    ];

    CONST IN_RANGE = [
        'valor',
        'area',
    ];

    public static function tableName()
    {
        return 'Imovel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['latitude', 'longitude', 'valor', 'valorCondominio', 'valorIPTU', 'valorSeguroIncendio', 'valorRendaMinima'], 'number'],
            [['idProprietarioPF', 'idProprietarioPJ', 'categoria', 'tipo', 'estrutura', 'idCEP', 'numeroLogradouro', 'idComplemento', 'quarto', 'vagaGaragem', 'banheiro', 'area', 'areaTerreno'], 'integer'],
            [['categoria', 'tipo', 'estrutura', 'idCEP', 'numeroLogradouro', 'quarto', 'vagaGaragem', 'banheiro', 'area', 'valor', 'valorRendaMinima'], 'required'],
            [['descricao'], 'string'],
            [['criado', 'atualizado','endereco','idProprietarioUsuario','idSessao'], 'safe'],
            [['valorComplemento'], 'string', 'max' => 30],
            [['linkStreetView'], 'string', 'max' => 255],
            [['idCEP'], 'exist', 'skipOnError' => true, 'targetClass' => Cep::className(), 'targetAttribute' => ['idCEP' => 'id']],
            [['idComplemento'], 'exist', 'skipOnError' => true, 'targetClass' => Complemento::className(), 'targetAttribute' => ['idComplemento' => 'id']],
            [['idProprietarioPJ'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['idProprietarioPJ' => 'id']],
            [['idProprietarioPF'], 'exist', 'skipOnError' => true, 'targetClass' => Pessoa::className(), 'targetAttribute' => ['idProprietarioPF' => 'id']],
            [['idProprietarioUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idProprietarioUsuario' => 'id']],

            
        ];
    }
    public function fields()
    {
        $fields = parent::fields();
    
        // remove fields that contain sensitive information
        // unset($fields['id']); 
        $fields['anuncio'] = 'anuncio';
        $fields['endereco'] = 'cep';
        $fields['thumbnails'] = 'thumbnails';
        $fields['enderecoCompleto'] = 'enderecoCompleto';
        $fields['contrato'] = 'contrato';
        return $fields;
    }
    public function extraFields()
    {
        return ['id'];
    }
    
    // public function getEndereco(){
        
    // }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'idProprietarioPF' => 'Proprietário PF',
            'idProprietarioPJ' => 'Proprietário PJ',
            'categoria' => 'Categoria',
            'tipo' => 'Tipo',
            'estrutura' => 'Estrutura',
            'idCEP' => 'CEP',
            'numeroLogradouro' => 'Número',
            'idComplemento' => 'Complemento',
            'valorComplemento' => 'Valor do complemento',
            'descricao' => 'Descrição',
            'quarto' => 'Quartos',
            'vagaGaragem' => 'Vagas na garagem',
            'banheiro' => 'Banheiros',
            'area' => 'Área construída',
            'areaTerreno' => 'Área do terreno',
            'valor' => 'Valor',
            'valorCondominio' => 'Condomínio',
            'valorIPTU' => 'IPTU',
            'valorSeguroIncendio' => 'Seguro incêndio',
            'valorRendaMinima' => 'Comprovação mínima de renda',
            'linkStreetView' => 'Link do Street View',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

        /**
     * Gets query for [[IdCEP0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContrato()
    {
        return $this->hasOne(Contrato::className(), ['idImovel' => 'id']);
    }

    public function total(){
        return $this->valor + $this->valorCondominio; 
    }
    
    /**
     * Gets query for [[Anuncios]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getAnuncios()
    // {
    //     return $this->hasMany(Anuncio::className(), ['idImovel' => 'id']);
    // }

    /**
     * Gets query for [[Contratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratos()
    {
        return $this->hasMany(Contrato::className(), ['idImovel' => 'id']);
    }
    public function getEnderecoCompleto(){
        $cep = $this->cep;
        return $cep->completo;
    }

    public function getEnderecoSimples(){
        $cep = $this->cep;
        return $cep->enderecoSimples;
    }
    public function getEnderecoPadrao(){
        $cep = $this->cep;
        return $cep->tipoLogradouro->abreviacao. '. '.$cep->logradouro->nome.', Nº'.$this->numeroLogradouro.' '.$cep->bairro->nome;
    }
    /**
     * Gets query for [[Documentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(Documento::className(), ['idImovel' => 'id']);
    }

    public function getThumbnails()
    {
        return $this->hasMany(Documento::className(), ['idImovel' => 'id'])->andWhere(['idTipoDocumento' => TipoDocumento::TIPO_THUMBNAILS]);
    }

    /**
     * Gets query for [[IdCEP0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCep()
    {
        return $this->hasOne(CEP::className(), ['id' => 'idCEP']);
    }

        /**
     * Gets query for [[IdCEP0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnuncio()
    {
        return $this->hasOne(Anuncio::className(), ['idImovel' => 'id'])->andWhere(['ativo' => 1]);;
    }

    /**
     * Gets query for [[IdComplemento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdComplemento0()
    {
        return $this->hasOne(Complemento::className(), ['id' => 'idComplemento']);
    }

    /**
     * Gets query for [[IdProprietarioPJ0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProprietarioPJ0()
    {
        return $this->hasOne(Empresa::className(), ['id' => 'idProprietarioPJ']);
    }

    /**
     * Gets query for [[IdProprietarioPF0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProprietarioPF0()
    {
        return $this->hasOne(Pessoa::className(), ['id' => 'idProprietarioPF']);
    }

    /**
     * Gets query for [[ImovelOpcionals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImovelOpcionals()
    {
        return $this->hasMany(ImovelOpcional::className(), ['idImovel' => 'id']);
    }
}
