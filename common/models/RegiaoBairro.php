<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "RegiaoBairro".
 *
 * @property int $id Código
 * @property int $idBairro Bairro
 * @property int $idRegiao Região
 * @property string|null $criado
 * @property string|null $atualizado
 *
 * @property Bairro $idBairro0
 * @property Regiao $idRegiao0
 */
class RegiaoBairro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'RegiaoBairro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idBairro', 'idRegiao'], 'required'],
            [['idBairro', 'idRegiao'], 'integer'],
            [['criado', 'atualizado'], 'safe'],
            [['idBairro', 'idRegiao'], 'unique', 'targetAttribute' => ['idBairro', 'idRegiao']],
            [['idBairro'], 'exist', 'skipOnError' => true, 'targetClass' => Bairro::className(), 'targetAttribute' => ['idBairro' => 'id']],
            [['idRegiao'], 'exist', 'skipOnError' => true, 'targetClass' => Regiao::className(), 'targetAttribute' => ['idRegiao' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'idBairro' => 'Bairro',
            'idRegiao' => 'Região',
            'criado' => 'Criado',
            'atualizado' => 'Atualizado',
        ];
    }

    /**
     * Gets query for [[IdBairro0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdBairro0()
    {
        return $this->hasOne(Bairro::className(), ['id' => 'idBairro']);
    }

    /**
     * Gets query for [[IdRegiao0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdRegiao0()
    {
        return $this->hasOne(Regiao::className(), ['id' => 'idRegiao']);
    }
}
