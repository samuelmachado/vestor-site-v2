<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ContratoOpcao".
 *
 * @property int $id
 * @property string $nome
 * @property string $valorContrato Texto a ser incluindo no pdf do contrato
 * @property int $ativo
 *
 * @property ContratoResposta[] $contratoRespostas
 */
class ContratoOpcao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ContratoOpcao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['valorContrato'], 'string'],
            [['ativo'], 'integer'],
            [['nome'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'valorContrato' => 'Texto a ser incluindo no pdf do contrato',
            'ativo' => 'Ativo',
        ];
    }

    /**
     * Gets query for [[ContratoRespostas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratoRespostas()
    {
        return $this->hasMany(ContratoResposta::className(), ['idContratoOpcao' => 'id']);
    }
}
