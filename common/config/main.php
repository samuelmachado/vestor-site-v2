<?php
return [
	'name' => 'Car sharing',
	'language' => 'pt-br',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class'=>'yii\caching\DbCache',

            // 'class' => 'yii\caching\ApcCache',
            // 'keyPrefix' => 'vestor-cache',       // a unique cache key prefix
        ],
        'fileTable' => [
            'class' => 'common\components\FileTable',
        ],
        'dateCheck' => [
            'class' => 'common\components\DateCheck',
        ],
        'formatter' => [
            'class' => 'common\components\MyFormatter',
        ],
        'arrayPicker' => [
            'class' => 'common\components\ArrayPicker',
        ],
        'LocacaoUrl' => [
            'class' => 'common\components\LocacaoUrl',
        ],
        'jwt' => [
            'class' => \sizeg\jwt\Jwt::class,
            'key'   => 'secret',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'stmp.devell.com.br',
                'username' => 'thainan.prado@devell.com.br',
                'password' => 'DevellTap!@#123',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        

    ],
    'modules' => [
        'gridview' => [
            'class' => 'kartik\grid\Module',
            // other module settings
        ]
    ]
];