<?php
return [
    'adminEmail' => 'site@miniportal.com.br',
    'supportEmail' => 'site@miniportal.com.br',
    'user.passwordResetTokenExpire' => 3600,
];
