<?php

namespace common\components;

use Yii;
use yii\base\Component;

use yii\helpers\Url;

class LocacaoUrl  extends Component {

    public static function inquilino($ultimaEtapa, $id)
	{
        $url = '';
        switch($ultimaEtapa){
            // case 1: $url = ['imoveis/etapa1', 'id' =>  $sessao->id]; break;
            case 2: $url = ['imoveis/etapa2', 'id' =>  $id]; break;
            case 3: $url = ['imoveis/etapa3', 'id' =>  $id]; break;
            case 4: $url = ['imoveis/aguarde', 'tipo' =>  1]; break;
            //Inq segunda etapa
            case 9: $url = ['gerenciar/etapa6', 'id' =>  $id]; break;
            case 10: $url = ['gerenciar/etapa7', 'id' =>  $id]; break;
            case 12: $url = ['imoveis/aguarde', 'tipo' => 3]; break;
        } 
        return $url;
    }

    public static function proprietario($ultimaEtapa, $id)
	{
        $url = '';

        switch($ultimaEtapa){
            case 4: $url = ['gerenciar/etapa1', 'id' =>  $id]; break;
            case 5: $url = ['gerenciar/etapa2', 'id' =>  $id]; break;
            case 6: $url = ['gerenciar/etapa3', 'id' =>  $id]; break;
            case 7: $url = ['gerenciar/etapa4', 'id' =>  $id]; break;
            case 8: $url = ['gerenciar/etapa5', 'id' =>  $id]; break;

        } 
        return $url;
    }
}