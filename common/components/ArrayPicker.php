<?php
namespace common\components;

use Exception;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
class ArrayPicker extends Component
{
	public function pick($data, $keys){
        if(gettype($data) != 'array' && gettype($data) != 'object')
            throw new Exception('You must provide an array or object');
        if(gettype($data) == 'object')
            $data = [$data];
		if (!is_array($keys)) $keys = [$keys];
		$output=array_map(function ($el) use ($keys) {
			$o = [];
			foreach($keys as $key){
				$o[$key] = isset($el[$key])?$el[$key]:false;
			}
			return $o;
        }, $data);
        
        // if was an unique object 
        if(count($output) == 1)
            return $output[0];
        return $output;
	}
}
?>