<?php

namespace common\components;

use Yii;
use yii\i18n\Formatter;

class MyFormatter extends Formatter {

	public static function asCep($string)
	{
		return substr($string, 0, 5) . '-' . substr($string, 5);
	}

	public static function asCpf($string)
	{
		return substr($string, 0, 3) . '.' . substr($string, 3, 3) . '.' . substr($string, 6, 3) . '-' . substr($string, 9);
	}

	public static function asCnpj($string)
	{
		return substr($string, 0, 2) . '.' . substr($string, 2, 3) . '.' . substr($string, 5, 3) . '/' . substr($string, 8, 4) . '-' . substr($string, 12);
	}

	public static function asInscricaoEstadual($string)
	{
		return substr($string, 0, 3) . '.' . substr($string, 3, 3) . '.' . substr($string, 6, 3) . '.' . substr($string, 9, 3);
	}

	public static function asMes($string)
	{
		return substr($string, 4, 2) . '/' . substr($string, 0, 4);
	}

	public static function asData($string)
	{
		return substr($string, 8, 2) . '/' . substr($string, 5, 2) . '/' . substr($string, 0, 4);
	}

	public static function asDataHora($string)
	{
		return substr($string, 8, 2) . '/' . substr($string, 5, 2) . '/' . substr($string, 0, 4) . ' ' . substr($string, 11);
	}

	public static function asDataSemSeparador($string)
	{
		return substr($string, 6, 2) . '/' . substr($string, 4, 2) . '/' . substr($string, 0, 4);
	}

	public static function asHora($string)
	{
		return substr($string, 0, 2) . ':' . substr($string, 3, 2) ;
	}

	public static function moneyMask(){
		return [
            'groupSeparator' => '.',
            'radixPoint' => ',',
            // 'currencyCode' => 'R$',
            // 'nullDisplay' => '',
        ];
	}
	
    public function asDecimalBRL($string) {
        if(strpos($string, ',')){
              $string = str_ireplace(".","",$string); 
              $string = str_ireplace(",",".",$string); 
        }
        return $string; 
    }

	public static function addMonths($months, $date){
        return date('Y-m-d',(strtotime ( '+'.$months.' months' , strtotime ($date) ) ) );
    }
   
    public function asBRL($string){
        return number_format(round($string,2), 2, ',', '.');
        
    }
}