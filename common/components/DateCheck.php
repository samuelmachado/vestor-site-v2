<?php

namespace common\components;

use Yii;

class DateCheck {

	public function pastDate($dateTimeBefore, $dateTimeAfter)
	{
		// $fuso = new DateTimeZone('America/Sao_Paulo');
		// $data->setTimezone($fuso);
		$before = new \DateTime($dateTimeBefore);
		$after = new \DateTime($dateTimeAfter);
		if ( $before > $after )
			return true;
		return false;
	}

	public function diaSemana($data, $format=true){
		$diasemana = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'];
		if($format)
			$diasemana = ['Domingo-feira', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado-feira'];
		


		$diasemana_numero = date('w', strtotime($data));

		return $diasemana[$diasemana_numero];
	}
}