<?php
namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Imovel;
use common\models\Visita;
use common\models\Anuncio;
use stdClass;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;


class AnunciosController extends BaseController
{
    public $modelClass = 'common\models\Anuncio';

    public function init()
    {
        parent::init();   
    }

	// public function behaviors()
	// {
	//     $behaviors = parent::behaviors();
	//     $behaviors['authenticator'] = [
	//     	'class' => QueryParamAuth::className(),
	//     ];
	//     return $behaviors;
	// }

    public function actions()
    {
        $actions = parent::actions();
        // unset($actions['view']);
        // unset($actions['index']);
        // unset($actions['create']);
        return $actions;
    }

    public function actionDiasDisponiveis($id){
        $anuncio = Anuncio::findOne($id);
        $hoje = date('Y-m-d');
        ;
        $tempoCont = 0;
        $output = [];
 
        // $date = \DateTime::createFromFormat( 'd/m/Y', date('Y-m-d H:i:s'));
        // $diaSemana = $date->format('w');
        $visitas = Visita::find()
        ->where(['>=','dataVisita',$hoje])
        ->andWhere(['<=','dataVisita', $this->addDays(7, date('Y-m-d'))])
        ->andWhere(['idAnuncio' => $id])
        ->all();

        
     
        $dia = 1;
        $dias = [];
        while($dia < 8) {
            $tempoCont = 450; //começando 7:30 e terminando 19:30
            $futuro = $this->addDays($dia, date('Y-m-d'));
            for($i = 0; $i < 25 ; $i++){
                $tempo = $this->convertToHoursMins($tempoCont);
                $dias[$futuro][$i] = $tempo;
                foreach($visitas as $visita) {
                    if($visita->dataVisita == $futuro && $visita->hora == $tempo)
                        $dias[$futuro][$i] = "";
                }
                $tempoCont += 30;
            }
            $dia++;
        }

        return $dias;
    }

    private function convertToHoursMins($time, $format = '%02d:%02d:00') {
        if ($time < 1) {
            return '00:00:00';
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

}

