<?php
namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Imovel;
use stdClass;
use yii\data\ActiveDataProvider;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\ActiveQueryInterface;
use yii\db\Connection;
use yii\db\QueryInterface;
use yii\di\Instance;

class ActiveDataProviderFormated extends \yii\data\ActiveDataProvider {
    public function init()
    {
        parent::init();
        if (is_string($this->db)) {
            $this->db = Instance::ensure($this->db, Connection::className());
        }
    }
    protected function prepareModels()
    {
        if (!$this->query instanceof QueryInterface) {
            throw new InvalidConfigException('The "query" property must be an instance of a class that implements the QueryInterface e.g. yii\db\Query or its subclasses.');
        }
        $query = clone $this->query;
        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();
            if ($pagination->totalCount === 0) {
                return [];
            }
            $query->limit($pagination->getLimit())->offset($pagination->getOffset());
        }
        if (($sort = $this->getSort()) !== false) {
            $query->addOrderBy($sort->getOrders());
        }

        return $this->successResponse($query->all($this->db));
    }

        // Encapsula uma resposta de sucesso
        public function successResponse($data, $message='Realizado com sucesso', $postRecieved=false){
            return $this->response($data, true, $message, null, $postRecieved);
        }
    
        // Encapsula uma resposta de falha
        public function failResponse($data, $error_code, $message='Não foi possível processar sua solicitação', $postRecieved=false){
            return $this->response($data, false, $message, $error_code, $postRecieved);
        }
        // Formata um payload padrão para TODA request
        public function response($data, $status=false, $message='', $error_code = null, $postRecieved=false) {
            $repsonse = [
                'status' => $status,
                'data' => $data,
                'message' => $message,
                'error_code' => $error_code,
            ];
    
            if($postRecieved)
                $repsonse['post'] = $postRecieved;
            return $repsonse;
        }
}