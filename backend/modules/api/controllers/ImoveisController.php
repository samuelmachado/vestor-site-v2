<?php
namespace backend\modules\api\controllers;

use common\models\Anuncio;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Imovel;
use common\models\Visita;
use stdClass;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;


date_default_timezone_set('America/Sao_Paulo');  
class ImoveisController extends BaseController
{
    public $modelClass = 'common\models\Imovel';

    public function init()
    {
   
        parent::init();
        
    }
   /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => \sizeg\jwt\JwtHttpBearerAuth::class,
            'optional' => [
                'index',
                'view',
                // 'meus-imoveis'
            ],
        ];
        return $behaviors;
    }


    public function actions()
    {
        $actions = parent::actions();
        unset($actions['view']);
        unset($actions['index']);
        // unset($actions['delete']);
        return $actions;
    } 


    public function actionMeusImoveis(){
        $id = \Yii::$app->User->identity->id;
        $imoveis = Imovel::find()->where(['idProprietarioUsuario' => $id])->all();
        // return $imoveis;
        $out=[];
        foreach($imoveis as $imovel) {
            if($imovel->anuncio){
                $out[] = $imovel;
            }
        }
        return $out;
        //return 'ok';
    }
    public function actionIndex(){
        $query = Imovel::find();
        // $_GET = json_decode(file_get_contents('php://input'), true);
        $query->andWhere('idSessao IS NULL');
        
        $this->equalsTo($query, Imovel::FIELDS_EQUALS_TO);
        $this->greaterOrEqualTo($query, Imovel::FIELDS_GREATHER_OR_EQUAL);
        $this->inRange($query, Imovel::IN_RANGE);
        if(isset($_GET['bairros']) && $_GET['bairros'] != ''){
            $bairros = json_decode($_GET['bairros'], true);
            $bairros = array_column($bairros, 'key');
            if($bairros)
                $query->andWhere(['in' ,'idBairro', $bairros]);
        }
        $query->joinWith('anuncio');
        $res = new ActiveDataProvider([
            'query' => $query,
        ]);
        // print_r($res);
        return $res;
    }
    private function diasDisponiveis($id){
        $anuncio = Anuncio::findOne($id);
        $hoje = date('Y-m-d');
        ;
        $tempoCont = 0;
        $output = [];
 
        // $date = \DateTime::createFromFormat( 'd/m/Y', date('Y-m-d H:i:s'));
        // $diaSemana = $date->format('w');
        $visitas = Visita::find()
        ->where(['>=','dataVisita',$hoje])
        ->andWhere(['<=','dataVisita', $this->addDays(7, date('Y-m-d'))])
        ->andWhere(['idAnuncio' => $id])
        ->all();

        
     
        $dia = 1;
        $dias = [];
        while($dia < 8) {
            $tempoCont = 450; //começando 7:30 e terminando 19:30
            $futuro = $this->addDays($dia, date('Y-m-d'));
            for($i = 0; $i < 25 ; $i++){
                $tempo = $this->convertToHoursMins($tempoCont);
                $dias[$futuro][$i] = $tempo;
                foreach($visitas as $visita) {
                    if($visita->dataVisita == $futuro && $visita->hora == $tempo)
                        $dias[$futuro][$i] = "";
                }
                $tempoCont += 30;
            }
            $dia++;
        }
        // return $dias;
        $arrLimpo = [];
        foreach($dias as $key=>$dia){
            $diaLimpo = [];
            foreach($dia as $hora){
                if($hora != "")
                    $diaLimpo[] = $hora;
            }
            $arrLimpo[$key] =  $diaLimpo;
        }
        return $arrLimpo;
    }
    public function actionDiasDisponiveis($idAnuncio){
        return $this->diasDisponiveis($idAnuncio);
    }

    private function convertToHoursMins($time, $format = '%02d:%02d:00') {
        if ($time < 1) {
            return '00:00:00';
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }
    public function actionView($id){
        $imovel = Imovel::findOne($id);
        return [
            'imovel' => $imovel,
            'diasDisponiveis' => $this->diasDisponiveis($imovel->anuncio->id),
        ];
    }

    public function actionVisitaRepetida($idAnuncio){
        $id = \Yii::$app->User->identity->id;
        $visita = Visita::find()
        ->andWhere(['idUsuarioAgendado' => $id])
        ->andWhere(['idAnuncio' => $idAnuncio])
        ->one();

        if($visita){
            return ['status' => true];
        }
        return ['status' => false];
        
    }
   

}