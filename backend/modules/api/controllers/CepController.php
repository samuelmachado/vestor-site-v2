<?php

namespace backend\modules\api\controllers;

use common\models\Bairro;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Imovel;
use common\models\Logradouro;
use stdClass;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
class CepController extends BaseController
{
    public $modelClass = 'common\models\Cep';

    public function init()
    {
   
        parent::init();
        
    }

	// public function behaviors()
	// {
	//     $behaviors = parent::behaviors();
	//     $behaviors['authenticator'] = [
	//     	'class' => QueryParamAuth::className(),
	//     ];
	//     return $behaviors;
    // }
    public function actions()
    {
        $actions = parent::actions();
        // unset($actions['view']);
        // unset($actions['index']);
        // unset($actions['delete']);
        return $actions;
    }

    public function actionSearch($texto){
        $listaBairros = [];
        $listaLogradouro = [];

        $texto = trim($texto);
        $listaBairros = Bairro::find()->where(['like', 'UPPER(nome)', mb_strtoupper($texto)])->limit(5)->all();
        $listaLogradouro = Logradouro::find()->where(['like', 'UPPER(nome)', mb_strtoupper($texto)])->limit(5)->all();
        
 
        return [
            'bairros' => $listaBairros,
            'logradouros' => $listaLogradouro

        ];
    }
}