<?php
namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;

use stdClass;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;


class NOMEController extends BaseController
{
    public $modelClass = 'common\models\NOME';

    public function init()
    {
   
        parent::init();
        
    }

	public function behaviors()
	{
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	    	'class' => QueryParamAuth::className(),
	    ];
	    return $behaviors;
	}

    public function actions()
    {
        $actions = parent::actions();
        // unset($actions['view']);
        // unset($actions['index']);
        // unset($actions['delete']);
        return $actions;
    }

}