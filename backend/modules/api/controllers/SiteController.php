<?php

namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Condutor;
use common\models\Historico;

class SiteController extends ActiveController
{
    public $modelClass = 'common\models\Usuario';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

    public function actionAutenticar(){
        return 'ok';
    }

}   