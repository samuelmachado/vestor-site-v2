<?php
namespace backend\modules\api\controllers;

use common\models\Pessoa;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Usuario;
use stdClass;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use sizeg\jwt\JwtHttpBearerAuth;


class UsuarioController extends BaseController
{
    public $modelClass = 'common\models\Usuario';

    public function init()
    {
   
        parent::init();
        
    }
  /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'token',
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        unset($actions['create']);
        unset($actions['update']);
        return $actions;
    }
    public function actionView(){
        $usuario = Usuario::findOne(\Yii::$app->User->identity->id);
        if(!$usuario) {
            return [
                'status' => false,
                'message' => 'Usuário não encontrado'
            ];
        }
        return [
            'status' => true,
            'message' => 'Encontrado',
            'usuario' => $usuario
        ];
    }
    public function actionUpdate(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        // return $_POST['data']['nome'];
        $usuario = Usuario::findOne(\Yii::$app->User->identity->id);
        if(!$usuario) {
            return [
                'status' => false,
                'message' => 'Usuário não encontrado'
            ];
        }
        if(isset($_POST) && isset($_POST['data']['nome']) && isset($_POST['data']['email']) ){
            $nome = isset($_POST['data']['nome']) ? $_POST['data']['nome'] : '';
            $email = isset($_POST['data']['email']) ? $_POST['data']['email'] : '';
            $cpf = isset($_POST['data']['cpf']) ? $_POST['data']['cpf'] : '';
         
            if($cpf){
                $cpf = str_replace(".", "", $cpf);
                $cpf = str_replace("-", "", $cpf);
            }
            $pessoa = $usuario->pessoa;
            if($nome){
                $pessoa->nome = $nome;
            }
            if($email){
                $usuario->email = $email;
            }

            if($cpf){
                $testeCpf = Pessoa::findOne(['CPF' => $cpf]);
                if($testeCpf){
                    $s= $testeCpf->usuario->email;
                    $email = explode('@',$s)[1];
                    return [
                        'status' => false,
                        'message' => 'CPF já cadastrado',
                        'detalhes' => substr($s,0,3).'******@'.$email,
                        'error' => 1001
                    ];
                }
                $pessoa->CPF = $cpf;
            }
            if($nome || $cpf) {
                $pessoa->save(false);
            }
            $usuario->cadastroFinalizado = 2;
            if(!$usuario->save()){
                return [
                    'status' => false,
                    'message' => 'Não foi possível atualizar os dados',
                    'detalhes' => $usuario->getErrors()
                ];
            }
            return [
                'status' => true,
                'message' => 'Usuário atualizado',
                'usuario' => $usuario
            ];
        }

        return [
            'status' => false,
            'message' => 'Envie os params'
        ];
    }
}