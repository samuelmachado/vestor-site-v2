<?php
namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
define('API_KEY','1079595086:AAEOPVUtgy4tZ3Q582HbihAzGmy4J8NtkZI');
define('ADMIN_ID', '-336417100');
class BaseController extends ActiveController
{
    public function init()
    {
        date_default_timezone_set('America/Sao_Paulo');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        parent::init();
        
    }
    public function removeDays($days, $date){
        return date('Y-m-d',(strtotime ( '-'.$days.' day' , strtotime ($date) ) ) );
    }

        
    public function addDays($days, $date){
        return date('Y-m-d',(strtotime ( '+'.$days.' day' , strtotime ($date) ) ) );
    }

    public function addMinutes($minutes, $dateTime){
        return date('Y-m-d H:i:s',(strtotime ( '+'.$minutes.' minutes' , strtotime ($dateTime) ) ) );
    }
    public function sendWhatsapp($data){
        $url = "https://eu98.chat-api.com/instance113817/sendMessage?token=q1qpd8hdxafaem1b";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_ENCODING,'UTF-8');
        curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($data));
        $res = curl_exec($ch);
        print_r($res);
        if(curl_error($ch)){
            var_dump(curl_error($ch));
        }else{
            return json_decode($res);
        }
    }
    private function makereq($method,$datas=[]){
        $url = "https://api.telegram.org/bot".API_KEY."/".$method;
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($datas));
        $res = curl_exec($ch);
        // print_r($res);
        if(curl_error($ch)){
            var_dump(curl_error($ch));
        }else{
            return json_decode($res);
        }
    }
    public function sendAdminMessage($TextMsg)
    {

     $this->makereq('sendMessage',[
    'chat_id'=>ADMIN_ID,
    'text'=>$TextMsg,
    'parse_mode'=>"MarkDown"
    ]);
    }
    private function sendMessage($chat_id, $TextMsg)
    {
    print $TextMsg;
     $this->makereq('sendMessage',[
    'chat_id'=>$chat_id,
    'text'=>$TextMsg,
    'parse_mode'=>"MarkDown"
    ]);
    }
    private  function SendContact($chat_id, $contactMsg)
    {
     makereq('sendContact',[
    'chat_id'=>$chat_id,
    'phone_number'=>$contact,
    'first_name'=>$name
    ]);
    }
    private  function SendSticker($chat_id, $sticker_ID)
    {
     makereq('sendSticker',[
    'chat_id'=>$chat_id,
    'sticker'=>$sticker_ID
    ]);
    }
    private function typing($chat_id)
    {
        $url = "https://api.telegram.org/bot".API_KEY."/sendChatAction?chat_id=".$chat_id."&action=typing";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $res = curl_exec($ch);
        print_r($res);
        if(curl_error($ch)){
            var_dump(curl_error($ch));
        }else{
            return json_decode($res);
        }
    }
    private function sendKeyboard($chatId){
             $this->makereq('sendMessage',[
            'chat_id'=>$chatId,
            'text'=>"Selecione os botõeszinhos no seu teclado",
            'parse_mode'=>'MarkDown',
            'reply_markup'=>json_encode([
                'keyboard'=>[
                  [
                   ['text'=>"Me ajuda!"],
                   ['text'=>"Trocar de projeto"],
                   // ['text'=>"Address",'request_location'=>true], 
                   // ['text'=>"Phone Number",'request_contact'=>true]
                  ],
                ],
                'resize_keyboard'=>true
            ])
        ]);
    }
    private function inlineKeyboard($chatId, $keyboard, $txt, $resize=true){
        $keyboard = json_encode([
                'inline_keyboard'=>[
                  //[
                    $keyboard

                   // ['text'=>"Address",'request_location'=>true], 
                   // ['text'=>"Phone Number",'request_contact'=>true]
                  //],
                ],
             
            ]);
        $this->makereq('sendMessage',[
            'chat_id'=>$chatId,
            'text'=> $txt,
            'parse_mode'=>'MarkDown',
            'reply_markup' => $keyboard,
               'one_time_keyboard' => true,   
                'resize_keyboard'=> false,
        ]);
    }
  
  
    public function equalsTo(&$query, $fields) {
        foreach($fields as $field) 
            if(\Yii::$app->request->get($field) && \Yii::$app->request->get($field) != '') {
                $query->andWhere([$field => \Yii::$app->request->get($field)]);
            }
    }

    public function greaterOrEqualTo(&$query, $fields) {
        foreach($fields as $field) 
            if(\Yii::$app->request->get($field) && \Yii::$app->request->get($field) != '') {
                $query->andWhere(['>=', $field ,\Yii::$app->request->get($field)]);
            }
    }
    public function inRange(&$query, $fields) {

        foreach($fields as $field){
            $condition = ''; 
            if(\Yii::$app->request->get('min_'.$field) && \Yii::$app->request->get('min_'.$field) != '') 
            {
                $condition = ['>=', $field, \Yii::$app->request->get('min_'.$field)]; 
            }
            if(\Yii::$app->request->get('max_'.$field) && \Yii::$app->request->get('max_'.$field) != '') 
            {
                $condition = ['<=', $field, \Yii::$app->request->get('max_'.$field)]; 
            }
            if(
                \Yii::$app->request->get('min_'.$field) && 
                \Yii::$app->request->get('max_'.$field) && 
                \Yii::$app->request->get('min_'.$field) != '' && 
                \Yii::$app->request->get('max_'.$field) != ''
                ) 
                {
                    $condition = ['between', $field ,\Yii::$app->request->get('min_'.$field), \Yii::$app->request->get('max_'.$field)];
                }
            if($condition)
                $query->andWhere($condition);
        }
    }

    // Encapsula o tratamento e resposta de objetos
    public function sendBomb($data, $message='', $postRecieved=false){
        $output = $this->format($data);
        return $this->successResponse($output);
    }

    // Trata formatação de objetos seja um array de objeto OU um objeto
    public function format($data, $formatMethod='formatObject'){
        $output = [];
        if($data) {
            if(is_array($data)) {
                foreach($data as $row) {
                    $output[] = $this->$formatMethod($row);
                }
            } else {
                $output = $this->$formatMethod($data);
            }
        }
        
        return $output;
    }

    // Formata um OBJETO específico, esse método deve ser sobrescrito em cada controller
    // objetivo dele é dizer quais fields vão ou não no payload
    public function formatObject($object) {
        return $object;
    }

    // Encapsula uma resposta de sucesso
    public function successResponse($data, $message='Realizado com sucesso', $postRecieved=false){
        return $this->response($data, true, $message, null, $postRecieved);
    }

    // Encapsula uma resposta de falha
    public function failResponse($data, $error_code, $message='Não foi possível processar sua solicitação', $postRecieved=false){
        return $this->response($data, false, $message, $error_code, $postRecieved);
    }
    // Formata um payload padrão para TODA request
    public function response($data, $status=false, $message='', $error_code = null, $postRecieved=false) {
        $repsonse = [
            'status' => $status,
            'data' => $data,
            'message' => $message,
            'error_code' => $error_code,
        ];

        if($postRecieved)
            $repsonse['post'] = $postRecieved;
        return $repsonse;
    }
}