<?php
namespace backend\modules\api\controllers;

use common\models\Pessoa;
use common\models\Respostas;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;

use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;


class WebhookController extends BaseController
{
    public $modelClass = 'common\models\Usuario';

    public function init()
    {
   
        parent::init();
        
    }


    public function actions()
    {
        $actions = parent::actions();
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        unset($actions['update']);
        unset($actions['create']);
        return $actions;
    }

    public function actionRead(){
        $json = file_get_contents('php://input');
        $data = json_decode($json, true);   

        // $json = file_get_contents('php://input');
      
        
        if(!isset($data['messages']))
            return ['status' => false];
        // $ifp = fopen('../../log.txt', 'wb');
        // fwrite($ifp, $json);
        // fclose($ifp); 
        foreach($data['messages'] as $message){ 
            //  if(!$message['fromMe']){            
               $sender = explode("@", $message['chatId']);
               $sender = substr($sender[0], 2);
               $pessoa = Pessoa::findOne(['telefone' => $sender]); //$this->db->where('con_number', $sender)->get('a_contacts')->row();
               $r = new Respostas();
               $r->idPessoa = $pessoa ? $pessoa->id : null;
               $r->nome =  $message['senderName'];
               $r->origem =  $message['fromMe'] == true ? 1 : 0;
            //    $r->tipo =  $message['type'];
               $r->text = $message['body'];
               $r->numero = $message['messageNumber'];
               $r->registrado = date('Y-m-d H:i:s'); 
               $r->save();
            //    if(!$r->save())
            //     return ['status' => false, 'data' => $r->getErrors()];
               
            //  }
        }
        return ['status' => true, 'data' => $data];
    }
}