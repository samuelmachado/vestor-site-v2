<?php
namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;

use common\models\Pin;
use common\models\Usuario;
use common\models\Pessoa;
use common\models\UsuarioPerfil;
use sizeg\jwt\JwtHttpBearerAuth;

use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use Yii;

class PinsController extends BaseController
{
    public $modelClass = 'common\models\Pin';

    public function init()
    {
   
        parent::init();
        
    }

    /**
     * @inheritdoc
     */
    // public function behaviors()
    // {
    //     $behaviors = parent::behaviors();
    //     $behaviors['authenticator'] = [
    //         'class' => JwtHttpBearerAuth::class,
    //         'optional' => [
    //             'token',
    //         ],
    //     ];
    //     return $behaviors;
    // }

    public function actionTesteAdmin(){
        Pin::enviar('12982948776', '6666-TESTE');
    }
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['view']);
        // unset($actions['index']);
        unset($actions['delete']);
        unset($actions['create']);
        return $actions;
    }

    public function actionGerar(){
        $pinLog = '';
        $model = new Pin();
        $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->validate()) {
            $max = 9999;
            $min = 1000;
            $model->pin = rand($min, $max);
            
            $pinAtual = Pin::searchPin($model->telefone);
           
            if(!$pinAtual or $this->addMinutes(10, $pinAtual->criado) <= date('Y-m-d H:i:s'))
            {
                if($pinAtual) $pinAtual->delete();
                $pinLog = 'Um novo PIN foi enviado';   
                if(!$model->save()) return $model->getErrors();
                $response = Pin::gerar($model);
            } 
            else 
            {
                $pinLog = 'Reenviamos o PIN';   
                $pinAtual->isTeste = $model->isTeste;
                $response = Pin::gerar($pinAtual);
            }
            
          return [
              'isTeste' => $model->isTeste,
              'status' => $response ? 'PIN enviado com sucesso' : 'Não foi possível enviar o PIN',
              'data' => $response,
              'log' => $pinLog
          ];
           
        }
        return $model->getErrors();
    }

    public function actionAutenticar(){
        $_POST =\Yii::$app->getRequest()->getBodyParams();
        $atualizarUsuario = null;
        $novoUsuario = false;
        $model = new Pin();
        $model->scenario = 'autenticar';

        // $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
        $model->pin = $_POST['pin'];
        $model->telefone = $_POST['telefone'];
        if(isset($_POST['usuario']))
            $atualizarUsuario = $_POST['usuario'];

        if ($model->pin && $model->telefone) {
            $meuPin = Pin::searchPin($model->telefone,$model->pin);
            if(!$meuPin){
                return ['status' => false, 'message' => "Pin Inválido"];
            }
            if($atualizarUsuario){
                $usuario = Usuario::findOne($atualizarUsuario['id']);
                $pessoa = $usuario->pessoa;
                $pessoa->telefone = $model->telefone;
                $pessoa->save();
            } else {
                
                $usuario = Usuario::findByTelefone($model->telefone);
            }
            if(!$usuario) {
              
                $pessoa = new Pessoa();
                $pessoa->telefone = $model->telefone;
                if(!$pessoa->save()) return $pessoa->getErrors();

                $usuario = new Usuario();
                
                $usuario->idPessoa = $pessoa->id;
                $usuario->generateAuthKey();
                if(!$usuario->save()) return $usuario->getErrors();
                
                UsuarioPerfil::criarCliente($usuario);
                
                $usuario = Usuario::findOne($usuario->id);
                // $usuario->cadastroFinalizado = 1;
            }
            
            $usuario->authKey = Usuario::generateToken($usuario->id);
            $usuario->save();
            // $meuPin->delete();
            return $usuario;
        }       
        return ['status' => false, 'message' => "Envie nome e telefone"];
         
    }

    
}