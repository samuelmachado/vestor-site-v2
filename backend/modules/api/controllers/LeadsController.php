<?php
namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;

use stdClass;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;
use common\models\Lead;

class LeadsController extends BaseController
{
    public $modelClass = 'common\models\Lead';

    public function init()
    {
   
        parent::init();
        
    }

	// public function behaviors()
	// {
	//     $behaviors = parent::behaviors();
	//     $behaviors['authenticator'] = [
	//     	'class' => QueryParamAuth::className(),
	//     ];
	//     return $behaviors;
	// }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['index']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['delete']);
        return $actions;
    }

    public function actionCreate(){
      /* @var $model \yii\db\ActiveRecord */
    $model = new Lead();

    $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
    $model->idCidade = 1;
    if ($model->save(false)) {
        $response = \Yii::$app->getResponse();
        $response->setStatusCode(201);
        $id = implode(',', array_values($model->getPrimaryKey(true)));
        $this->sendAdminMessage('
        ===⭐ '.'NOVO LEAD'.' ⭐===
        ⏰ '.date('d/m/Y H:i:s').' 
        😃 '.$model->nome.'
        💻 '.$model->ddd.$model->telefone.'
        💬 '.$model->email.'
        💬 '.$_POST['cidade'].'
        ================
                ');
        // $response->getHeaders()->set('Location', '');

        header("Location: https://vestor.com.br/?enviado=1");
        die();
    } elseif (!$model->hasErrors()) {
        throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
    }

    return $model;
  
    }
}