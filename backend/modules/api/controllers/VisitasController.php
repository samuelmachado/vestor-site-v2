<?php
namespace backend\modules\api\controllers;

use common\models\Anuncio;
use common\models\Visita;
use Exception;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;

use stdClass;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use sizeg\jwt\JwtHttpBearerAuth;

class VisitasController extends BaseController
{
    public $modelClass = 'common\models\Visita';

    public function init()
    {
        parent::init();   
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'token',
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        // unset($actions['view']);
        // unset($actions['index']);
        unset($actions['create']);
        return $actions;
    }
    
    public function actionMinhasVisitas(){
        // \Yii::$app->cache->flush();
        $id = \Yii::$app->User->identity->id;
        //->cache(60)
        $visitas = Visita::find()->andWhere(['idUsuarioAgendado' => $id])->all();
        $out=[];
        foreach($visitas as $visita) {
            $imovel = \Yii::$app->arrayPicker->pick($visita->anuncio->imovel, ['id','thumbnails','enderecoCompleto']);
            $imovel['visita'] = \Yii::$app->arrayPicker->pick($visita, ['id','dataVisita', 'hora']);
            $imovel['anuncio'] = \Yii::$app->arrayPicker->pick($visita->anuncio, ['id']);
            $out[] = $imovel;
        }
        return $out;
    }

    public function actionCreate(){
        $_POST = json_decode(file_get_contents('php://input'), true);

        if(isset($_POST)){
            // return $_POST;
            if(isset($_POST['data']['editar'])){
                $visita = Visita::findOne($_POST['data']['editar']);
                $visita->delete();
            }
            $data = explode('T',$_POST['data']['dia']);
            $data = $data[0];
            $hora = $_POST['data']['hora'];

            $visitaAgendada = Visita::find()->andWhere(['dataVisita' => $data])->andWhere(['hora' => $hora])->all();
            if($visitaAgendada){
                return [
                    'status' => false,
                    'error_code' => 21,
                    'message' => 'Este horário acabou de ser agendado'
                ];
            }


            $visita = new Visita();
            $visita->dataVisita = $data;
            $visita->hora = $_POST['data']['hora'];
            $visita->idUsuarioAgendado = \Yii::$app->User->identity->id;
            $visita->idAnuncio = $_POST['data']['imovel']['imovel']['anuncio']['id'];
            $visita->save();
            if(!$visita->save()){
                return [
                    'status' => false,
                    'error_code' => 22,
                    'message' => 'Erro ao salvar visita',
                    'detalhes' => $visita->getErrors()
                ];
            }
            $pessoa = \Yii::$app->User->identity->pessoa;
            $data = date("d/Y", strtotime($visita->dataVisita));
            $hrMin = explode(':',$visita->hora);
            $hrMin = $hrMin[0].':'.$hrMin[1];
            setlocale(LC_TIME, 'pt');
            $res = strftime("%e, %A", strtotime($visita->dataVisita));
            // $diasdasemana = array (1 => "Segunda-Feira",2 => "Terça-Feira",3 => "Quarta-Feira",4 => "Quinta-Feira",5 => "Sexta-Feira",6 => "Sábado",0 => "Domingo");

//             $this->sendWhatsapp([
//                 'chatId' => '55'.$pessoa->telefone.'@c.us',
//                 'body' => '🥳 Woow! '.$pessoa->nome.' sua visita foi agendada.

// É dia '.$res.' às *'.$hrMin.'*
// '.$visita->anuncio->imovel->enderecoSimples.', Nº '.$visita->anuncio->imovel->numeroLogradouro.'
// Link Maps: https://goo.gl/maps/FNGfViSAaZiL9Z7S9
// *Salva o meu contato*'
//             ]);
            return [
                'status' => true,
                'error_code' => null,
                'message' => 'Visita agendada com sucesso',
                'visita' => $visita
            ];
        }
        return [
            'status' => false,
            'error_code' => 20,
            'message' => 'Erro ao processar dados'
        ];
    }

    public function actionRegendar(){
        \Yii::$app->cache->flush();
        $_POST =\Yii::$app->getRequest()->getBodyParams();
        $id = \Yii::$app->User->identity->id;
        $data = explode('T',$_POST['data']['dia']);
        $data = $data[0];
        $hora = $_POST['data']['hora'];
        
        $visita = new Visita();
        $visita->dataVisita = $data;
        $visita->hora = $hora;
        $visita->idUsuarioAgendado = $id;
        $visita->idAnuncio = $_POST['data']['visita']['anuncio']['id'];
        try {
        if($visita->save()){
            $visitaAntiga = Visita::findOne($_POST['data']['visita']['visita']['id']);
            if($visitaAntiga)
                $visitaAntiga->delete();
        } else {
            return ['status' => false];
        }
        } catch(Exception $e){
            return ['status' => false, 'e' => $e];
        }
        return ['status' => true];
    }
    public function actionCancelar($id){
        \Yii::$app->cache->flush();
        $visita = Visita::findOne($id);
        if(!$visita)
            return ['status' => true];
        if($visita->delete()){
            return ['status' => true];
        }
        return ['status' => false];
    }
}