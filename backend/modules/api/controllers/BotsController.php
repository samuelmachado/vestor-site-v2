<?php

namespace backend\modules\api\controllers;
use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use common\models\Bot;
use common\models\Usuario;
use common\models\Projeto;
use common\models\Opcao;
use common\models\Chamado;
define('API_KEY','1079595086:AAEOPVUtgy4tZ3Q582HbihAzGmy4J8NtkZI');
define('ADMIN_ID', '-336417100');
class BotsController extends ActiveController
{
	public $modelClass = 'common\models\Usuario';
    public function init() {
        date_default_timezone_set('America/Sao_Paulo');
        parent::init();
    }
	// public function behaviors()
    // {
    //     $behaviors = parent::behaviors();
    //     $behaviors['authenticator'] = [
    //         'class' => QueryParamAuth::className(),
    //     ];
    //     return $behaviors;
    // }
    private function makereq($method,$datas=[]){
        $url = "https://api.telegram.org/bot".API_KEY."/".$method;
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($datas));
        $res = curl_exec($ch);
        print_r($res);
        if(curl_error($ch)){
            var_dump(curl_error($ch));
        }else{
            return json_decode($res);
        }
    }
    private function sendMessage($chat_id, $TextMsg)
    {
    print $TextMsg;
     $this->makereq('sendMessage',[
    'chat_id'=>$chat_id,
    'text'=>$TextMsg,
    'parse_mode'=>"MarkDown"
    ]);
    }
    private  function SendContact($chat_id, $contactMsg)
    {
     makereq('sendContact',[
    'chat_id'=>$chat_id,
    'phone_number'=>$contact,
    'first_name'=>$name
    ]);
    }
    private  function SendSticker($chat_id, $sticker_ID)
    {
     makereq('sendSticker',[
    'chat_id'=>$chat_id,
    'sticker'=>$sticker_ID
    ]);
    }
    private function typing($chat_id)
    {
        $url = "https://api.telegram.org/bot".API_KEY."/sendChatAction?chat_id=".$chat_id."&action=typing";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $res = curl_exec($ch);
        print_r($res);
        if(curl_error($ch)){
            var_dump(curl_error($ch));
        }else{
            return json_decode($res);
        }
    }
    private function sendKeyboard($chatId){
             $this->makereq('sendMessage',[
            'chat_id'=>$chatId,
            'text'=>"Selecione os botõeszinhos no seu teclado",
            'parse_mode'=>'MarkDown',
            'reply_markup'=>json_encode([
                'keyboard'=>[
                  [
                   ['text'=>"Me ajuda!"],
                   ['text'=>"Trocar de projeto"],
                   // ['text'=>"Address",'request_location'=>true], 
                   // ['text'=>"Phone Number",'request_contact'=>true]
                  ],
                ],
                'resize_keyboard'=>true
            ])
        ]);
    }
    private function inlineKeyboard($chatId, $keyboard, $txt, $resize=true){
        $keyboard = json_encode([
                'inline_keyboard'=>[
                  //[
                    $keyboard

                   // ['text'=>"Address",'request_location'=>true], 
                   // ['text'=>"Phone Number",'request_contact'=>true]
                  //],
                ],
             
            ]);
        $this->makereq('sendMessage',[
            'chat_id'=>$chatId,
            'text'=> $txt,
            'parse_mode'=>'MarkDown',
            'reply_markup' => $keyboard,
               'one_time_keyboard' => true,   
                'resize_keyboard'=> false,
        ]);
    }
  
   
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);
        unset($actions['delete']);
        unset($actions['create']);
        unset($actions['update']);

        return $actions;
    }
    public function actionNovoLead(){
        $this->sendMessage(ADMIN_ID, '☺ Você atualmente está trabalhando no **');
    }
    public function actionNovaBuildTeste(){
        $this->sendMessage(ADMIN_ID, '•• Nova versão de build de teste •• IP: '.$_SERVER['REMOTE_ADDR']);
    }
    public function actionProcessar()
    {
        $data = file_get_contents('php://input');
        $fp = fopen($_SERVER['DOCUMENT_ROOT'].'/data.txt', 'w');
        fwrite($fp, $data);
        fclose($fp);    
        
        $update = json_decode($data);

        // Resposta via button
        if(isset($update->callback_query)){

            $callbackData = $update->callback_query->data;
            $msgOriginal = $update->callback_query->message->text;
            $chatId = $update->callback_query->message->chat->id;   

        
        } else {
            $chatId = $update->message->chat->id;
            
            $this->typing($chatId);
            $this->sendMessage($chatId, '😰 ixi...');
        
                                                   

        }       
        print 1;
    } 



}
