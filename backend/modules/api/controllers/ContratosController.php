<?php
namespace backend\modules\api\controllers; 
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;

use stdClass;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use DocuSign\eSign\Model\Document;
use DocuSign\eSign\Model\CertifiedDelivery;
use common\models\Contrato;
use sizeg\jwt\JwtHttpBearerAuth;

class ContratosController extends BaseController
{
    public $modelClass = 'common\models\Contrato';

    public function init()
    {
   
        parent::init();
        
    }
     /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'token',
            ],
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        // unset($actions['view']);
        // unset($actions['index']);
        // unset($actions['delete']);
        return $actions;
    }
    public function list_envelopes(){
        $accessToken = '
        eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjY4MTg1ZmYxLTRlNTEtNGNlOS1hZjFjLTY4OTgxMjIwMzMxNyJ9.eyJUb2tlblR5cGUiOjUsIklzc3VlSW5zdGFudCI6MTU4MjU4NDg4MywiZXhwIjoxNTgyNjEzNjgzLCJVc2VySWQiOiIzYzg2YTlmYi1lMDQzLTQwMDctYjc0Yy00MDI1YmY0YjQ5NDkiLCJzaXRlaWQiOjEsInNjcCI6WyJzaWduYXR1cmUiLCJjbGljay5tYW5hZ2UiLCJvcmdhbml6YXRpb25fcmVhZCIsInJvb21fZm9ybXMiLCJncm91cF9yZWFkIiwicGVybWlzc2lvbl9yZWFkIiwidXNlcl9yZWFkIiwidXNlcl93cml0ZSIsImFjY291bnRfcmVhZCIsImRvbWFpbl9yZWFkIiwiaWRlbnRpdHlfcHJvdmlkZXJfcmVhZCIsImR0ci5yb29tcy5yZWFkIiwiZHRyLnJvb21zLndyaXRlIiwiZHRyLmRvY3VtZW50cy5yZWFkIiwiZHRyLmRvY3VtZW50cy53cml0ZSIsImR0ci5wcm9maWxlLnJlYWQiLCJkdHIucHJvZmlsZS53cml0ZSIsImR0ci5jb21wYW55LnJlYWQiLCJkdHIuY29tcGFueS53cml0ZSJdLCJhdWQiOiJmMGYyN2YwZS04NTdkLTRhNzEtYTRkYS0zMmNlY2FlM2E5NzgiLCJhenAiOiJmMGYyN2YwZS04NTdkLTRhNzEtYTRkYS0zMmNlY2FlM2E5NzgiLCJpc3MiOiJodHRwczovL2FjY291bnQtZC5kb2N1c2lnbi5jb20vIiwic3ViIjoiM2M4NmE5ZmItZTA0My00MDA3LWI3NGMtNDAyNWJmNGI0OTQ5IiwiYW1yIjpbImludGVyYWN0aXZlIl0sImF1dGhfdGltZSI6MTU4MjU4NDg3OSwicHdpZCI6ImUyMWI4ZGZkLTE5Y2EtNDgwZC05NDQ3LTdmNzdlZjM0ZGUxMyJ9.uAfbtnUN2c1pBKLlDmEcIpa7NXa6FwpDzjQ6djPrsIEBcwFQ3EoxufGqzGwIP_F8V79uYavo-gb9VHzqAsP_TB7KuiGh-PpYTmNC42fEtnJjjF7Tzl7IR7D5ALCKD3E9XMNSp6OXw1FmWpvWSwi_AHoMhg7t_o0MmhVn1BQ3SDMAvlhHK6JS6nH7oZtd75_BaJQ83VXmt3mirhKeyflIqXF6RoiDs-STVjWa-AJvrjreGv1svO_xvyVEp09T1cZc69u_41-dV5HpAtsyuxWZP8-EsBGY3_ieeOdhOIyzb7AHNFtr7M1j8SSQsuTQ9c1Ej4GwHjmZ3tgzK8RQlN_C4A
        ';
        # Obtain your accountId from demo.docusign.com -- the account id is shown in the drop down on the
        # upper right corner of the screen by your picture or the default picture. 
        $accountId = 'c689e0ae-56ea-4909-b3e4-eb6ed1389cb6';
    
        # The API base_path
        $basePath = 'https://demo.docusign.net/restapi';
    
        # configure the EnvelopesApi object
        $config = new \DocuSign\eSign\Configuration();
        $config->setHost($basePath);
        $config->addDefaultHeader("Authorization", "Bearer " . $accessToken);
        $apiClient = new \DocuSign\eSign\Client\ApiClient($config);
        $envelopeApi = new \DocuSign\eSign\Api\EnvelopesApi($apiClient);
    
        #
        # Step 1. Create the options object. We want the envelopes created 10 days ago or more recently.
        #
        $date = new \Datetime();
        $date->sub(new \DateInterval("P10D"));
        $options = new \DocuSign\eSign\Api\EnvelopesApi\ListStatusChangesOptions();
        // $options->setFromDate($date->format("Y/m/d"));
        $options->setEnvelopeIds(['07fc2a4e-e98e-4a55-980d-deda3a1cc7c3']);
        #
        #  Step 2. Request the envelope list.
        #
        $results = $envelopeApi->listStatusChanges($accountId, $options);
        return $results;
    }

    public function actionInfo(){
        try {
            $results = $this->list_envelopes();
            ?>
        <html lang="en">
            <body>
                <h4>Results</h4>
                <p><code><pre><?= print_r ($results) ?></pre></code></p>
            </body>
        </html>
            <?php
        } catch (\Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            if ($e instanceof \DocuSign\eSign\Client\ApiException) {
                print ("\nDocuSign API error information: \n");
                var_dump ($e->getResponseBody());
            }
        }
    }
    public function sign()
        {
        #
        # The document $fileNamePath will be sent to be signed by <signer_name>
    
        # Settings
        # Fill in these constants
        #
        # Obtain an OAuth access token from https://developers.docusign.com/oauth-token-generator
        $accessToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjY4MTg1ZmYxLTRlNTEtNGNlOS1hZjFjLTY4OTgxMjIwMzMxNyJ9.eyJUb2tlblR5cGUiOjUsIklzc3VlSW5zdGFudCI6MTU4MzAxMDQ5MSwiZXhwIjoxNTgzMDM5MjkxLCJVc2VySWQiOiIzYzg2YTlmYi1lMDQzLTQwMDctYjc0Yy00MDI1YmY0YjQ5NDkiLCJzaXRlaWQiOjEsInNjcCI6WyJzaWduYXR1cmUiLCJjbGljay5tYW5hZ2UiLCJvcmdhbml6YXRpb25fcmVhZCIsInJvb21fZm9ybXMiLCJncm91cF9yZWFkIiwicGVybWlzc2lvbl9yZWFkIiwidXNlcl9yZWFkIiwidXNlcl93cml0ZSIsImFjY291bnRfcmVhZCIsImRvbWFpbl9yZWFkIiwiaWRlbnRpdHlfcHJvdmlkZXJfcmVhZCIsImR0ci5yb29tcy5yZWFkIiwiZHRyLnJvb21zLndyaXRlIiwiZHRyLmRvY3VtZW50cy5yZWFkIiwiZHRyLmRvY3VtZW50cy53cml0ZSIsImR0ci5wcm9maWxlLnJlYWQiLCJkdHIucHJvZmlsZS53cml0ZSIsImR0ci5jb21wYW55LnJlYWQiLCJkdHIuY29tcGFueS53cml0ZSJdLCJhdWQiOiJmMGYyN2YwZS04NTdkLTRhNzEtYTRkYS0zMmNlY2FlM2E5NzgiLCJhenAiOiJmMGYyN2YwZS04NTdkLTRhNzEtYTRkYS0zMmNlY2FlM2E5NzgiLCJpc3MiOiJodHRwczovL2FjY291bnQtZC5kb2N1c2lnbi5jb20vIiwic3ViIjoiM2M4NmE5ZmItZTA0My00MDA3LWI3NGMtNDAyNWJmNGI0OTQ5IiwiYW1yIjpbImludGVyYWN0aXZlIl0sImF1dGhfdGltZSI6MTU4MzAxMDQ4OCwicHdpZCI6ImUyMWI4ZGZkLTE5Y2EtNDgwZC05NDQ3LTdmNzdlZjM0ZGUxMyJ9.21OzIK84D-ehcoec-bwa4tv3TKmUiot4NK_ERu_jUo9nQqXJuuGrxS8Ckz91bxQeiki1tTiTLiURH_jcyYiRbqSp0y3qJK21MINIYnZEkqsAvCnUT8kn35x9HAz-pLrxYliAYtyGcrlnUKpCqgmCG2_RwbS24hEMnxuURiy0moXJYhglkDbfIs7ir97YqEJbEwTev34Y0wXMNOjom7iSXXKgXqCF7ll68spApfU_0CjlCpbcOeEHUrsxeRG65uMwR6DJQF2W_GeQbyrBJUpIs7-NZ5FWM3mox6a1Coy96GdmoC7zGyls5Ngd6z5DAc8ME9L4Hu8TzjNf_uTUexpu_g';
        # Obtain your accountId from demo.docusign.com -- the account id is shown in the drop down on the
        # upper right corner of the screen by your picture or the default picture. 
        $accountId = 'c689e0ae-56ea-4909-b3e4-eb6ed1389cb6';
        # Recipient Information:
        $signerName = 'Samuel Pereira Machado Alves da Silva';
        $signerEmail = 'samuel.codigo@gmail.com';
        # The document you wish to send. Path is relative to the root directory of this repo.
        $fileNamePath = '../../frontend/web/arquivos/teste.pdf';
    
        # The API base_path
        $basePath = 'https://demo.docusign.net/restapi';
    
        # Constants
        $appPath = getcwd();
    
        #
        # Step 1. The envelope definition is created.
        #         One signHere tab is added.
        #         The document path supplied is relative to the working directory
        #
        # Create the component objects for the envelope definition...
        $contentBytes = file_get_contents($appPath . "/" . $fileNamePath);
        $base64FileContent =  base64_encode ($contentBytes);
    
        # create the DocuSign document object
        $document = new \DocuSign\eSign\Model\Document([  
            'document_base64' => $base64FileContent, 
            'name' => 'Example document', # can be different from actual file name
            'file_extension' => 'pdf', # many different document types are accepted
            'document_id' => '1' # a label used to reference the doc
        ]);
        // $smsAuthentication = new \DocuSign\eSign\Model\RecipientSMSAuthentication;
        // // $providedPhoneNumber= ['+5512982948776','+5512981967859']; # represents your {PHONE_NUMBER}
        // $providedPhoneNumber = ['+5512996076459']; 
        // $smsAuthentication->setSenderProvidedNumbers($providedPhoneNumber);
        
        #  $smsAuthentication = new \DocuSign\eSign\Model\RecipientSMSAuthentication;
        $smsAuthentication2 = new \DocuSign\eSign\Model\RecipientSMSAuthentication;
        $providedPhoneNumber2= ['+5512988388583']; # represents your {PHONE_NUMBER}
        // $providedPhoneNumber2= ['+5512982948776','+5512981967859']; # represents your {PHONE_NUMBER}
        $smsAuthentication2->setSenderProvidedNumbers($providedPhoneNumber2);

        // $smsAuthentication3 = new \DocuSign\eSign\Model\RecipientSMSAuthentication;
        // $providedPhoneNumber3= ['+5512981967859']; # represents your {PHONE_NUMBER}
        // $smsAuthentication3->setSenderProvidedNumbers($providedPhoneNumber3);

        #
        # The signer object
        $signer = new \DocuSign\eSign\Model\Signer([ 
            'email' => $signerEmail, 'name' => $signerName, 'recipient_id' => "1", 'routing_order' => "1", 'sms_authentication' => $smsAuthentication2, 'require_id_lookup' => 'false', 'id_check_configuration_name' => "SMS Auth $"
        ]);
         
        // $signer2 = new \DocuSign\eSign\Model\Signer([ 
        //     'email' => 'sam@vestor.com.br', 'name' => 'Ariel Henrique Teixeira', 'recipient_id' => "2", 'routing_order' => "1", 'sms_authentication' => $smsAuthentication2, 'require_id_lookup' => 'false', 'id_check_configuration_name' => "SMS Auth $"
        // ]);

        // $signer3 = new \DocuSign\eSign\Model\Signer([ 
        //     'email' => 'poliveira13@hotmail.com', 'name' => 'Priscila Paula', 'recipient_id' => "3", 'routing_order' => "2", 'sms_authentication' => $smsAuthentication3, 'require_id_lookup' => 'false', 'id_check_configuration_name' => "SMS Auth $"
        // ]);
        
        $responsavel = new CertifiedDelivery([
            'recipient_id' => "999",
            'email' => 'samuel.server1@gmail.com',
            'name' => 'Samuel'
        ]);
        
        # DocuSign SignHere field/tab object
        $signHere = new \DocuSign\eSign\Model\SignHere([ 
            'document_id' => '1',
            'page_number' => '1',
            'recipient_id' => '1', 
            'tab_label' => 'SignHereTab', 
            'name' => $signerName,
        ], 
        [ 
            'document_id' => '1',
            'page_number' => '1',
            'recipient_id' => '2', 
            'tab_label' => 'SignHereTab', 
        ],

        [ 
            'document_id' => '1',
            'page_number' => '1',
            'recipient_id' => '3', 
            'tab_label' => 'SignHereTab', 
        ]
    );


        # Add the tabs to the signer object
        # The Tabs object wants arrays of the different field/tab types
        $signer->setTabs(new \DocuSign\eSign\Model\Tabs(['sign_here_tabs' => [$signHere]])); 
    
        # Next, create the top level envelope definition and populate it.
        $envelopeDefinition = new \DocuSign\eSign\Model\EnvelopeDefinition([
            'email_blurb' => 'Contrato referente a locação de imóvel, em caso de dúvidas favor encaminhar para contrato@vestor.com.br<br><b>Imóvel:</b> Rua maria carolina de jesus, 35, Jardim Americano, São José dos Campos, São Paulo, Brasil.<br>Proprietário: Jurandir da Silva, CPF: 421.030.508-19<br>Locatários: Jusé da silva, CPF: 421.123.123-13, Maria da silva CPF: 123.123.123-14<br> ',
            'brand_id' => '0ef8cee4-7b47-4110-a6de-47602a7e24e9',
            'email_subject' => "Contrato de locação Véstor - Nº 200/2020",
            'documents' => [$document], # The order in the docs array determines the order in the envelope
            # The Recipients object wants arrays for each recipient type
            // 'recipients' => new \DocuSign\eSign\Model\Recipients(['signers' => [$signer, $signer2, $signer3]]), 
            'recipients' => new \DocuSign\eSign\Model\Recipients(['signers' => [$signer] , 'certified_deliveries' => [$responsavel]]), 

            'status' => "sent" # requests that the envelope be created and sent.
        ]);
      
        #  Step 2. Create/send the envelope.
        #
        $config = new \DocuSign\eSign\Configuration();
        $config->setHost($basePath);
        $config->addDefaultHeader("Authorization", "Bearer " . $accessToken);
        $apiClient = new \DocuSign\eSign\Client\ApiClient($config);
        $envelopeApi = new \DocuSign\eSign\Api\EnvelopesApi($apiClient);
        $results = $envelopeApi->createEnvelope($accountId, $envelopeDefinition);
        return $results;
    }


    public function actionTeste(){
        try {
            $results = $this->sign();
            ?>
    <html lang="en">
        <body>
        <h4>Results</h4>
        <p>Status: <?= $results['status'] ?>, Envelope ID: <?= $results['envelope_id'] ?></p>
        </body>
    </html>
            <?php
        } catch (\Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            if ($e instanceof \DocuSign\eSign\Client\ApiException) {
                print ("\nDocuSign API error information: \n");
                var_dump ($e->getResponseBody());
            }
        }    
        die();
    }


    public function actionNovo(){
        $model = new Contrato();
        $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if($model->inicio){
            $inicio = explode(",", $model->inicio);
            $inicio = trim($inicio[1]);
            $inicio = explode("/", $inicio);
            // return $inicio;
            $inicio = $inicio[2]."-".$inicio[1]."-".$inicio[0];
            $model->inicio = $inicio;
        }
        if($model->fim){
            $fim = $model->fim;
            $fim = explode("/", $fim);
            $fim = $fim[2]."-".$fim[1]."-".$fim[0];
            $model->fim = $fim;
        }
        $model->duracao = 30;
        $model->idUsuarioInquilino = \Yii::$app->User->identity->id;
        if ($model->validate()) {
            $model->save();
            return ['status' => true, 'model' => $model];
        }
        return ['status' => false, 'errors' => $model->getErrors()];
    }
}